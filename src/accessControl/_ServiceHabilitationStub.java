package accessControl;

/**
 * Interface definition : ServiceHabilitation
 * 
 * @author OpenORB Compiler
 */
public class _ServiceHabilitationStub extends org.omg.CORBA.portable.ObjectImpl
        implements ServiceHabilitation
{
    static final String[] _ids_list =
    {
        "IDL:accessControl/ServiceHabilitation:1.0"
    };

    public String[] _ids()
    {
     return _ids_list;
    }

    private final static Class _opsClass = accessControl.ServiceHabilitationOperations.class;

    /**
     * Operation ajouterHabilitation
     */
    public void ajouterHabilitation(accessControl.Collab e, accessControl.Habilitation h)
        throws accessControl.Existe
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("ajouterHabilitation",true);
                    accessControl.CollabHelper.write(_output,e);
                    accessControl.HabilitationHelper.write(_output,h);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.ExisteHelper.id()))
                    {
                        throw accessControl.ExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("ajouterHabilitation",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceHabilitationOperations _self = (accessControl.ServiceHabilitationOperations) _so.servant;
                try
                {
                    _self.ajouterHabilitation( e,  h);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation supprimerHabilitation
     */
    public void supprimerHabilitation(accessControl.Collab e, accessControl.Habilitation h)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("supprimerHabilitation",true);
                    accessControl.CollabHelper.write(_output,e);
                    accessControl.HabilitationHelper.write(_output,h);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("supprimerHabilitation",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceHabilitationOperations _self = (accessControl.ServiceHabilitationOperations) _so.servant;
                try
                {
                    _self.supprimerHabilitation( e,  h);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getHabilitations
     */
    public short[] getHabilitations(accessControl.Collab e)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getHabilitations",true);
                    accessControl.CollabHelper.write(_output,e);
                    _input = this._invoke(_output);
                    short[] _arg_ret = accessControl.ServiceHabilitationPackage.HabilitationsHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getHabilitations",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceHabilitationOperations _self = (accessControl.ServiceHabilitationOperations) _so.servant;
                try
                {
                    return _self.getHabilitations( e);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation verifierHabilitation
     */
    public boolean verifierHabilitation(accessControl.Collab e, accessControl.Habilitation h)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("verifierHabilitation",true);
                    accessControl.CollabHelper.write(_output,e);
                    accessControl.HabilitationHelper.write(_output,h);
                    _input = this._invoke(_output);
                    boolean _arg_ret = _input.read_boolean();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("verifierHabilitation",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceHabilitationOperations _self = (accessControl.ServiceHabilitationOperations) _so.servant;
                try
                {
                    return _self.verifierHabilitation( e,  h);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

}
