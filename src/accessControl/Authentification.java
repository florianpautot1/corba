package accessControl;

/**
 * Struct definition : Authentification
 * 
 * @author OpenORB Compiler
*/
public final class Authentification implements org.omg.CORBA.portable.IDLEntity
{
    @Override
	public String toString() {
		return "Authentification [id=" + id + ", empreinte=" + empreinte + ", mdp=" + mdp + "]";
	}

	/**
     * Struct member id
     */
    public String id;

    /**
     * Struct member empreinte
     */
    public String empreinte;

    /**
     * Struct member mdp
     */
    public String mdp;

    /**
     * Default constructor
     */
    public Authentification()
    { }

    /**
     * Constructor with fields initialization
     * @param id id struct member
     * @param empreinte empreinte struct member
     * @param mdp mdp struct member
     */
    public Authentification(String id, String empreinte, String mdp)
    {
        this.id = id;
        this.empreinte = empreinte;
        this.mdp = mdp;
    }

}
