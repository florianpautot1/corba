package accessControl;

/**
 * Holder class for : ServiceHabilitation
 * 
 * @author OpenORB Compiler
 */
final public class ServiceHabilitationHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal ServiceHabilitation value
     */
    public accessControl.ServiceHabilitation value;

    /**
     * Default constructor
     */
    public ServiceHabilitationHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public ServiceHabilitationHolder(accessControl.ServiceHabilitation initial)
    {
        value = initial;
    }

    /**
     * Read ServiceHabilitation from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = ServiceHabilitationHelper.read(istream);
    }

    /**
     * Write ServiceHabilitation into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        ServiceHabilitationHelper.write(ostream,value);
    }

    /**
     * Return the ServiceHabilitation TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return ServiceHabilitationHelper.type();
    }

}
