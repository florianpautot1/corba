package accessControl;

/** 
 * Helper class for : ServiceRH
 *  
 * @author OpenORB Compiler
 */ 
public class ServiceRHHelper
{
    /**
     * Insert ServiceRH into an any
     * @param a an any
     * @param t ServiceRH value
     */
    public static void insert(org.omg.CORBA.Any a, accessControl.ServiceRH t)
    {
        a.insert_Object(t , type());
    }

    /**
     * Extract ServiceRH from an any
     * @param a an any
     * @return the extracted ServiceRH value
     */
    public static accessControl.ServiceRH extract(org.omg.CORBA.Any a)
    {
        if (!a.type().equal(type()))
            throw new org.omg.CORBA.MARSHAL();
        try {
            return accessControl.ServiceRHHelper.narrow(a.extract_Object());
        } catch (final org.omg.CORBA.BAD_PARAM e) {
            throw new org.omg.CORBA.MARSHAL(e.getMessage());
        }
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the ServiceRH TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            _tc = orb.create_interface_tc(id(),"ServiceRH");
        }
        return _tc;
    }

    /**
     * Return the ServiceRH IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:accessControl/ServiceRH:1.0";

    /**
     * Read ServiceRH from a marshalled stream
     * @param istream the input stream
     * @return the readed ServiceRH value
     */
    public static accessControl.ServiceRH read(org.omg.CORBA.portable.InputStream istream)
    {
        return(accessControl.ServiceRH)istream.read_Object(accessControl._ServiceRHStub.class);
    }

    /**
     * Write ServiceRH into a marshalled stream
     * @param ostream the output stream
     * @param value ServiceRH value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, accessControl.ServiceRH value)
    {
        ostream.write_Object((org.omg.CORBA.portable.ObjectImpl)value);
    }

    /**
     * Narrow CORBA::Object to ServiceRH
     * @param obj the CORBA Object
     * @return ServiceRH Object
     */
    public static ServiceRH narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof ServiceRH)
            return (ServiceRH)obj;

        if (obj._is_a(id()))
        {
            _ServiceRHStub stub = new _ServiceRHStub();
            stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
            return stub;
        }

        throw new org.omg.CORBA.BAD_PARAM();
    }

    /**
     * Unchecked Narrow CORBA::Object to ServiceRH
     * @param obj the CORBA Object
     * @return ServiceRH Object
     */
    public static ServiceRH unchecked_narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof ServiceRH)
            return (ServiceRH)obj;

        _ServiceRHStub stub = new _ServiceRHStub();
        stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
        return stub;

    }

}
