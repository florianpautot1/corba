package accessControl.clients;

import javax.swing.JOptionPane;

import accessControl.Annuaire;
import accessControl.Collab;
import accessControl.NonExiste;
import accessControl.Porte;
import accessControl.ServiceAcceuil;
import accessControl.ServiceAuthentification;
import accessControl.ServiceAutorisation;
import accessControl.ServiceHabilitation;
import accessControl.ServiceHistorisation;
import accessControl.ServiceRH;
import accessControl.Zone;

public final class Client {

	private static org.omg.CORBA.ORB orb = null;
	private static org.omg.CosNaming.NamingContext nameRoot = null;
	private static String IP_DNS = "192.168.43.86";
	private static boolean isInit = false;
	
	private static ServiceAcceuil accueilDist = null;
	private static Annuaire annuaireDistant = null;
	private static ServiceAuthentification authentificationDistant = null;
	private static ServiceAutorisation autorisationDistant = null;
	private static ServiceHabilitation habDist = null;
	private static ServiceHistorisation histDist = null;
	private static ServiceRH rhDist = null;

	private static int LastInsertedCollabTempId = 0;
	private static int LastInsertedCollabPermId = 0;

	
	/* Permet d'avoir dans chaques fen�tres de l'IHM l'ID du collaborateur et si il est temporaire ou non */
	private static String idCollab;
	public static void setIdCollab(String idCollab) { Client.idCollab = idCollab; }
	public static String getIdCollab() { return idCollab; }
	
	private static boolean estPermanent;
	public static void setEstPermanent(boolean estPermanent) { Client.estPermanent = estPermanent; }
	public static boolean getEstPermanent() { return estPermanent; }
	
	private static Collab collab = new Collab("", "", "", "");
	public static void setCollab(Collab collab) { Client.collab = collab; }
	public static Collab getCollab() { return collab; }
	
	
	public static int getLastInsertedCollabTempId() {
		return LastInsertedCollabTempId;
	}

	public static void setLastInsertedCollabTempId(int getLastInsertedCollabTempId) {
		Client.LastInsertedCollabTempId = getLastInsertedCollabTempId;
	}

	public static int getLastInsertedCollabPermId() {
		return LastInsertedCollabPermId;
	}

	public static void setLastInsertedCollabPermId(int getLastInsertedCollabPermId) {
		Client.LastInsertedCollabPermId = getLastInsertedCollabPermId;
	}


	public static void init() {
		// Récupération de l'objet corba
		// ========================================================

		String[] args = null;
		// Intialisation de l'orb
		orb = org.omg.CORBA.ORB.init(args, null);

		// Recuperation du naming service
		nameRoot = org.omg.CosNaming.NamingContextHelper
				.narrow(orb.string_to_object("corbaloc:iiop:1.2@"+IP_DNS+":2001/NameService"));
		
		isInit = true;
	}

	// Methode de récupération de l'objet distant annuaire
	public static Annuaire getServiceAnnuaire() {
		if(!isInit)
			init();
		if (annuaireDistant == null) {
			try {
				// Construction du nom a rechercher
				String nomService = "Annuaire";
				org.omg.CosNaming.NameComponent[] nameToFind = new org.omg.CosNaming.NameComponent[1];
				nameToFind[0] = new org.omg.CosNaming.NameComponent(nomService, "");

				// Recherche aupres du naming service
				annuaireDistant = (Annuaire) nameRoot.resolve(nameToFind);
				System.out.println("Objet '" + nomService + "' trouve aupres du service de noms. IOR de l'objet :");
				System.out.println(orb.object_to_string(annuaireDistant));

				// ========================================================
				// ========================================================

			} catch (Exception e) {
				System.out.println("ERROR : " + e);
				e.printStackTrace(System.out);
			}
		}
		return annuaireDistant;

	}

	// Methode de récupération de l'objet distant ServiceAuthentification
	public static ServiceAuthentification getServiceAuthentification() {
		if(!isInit)
			init();
		if (authentificationDistant == null) {
			try {
				// Construction du nom a rechercher
				String nomService = "Authentification";
				org.omg.CosNaming.NameComponent[] nameToFind = new org.omg.CosNaming.NameComponent[1];
				nameToFind[0] = new org.omg.CosNaming.NameComponent(nomService, "");

				// Recherche aupres du naming service
				authentificationDistant = (ServiceAuthentification) nameRoot.resolve(nameToFind);
				System.out.println("Objet '" + nomService + "' trouve aupres du service de noms. IOR de l'objet :");
				System.out.println(orb.object_to_string(authentificationDistant));

				// ========================================================
				// ========================================================

			}

			catch (Exception e) {
				System.err.println("ERROR: " + e);
				e.printStackTrace(System.out);
			}
		}

		return authentificationDistant;
	}

	// Methode de récupération de l'objet distant ServiceAutorisation
	public static ServiceAutorisation getServiceAutorisation() {
		if(!isInit)
			init();
		if (autorisationDistant == null) {
			try {
				// Construction du nom a rechercher
				String nomService = "Autorisation";
				org.omg.CosNaming.NameComponent[] nameToFind = new org.omg.CosNaming.NameComponent[1];
				nameToFind[0] = new org.omg.CosNaming.NameComponent(nomService, "");

				// Recherche aupres du naming service
				autorisationDistant = (ServiceAutorisation) nameRoot.resolve(nameToFind);
				System.out.println("Objet '" + nomService + "' trouve aupres du service de noms. IOR de l'objet :");
				System.out.println(orb.object_to_string(autorisationDistant));

				// ========================================================
				// ========================================================

			}

			catch (Exception e) {
				System.err.println("ERROR: " + e);
				e.printStackTrace(System.out);
			}
		}
		return autorisationDistant;
	}

	// Methode de récupération de l'objet distant ServiceHabilitation
	public static ServiceHabilitation getServiceHabilitation() {
		if(!isInit)
			init();
		if (habDist == null) {
			try {
				// Construction du nom a rechercher
				String nomService = "Habilitation";
				org.omg.CosNaming.NameComponent[] nameToFind = new org.omg.CosNaming.NameComponent[1];
				nameToFind[0] = new org.omg.CosNaming.NameComponent(nomService, "");

				// Recherche aupres du naming service
				habDist = (ServiceHabilitation) nameRoot.resolve(nameToFind);
				System.out.println("Objet '" + nomService + "' trouve aupres du service de noms. IOR de l'objet :");
				System.out.println(orb.object_to_string(habDist));

				// ========================================================
				// ========================================================
			}

			catch (Exception e) {
				System.err.println("ERROR: " + e);
				e.printStackTrace(System.out);
			}
		}
		return habDist;
	}

	// Methode de récupération de l'objet distant ServiceHistorisation
	public static ServiceHistorisation getServiceHistorisation() {
		if(!isInit)
			init();
		if (histDist == null) {
			try {
				// Construction du nom a rechercher
				String nomService = "Historisation";
				org.omg.CosNaming.NameComponent[] nameToFind = new org.omg.CosNaming.NameComponent[1];
				nameToFind[0] = new org.omg.CosNaming.NameComponent(nomService, "");

				// Recherche aupres du naming service
				histDist = (ServiceHistorisation) nameRoot.resolve(nameToFind);
				System.out.println("Objet '" + nomService + "' trouve aupres du service de noms. IOR de l'objet :");
				System.out.println(orb.object_to_string(histDist));

				// ========================================================
				// ========================================================
			}

			catch (Exception e) {
				System.err.println("ERROR: " + e);
				e.printStackTrace(System.out);
			}
		}
		return histDist;
	}

	// Methode de récupération de l'objet distant ServiceRH
	public static ServiceRH getServiceRH() {
		if(!isInit)
			init();
		if (rhDist == null) {
			try {
				// Construction du nom a rechercher
				String nomService = "RH";
				org.omg.CosNaming.NameComponent[] nameToFind = new org.omg.CosNaming.NameComponent[1];
				nameToFind[0] = new org.omg.CosNaming.NameComponent(nomService, "");

				// Recherche aupres du naming service
				rhDist = (ServiceRH) nameRoot.resolve(nameToFind);
				System.out.println("Objet '" + nomService + "' trouve aupres du service de noms. IOR de l'objet :");
				System.out.println(orb.object_to_string(rhDist));

				// ========================================================
				// ========================================================
			}

			catch (Exception e) {
				System.err.println("ERROR: " + e);
				e.printStackTrace(System.out);
			}
		}

		return rhDist;
	}

	// Methode de récupération de l'objet distant ServiceAccueil
	public static ServiceAcceuil getServiceAccueil() {
		if(!isInit)
			init();
		if (accueilDist == null) {
			try {
				// Construction du nom a rechercher
				String nomService = "Accueil";
				org.omg.CosNaming.NameComponent[] nameToFind = new org.omg.CosNaming.NameComponent[1];
				nameToFind[0] = new org.omg.CosNaming.NameComponent(nomService, "");

				// Recherche aupres du naming service
				accueilDist = (ServiceAcceuil) nameRoot.resolve(nameToFind);
				System.out.println("Objet '" + nomService + "' trouve aupres du service de noms. IOR de l'objet :");
				System.out.println(orb.object_to_string(accueilDist));

				// ========================================================
				// ========================================================
			}

			catch (Exception e) {
				System.err.println("ERROR: " + e);
				e.printStackTrace(System.out);
			}
		}

		return accueilDist;
	}

	/* R�cup�ration d'une porte */
	public static Zone getZone(Short idPorte) {
		
		Porte allPortes[] = getPortes();
		Zone zoneRes = null;
		for(int i = 0; i < allPortes.length; i++) {
			if (allPortes[i].id == idPorte) {
				zoneRes = allPortes[i].z;	
			}
		}	
		return zoneRes;
	}
	
	/* Initialisation en dur des portes */
	public static Porte[] getPortes() {
		
		Zone allZones[];
		allZones = getServiceAutorisation().getZones();
		
		final Porte Porte1A = new Porte();
		Porte1A.id = 1;
		Porte1A.name = "Porte 1A";
		Porte1A.z = allZones[0];
		
		final Porte Porte2A = new Porte();
		Porte2A.id = 2;
		Porte2A.name = "Porte 2A";
		Porte2A.z = allZones[0];
		
		final Porte Porte1B = new Porte();
		Porte1B.id = 3;
		Porte1B.name = "Porte 1B";
		Porte1B.z = allZones[1];
		
		final Porte Porte2B = new Porte();
		Porte2B.id = 4;
		Porte2B.name = "Porte 2B";
		Porte2B.z = allZones[1];
		
		final Porte Porte1C = new Porte();
		Porte1C.id = 5;
		Porte1C.name = "Porte 1C";
		Porte1C.z = allZones[2];
		
		final Porte Porte2C = new Porte();
		Porte2C.id = 6;
		Porte2C.name = "Porte 2C";
		Porte2C.z = allZones[2];
		
		final Porte Porte3C = new Porte();
		Porte3C.id = 7;
		Porte3C.name = "Porte 3C";
		Porte3C.z = allZones[2];
		
		Porte[] allPortes = { Porte1A, Porte2A, Porte1B, Porte2B, Porte1C, Porte2C, Porte3C };
		return allPortes;
	}
	
	/* Recuperation d'une porte */
	public static Porte getPorte(Short idPorte) {
		
		Porte allPortes[] = getPortes();
		Porte porteRes = null;
		for(int i = 0; i < allPortes.length; i++) {
			if (allPortes[i].id == idPorte) {
				porteRes = allPortes[i];	
			}
		}	
		return porteRes;
	}
	
	public void disconnect() {
		orb.shutdown(false);
	}
}
