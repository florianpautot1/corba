package accessControl;

/**
 * Interface definition : ServiceAutorisation
 * 
 * @author OpenORB Compiler
 */
public class _ServiceAutorisationStub extends org.omg.CORBA.portable.ObjectImpl
        implements ServiceAutorisation
{
    static final String[] _ids_list =
    {
        "IDL:accessControl/ServiceAutorisation:1.0"
    };

    public String[] _ids()
    {
     return _ids_list;
    }

    private final static Class _opsClass = accessControl.ServiceAutorisationOperations.class;

    /**
     * Operation getZones
     */
    public accessControl.Zone[] getZones()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getZones",true);
                    _input = this._invoke(_output);
                    accessControl.Zone[] _arg_ret = accessControl.ServiceAutorisationPackage.ZonesHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getZones",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    return _self.getZones();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation ajouterAutorisationTemp
     */
    public void ajouterAutorisationTemp(accessControl.AutorisationTemp a)
        throws accessControl.Existe
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("ajouterAutorisationTemp",true);
                    accessControl.AutorisationTempHelper.write(_output,a);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.ExisteHelper.id()))
                    {
                        throw accessControl.ExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("ajouterAutorisationTemp",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    _self.ajouterAutorisationTemp( a);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation ajouterAutorisationPerm
     */
    public void ajouterAutorisationPerm(accessControl.AutorisationPerm a)
        throws accessControl.Existe
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("ajouterAutorisationPerm",true);
                    accessControl.AutorisationPermHelper.write(_output,a);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.ExisteHelper.id()))
                    {
                        throw accessControl.ExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("ajouterAutorisationPerm",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    _self.ajouterAutorisationPerm( a);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation supprimerAutorisation
     */
    public void supprimerAutorisation(String idAut)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("supprimerAutorisation",true);
                    _output.write_string(idAut);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("supprimerAutorisation",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    _self.supprimerAutorisation( idAut);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation modifierAutorisationPerm
     */
    public void modifierAutorisationPerm(accessControl.AutorisationPerm a)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("modifierAutorisationPerm",true);
                    accessControl.AutorisationPermHelper.write(_output,a);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("modifierAutorisationPerm",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    _self.modifierAutorisationPerm( a);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation modifierAutorisationTemp
     */
    public void modifierAutorisationTemp(accessControl.AutorisationTemp a)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("modifierAutorisationTemp",true);
                    accessControl.AutorisationTempHelper.write(_output,a);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("modifierAutorisationTemp",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    _self.modifierAutorisationTemp( a);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation donnerRespPourZone
     */
    public void donnerRespPourZone(short idCollab, short idZone)
        throws accessControl.Existe
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("donnerRespPourZone",true);
                    _output.write_short(idCollab);
                    _output.write_short(idZone);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.ExisteHelper.id()))
                    {
                        throw accessControl.ExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("donnerRespPourZone",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    _self.donnerRespPourZone( idCollab,  idZone);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation supprimerResp
     */
    public void supprimerResp(short idCollab, short idZone)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("supprimerResp",true);
                    _output.write_short(idCollab);
                    _output.write_short(idZone);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("supprimerResp",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    _self.supprimerResp( idCollab,  idZone);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getResp
     */
    public short getResp(short idCollab)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getResp",true);
                    _output.write_short(idCollab);
                    _input = this._invoke(_output);
                    short _arg_ret = _input.read_short();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getResp",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    return _self.getResp( idCollab);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getAutorisationTemp
     */
    public accessControl.AutorisationTemp getAutorisationTemp(String idAut)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getAutorisationTemp",true);
                    _output.write_string(idAut);
                    _input = this._invoke(_output);
                    accessControl.AutorisationTemp _arg_ret = accessControl.AutorisationTempHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getAutorisationTemp",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    return _self.getAutorisationTemp( idAut);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getAutorisationPerm
     */
    public accessControl.AutorisationPerm getAutorisationPerm(String idAut)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getAutorisationPerm",true);
                    _output.write_string(idAut);
                    _input = this._invoke(_output);
                    accessControl.AutorisationPerm _arg_ret = accessControl.AutorisationPermHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getAutorisationPerm",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    return _self.getAutorisationPerm( idAut);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getAutorisationsPerms
     */
    public accessControl.AutorisationPerm[] getAutorisationsPerms(accessControl.Collab col)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getAutorisationsPerms",true);
                    accessControl.CollabHelper.write(_output,col);
                    _input = this._invoke(_output);
                    accessControl.AutorisationPerm[] _arg_ret = accessControl.ServiceAutorisationPackage.AutorisationsPermsHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getAutorisationsPerms",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    return _self.getAutorisationsPerms( col);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getAutorisationsTemps
     */
    public accessControl.AutorisationTemp[] getAutorisationsTemps(accessControl.Collab col)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getAutorisationsTemps",true);
                    accessControl.CollabHelper.write(_output,col);
                    _input = this._invoke(_output);
                    accessControl.AutorisationTemp[] _arg_ret = accessControl.ServiceAutorisationPackage.AutorisationsTempsHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getAutorisationsTemps",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    return _self.getAutorisationsTemps( col);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getAutorisationsPermsForZone
     */
    public accessControl.AutorisationPerm[] getAutorisationsPermsForZone(short idZone)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getAutorisationsPermsForZone",true);
                    _output.write_short(idZone);
                    _input = this._invoke(_output);
                    accessControl.AutorisationPerm[] _arg_ret = accessControl.ServiceAutorisationPackage.AutorisationsPermsHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getAutorisationsPermsForZone",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    return _self.getAutorisationsPermsForZone( idZone);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getAutorisationsTempsForZone
     */
    public accessControl.AutorisationTemp[] getAutorisationsTempsForZone(short idZone)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getAutorisationsTempsForZone",true);
                    _output.write_short(idZone);
                    _input = this._invoke(_output);
                    accessControl.AutorisationTemp[] _arg_ret = accessControl.ServiceAutorisationPackage.AutorisationsTempsHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getAutorisationsTempsForZone",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    return _self.getAutorisationsTempsForZone( idZone);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation verifierAutorisation
     */
    public boolean verifierAutorisation(accessControl.Collab e, accessControl.Zone z)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("verifierAutorisation",true);
                    accessControl.CollabHelper.write(_output,e);
                    accessControl.ZoneHelper.write(_output,z);
                    _input = this._invoke(_output);
                    boolean _arg_ret = _input.read_boolean();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("verifierAutorisation",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAutorisationOperations _self = (accessControl.ServiceAutorisationOperations) _so.servant;
                try
                {
                    return _self.verifierAutorisation( e,  z);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

}
