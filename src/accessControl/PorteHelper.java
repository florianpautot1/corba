package accessControl;

/** 
 * Helper class for : Porte
 *  
 * @author OpenORB Compiler
 */ 
public class PorteHelper
{
    private static final boolean HAS_OPENORB;
    static {
        boolean hasOpenORB = false;
        try {
            Thread.currentThread().getContextClassLoader().loadClass("org.openorb.CORBA.Any");
            hasOpenORB = true;
        }
        catch(ClassNotFoundException ex) {
        }
        HAS_OPENORB = hasOpenORB;
    }
    /**
     * Insert Porte into an any
     * @param a an any
     * @param t Porte value
     */
    public static void insert(org.omg.CORBA.Any a, accessControl.Porte t)
    {
        a.insert_Streamable(new accessControl.PorteHolder(t));
    }

    /**
     * Extract Porte from an any
     * @param a an any
     * @return the extracted Porte value
     */
    public static accessControl.Porte extract(org.omg.CORBA.Any a)
    {
        if (!a.type().equal(type()))
            throw new org.omg.CORBA.MARSHAL();
        if (HAS_OPENORB && a instanceof org.openorb.CORBA.Any) {
            // streamable extraction. The jdk stubs incorrectly define the Any stub
            org.openorb.CORBA.Any any = (org.openorb.CORBA.Any)a;
            try {
                org.omg.CORBA.portable.Streamable s = any.extract_Streamable();
                if(s instanceof accessControl.PorteHolder)
                    return ((accessControl.PorteHolder)s).value;
            } catch (org.omg.CORBA.BAD_INV_ORDER ex) {
            }
            accessControl.PorteHolder h = new accessControl.PorteHolder(read(a.create_input_stream()));
            a.insert_Streamable(h);
            return h.value;
        }
        return read(a.create_input_stream());
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;
    private static boolean _working = false;

    /**
     * Return the Porte TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            synchronized(org.omg.CORBA.TypeCode.class) {
                if (_tc != null)
                    return _tc;
                if (_working)
                    return org.omg.CORBA.ORB.init().create_recursive_tc(id());
                _working = true;
                org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
                org.omg.CORBA.StructMember []_members = new org.omg.CORBA.StructMember[3];

                _members[0] = new org.omg.CORBA.StructMember();
                _members[0].name = "id";
                _members[0].type = orb.get_primitive_tc(org.omg.CORBA.TCKind.tk_short);
                _members[1] = new org.omg.CORBA.StructMember();
                _members[1].name = "name";
                _members[1].type = orb.get_primitive_tc(org.omg.CORBA.TCKind.tk_string);
                _members[2] = new org.omg.CORBA.StructMember();
                _members[2].name = "z";
                _members[2].type = accessControl.ZoneHelper.type();
                _tc = orb.create_struct_tc(id(),"Porte",_members);
                _working = false;
            }
        }
        return _tc;
    }

    /**
     * Return the Porte IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:accessControl/Porte:1.0";

    /**
     * Read Porte from a marshalled stream
     * @param istream the input stream
     * @return the readed Porte value
     */
    public static accessControl.Porte read(org.omg.CORBA.portable.InputStream istream)
    {
        accessControl.Porte new_one = new accessControl.Porte();

        new_one.id = istream.read_short();
        new_one.name = istream.read_string();
        new_one.z = accessControl.ZoneHelper.read(istream);

        return new_one;
    }

    /**
     * Write Porte into a marshalled stream
     * @param ostream the output stream
     * @param value Porte value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, accessControl.Porte value)
    {
        ostream.write_short(value.id);
        ostream.write_string(value.name);
        accessControl.ZoneHelper.write(ostream,value.z);
    }

}
