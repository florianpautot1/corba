package accessControl;

/**
 * Interface definition : ServiceRH
 * 
 * @author OpenORB Compiler
 */
public abstract class ServiceRHPOA extends org.omg.PortableServer.Servant
        implements ServiceRHOperations, org.omg.CORBA.portable.InvokeHandler
{
    public ServiceRH _this()
    {
        return ServiceRHHelper.narrow(_this_object());
    }

    public ServiceRH _this(org.omg.CORBA.ORB orb)
    {
        return ServiceRHHelper.narrow(_this_object(orb));
    }

    private static String [] _ids_list =
    {
        "IDL:accessControl/ServiceRH:1.0"
    };

    public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte [] objectId)
    {
        return _ids_list;
    }

    public final org.omg.CORBA.portable.OutputStream _invoke(final String opName,
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler)
    {

        if (opName.equals("ajouterCollabPerm")) {
                return _invoke_ajouterCollabPerm(_is, handler);
        } else if (opName.equals("getCollabPerm")) {
                return _invoke_getCollabPerm(_is, handler);
        } else if (opName.equals("getCollabPerms")) {
                return _invoke_getCollabPerms(_is, handler);
        } else if (opName.equals("modifierCollabPerm")) {
                return _invoke_modifierCollabPerm(_is, handler);
        } else if (opName.equals("supprimerCollabPerm")) {
                return _invoke_supprimerCollabPerm(_is, handler);
        } else {
            throw new org.omg.CORBA.BAD_OPERATION(opName);
        }
    }

    // helper methods
    private org.omg.CORBA.portable.OutputStream _invoke_ajouterCollabPerm(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);

        try
        {
            ajouterCollabPerm(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.Existe _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.ExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_supprimerCollabPerm(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        short arg0_in = _is.read_short();

        try
        {
            supprimerCollabPerm(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_modifierCollabPerm(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);

        try
        {
            modifierCollabPerm(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getCollabPerm(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        short arg0_in = _is.read_short();

        try
        {
            accessControl.Collab _arg_result = getCollabPerm(arg0_in);

            _output = handler.createReply();
            accessControl.CollabHelper.write(_output,_arg_result);

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getCollabPerms(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        accessControl.Collab[] _arg_result = getCollabPerms();

        _output = handler.createReply();
        accessControl.CollabsHelper.write(_output,_arg_result);

        return _output;
    }

}
