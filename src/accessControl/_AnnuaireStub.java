package accessControl;

/**
 * Interface definition : Annuaire
 * 
 * @author OpenORB Compiler
 */
public class _AnnuaireStub extends org.omg.CORBA.portable.ObjectImpl
        implements Annuaire
{
    static final String[] _ids_list =
    {
        "IDL:accessControl/Annuaire:1.0"
    };

    public String[] _ids()
    {
     return _ids_list;
    }

    private final static Class _opsClass = accessControl.AnnuaireOperations.class;

    /**
     * Operation ajouterCollab
     */
    public void ajouterCollab(accessControl.Collab col)
        throws accessControl.Existe
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("ajouterCollab",true);
                    accessControl.CollabHelper.write(_output,col);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.ExisteHelper.id()))
                    {
                        throw accessControl.ExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("ajouterCollab",_opsClass);
                if (_so == null)
                   continue;
                accessControl.AnnuaireOperations _self = (accessControl.AnnuaireOperations) _so.servant;
                try
                {
                    _self.ajouterCollab( col);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation supprimerCollab
     */
    public void supprimerCollab(String idCol)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("supprimerCollab",true);
                    _output.write_string(idCol);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("supprimerCollab",_opsClass);
                if (_so == null)
                   continue;
                accessControl.AnnuaireOperations _self = (accessControl.AnnuaireOperations) _so.servant;
                try
                {
                    _self.supprimerCollab( idCol);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation modifierCollab
     */
    public void modifierCollab(accessControl.Collab col)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("modifierCollab",true);
                    accessControl.CollabHelper.write(_output,col);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("modifierCollab",_opsClass);
                if (_so == null)
                   continue;
                accessControl.AnnuaireOperations _self = (accessControl.AnnuaireOperations) _so.servant;
                try
                {
                    _self.modifierCollab( col);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation verifierPhoto
     */
    public boolean verifierPhoto(String idCollab, String photo)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("verifierPhoto",true);
                    _output.write_string(idCollab);
                    _output.write_string(photo);
                    _input = this._invoke(_output);
                    boolean _arg_ret = _input.read_boolean();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("verifierPhoto",_opsClass);
                if (_so == null)
                   continue;
                accessControl.AnnuaireOperations _self = (accessControl.AnnuaireOperations) _so.servant;
                try
                {
                    return _self.verifierPhoto( idCollab,  photo);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getCollab
     */
    public accessControl.Collab getCollab(String idCol)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getCollab",true);
                    _output.write_string(idCol);
                    _input = this._invoke(_output);
                    accessControl.Collab _arg_ret = accessControl.CollabHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getCollab",_opsClass);
                if (_so == null)
                   continue;
                accessControl.AnnuaireOperations _self = (accessControl.AnnuaireOperations) _so.servant;
                try
                {
                    return _self.getCollab( idCol);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getCollabs
     */
    public accessControl.Collab[] getCollabs()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getCollabs",true);
                    _input = this._invoke(_output);
                    accessControl.Collab[] _arg_ret = accessControl.CollabsHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getCollabs",_opsClass);
                if (_so == null)
                   continue;
                accessControl.AnnuaireOperations _self = (accessControl.AnnuaireOperations) _so.servant;
                try
                {
                    return _self.getCollabs();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

}
