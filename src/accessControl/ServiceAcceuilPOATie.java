package accessControl;

/**
 * Interface definition : ServiceAcceuil
 * 
 * @author OpenORB Compiler
 */
public class ServiceAcceuilPOATie extends ServiceAcceuilPOA
{

    //
    // Private reference to implementation object
    //
    private ServiceAcceuilOperations _tie;

    //
    // Private reference to POA
    //
    private org.omg.PortableServer.POA _poa;

    /**
     * Constructor
     */
    public ServiceAcceuilPOATie(ServiceAcceuilOperations tieObject)
    {
        _tie = tieObject;
    }

    /**
     * Constructor
     */
    public ServiceAcceuilPOATie(ServiceAcceuilOperations tieObject, org.omg.PortableServer.POA poa)
    {
        _tie = tieObject;
        _poa = poa;
    }

    /**
     * Get the delegate
     */
    public ServiceAcceuilOperations _delegate()
    {
        return _tie;
    }

    /**
     * Set the delegate
     */
    public void _delegate(ServiceAcceuilOperations delegate_)
    {
        _tie = delegate_;
    }

    /**
     * _default_POA method
     */
    public org.omg.PortableServer.POA _default_POA()
    {
        if (_poa != null)
            return _poa;
        else
            return super._default_POA();
    }

    /**
     * Operation ajouterCollabTemp
     */
    public void ajouterCollabTemp(accessControl.Collab col)
        throws accessControl.Existe
    {
        _tie.ajouterCollabTemp( col);
    }

    /**
     * Operation supprimerCollabTemp
     */
    public void supprimerCollabTemp(short idCol)
        throws accessControl.NonExiste
    {
        _tie.supprimerCollabTemp( idCol);
    }

    /**
     * Operation getCollabTemp
     */
    public accessControl.Collab getCollabTemp(short idCol)
        throws accessControl.NonExiste
    {
        return _tie.getCollabTemp( idCol);
    }

    /**
     * Operation getCollabTemps
     */
    public accessControl.Collab[] getCollabTemps()
    {
        return _tie.getCollabTemps();
    }

}
