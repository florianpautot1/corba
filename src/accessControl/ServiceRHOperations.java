package accessControl;

/**
 * Interface definition : ServiceRH
 * 
 * @author OpenORB Compiler
 */
public interface ServiceRHOperations
{
    /**
     * Operation ajouterCollabPerm
     */
    public void ajouterCollabPerm(accessControl.Collab col)
        throws accessControl.Existe;

    /**
     * Operation supprimerCollabPerm
     */
    public void supprimerCollabPerm(short idCol)
        throws accessControl.NonExiste;

    /**
     * Operation modifierCollabPerm
     */
    public void modifierCollabPerm(accessControl.Collab col)
        throws accessControl.NonExiste;

    /**
     * Operation getCollabPerm
     */
    public accessControl.Collab getCollabPerm(short idCol)
        throws accessControl.NonExiste;

    /**
     * Operation getCollabPerms
     */
    public accessControl.Collab[] getCollabPerms();

}
