package accessControl;

/**
 * Holder class for : ServiceRH
 * 
 * @author OpenORB Compiler
 */
final public class ServiceRHHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal ServiceRH value
     */
    public accessControl.ServiceRH value;

    /**
     * Default constructor
     */
    public ServiceRHHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public ServiceRHHolder(accessControl.ServiceRH initial)
    {
        value = initial;
    }

    /**
     * Read ServiceRH from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = ServiceRHHelper.read(istream);
    }

    /**
     * Write ServiceRH into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        ServiceRHHelper.write(ostream,value);
    }

    /**
     * Return the ServiceRH TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return ServiceRHHelper.type();
    }

}
