package accessControl;

/**
 * Interface definition : ServiceRH
 * 
 * @author OpenORB Compiler
 */
public class ServiceRHPOATie extends ServiceRHPOA
{

    //
    // Private reference to implementation object
    //
    private ServiceRHOperations _tie;

    //
    // Private reference to POA
    //
    private org.omg.PortableServer.POA _poa;

    /**
     * Constructor
     */
    public ServiceRHPOATie(ServiceRHOperations tieObject)
    {
        _tie = tieObject;
    }

    /**
     * Constructor
     */
    public ServiceRHPOATie(ServiceRHOperations tieObject, org.omg.PortableServer.POA poa)
    {
        _tie = tieObject;
        _poa = poa;
    }

    /**
     * Get the delegate
     */
    public ServiceRHOperations _delegate()
    {
        return _tie;
    }

    /**
     * Set the delegate
     */
    public void _delegate(ServiceRHOperations delegate_)
    {
        _tie = delegate_;
    }

    /**
     * _default_POA method
     */
    public org.omg.PortableServer.POA _default_POA()
    {
        if (_poa != null)
            return _poa;
        else
            return super._default_POA();
    }

    /**
     * Operation ajouterCollabPerm
     */
    public void ajouterCollabPerm(accessControl.Collab col)
        throws accessControl.Existe
    {
        _tie.ajouterCollabPerm( col);
    }

    /**
     * Operation supprimerCollabPerm
     */
    public void supprimerCollabPerm(short idCol)
        throws accessControl.NonExiste
    {
        _tie.supprimerCollabPerm( idCol);
    }

    /**
     * Operation modifierCollabPerm
     */
    public void modifierCollabPerm(accessControl.Collab col)
        throws accessControl.NonExiste
    {
        _tie.modifierCollabPerm( col);
    }

    /**
     * Operation getCollabPerm
     */
    public accessControl.Collab getCollabPerm(short idCol)
        throws accessControl.NonExiste
    {
        return _tie.getCollabPerm( idCol);
    }

    /**
     * Operation getCollabPerms
     */
    public accessControl.Collab[] getCollabPerms()
    {
        return _tie.getCollabPerms();
    }

}
