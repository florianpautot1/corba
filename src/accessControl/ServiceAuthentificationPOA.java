package accessControl;

/**
 * Interface definition : ServiceAuthentification
 * 
 * @author OpenORB Compiler
 */
public abstract class ServiceAuthentificationPOA extends org.omg.PortableServer.Servant
        implements ServiceAuthentificationOperations, org.omg.CORBA.portable.InvokeHandler
{
    public ServiceAuthentification _this()
    {
        return ServiceAuthentificationHelper.narrow(_this_object());
    }

    public ServiceAuthentification _this(org.omg.CORBA.ORB orb)
    {
        return ServiceAuthentificationHelper.narrow(_this_object(orb));
    }

    private static String [] _ids_list =
    {
        "IDL:accessControl/ServiceAuthentification:1.0"
    };

    public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte [] objectId)
    {
        return _ids_list;
    }

    public final org.omg.CORBA.portable.OutputStream _invoke(final String opName,
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler)
    {

        if (opName.equals("definirAuthentification")) {
                return _invoke_definirAuthentification(_is, handler);
        } else if (opName.equals("getAuthentification")) {
                return _invoke_getAuthentification(_is, handler);
        } else if (opName.equals("modifierEmpreinte")) {
                return _invoke_modifierEmpreinte(_is, handler);
        } else if (opName.equals("modifierMdp")) {
                return _invoke_modifierMdp(_is, handler);
        } else if (opName.equals("supprimerEmpreinte")) {
                return _invoke_supprimerEmpreinte(_is, handler);
        } else if (opName.equals("verificationEmpreinte")) {
                return _invoke_verificationEmpreinte(_is, handler);
        } else if (opName.equals("verificationMdp")) {
                return _invoke_verificationMdp(_is, handler);
        } else {
            throw new org.omg.CORBA.BAD_OPERATION(opName);
        }
    }

    // helper methods
    private org.omg.CORBA.portable.OutputStream _invoke_verificationEmpreinte(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();

        String _arg_result = verificationEmpreinte(arg0_in);

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_definirAuthentification(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);
        String arg1_in = _is.read_string();
        String arg2_in = _is.read_string();

        try
        {
            definirAuthentification(arg0_in, arg1_in, arg2_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        catch (accessControl.Existe _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.ExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_modifierEmpreinte(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);
        String arg1_in = _is.read_string();

        try
        {
            modifierEmpreinte(arg0_in, arg1_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_modifierMdp(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);
        String arg1_in = _is.read_string();

        try
        {
            modifierMdp(arg0_in, arg1_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_supprimerEmpreinte(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);

        try
        {
            supprimerEmpreinte(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_verificationMdp(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);
        String arg1_in = _is.read_string();

        boolean _arg_result = verificationMdp(arg0_in, arg1_in);

        _output = handler.createReply();
        _output.write_boolean(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getAuthentification(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);

        try
        {
            accessControl.Authentification _arg_result = getAuthentification(arg0_in);

            _output = handler.createReply();
            accessControl.AuthentificationHelper.write(_output,_arg_result);

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

}
