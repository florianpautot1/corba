package accessControl;

/**
 * Interface definition : ServiceAcceuil
 * 
 * @author OpenORB Compiler
 */
public class _ServiceAcceuilStub extends org.omg.CORBA.portable.ObjectImpl
        implements ServiceAcceuil
{
    static final String[] _ids_list =
    {
        "IDL:accessControl/ServiceAcceuil:1.0"
    };

    public String[] _ids()
    {
     return _ids_list;
    }

    private final static Class _opsClass = accessControl.ServiceAcceuilOperations.class;

    /**
     * Operation ajouterCollabTemp
     */
    public void ajouterCollabTemp(accessControl.Collab col)
        throws accessControl.Existe
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("ajouterCollabTemp",true);
                    accessControl.CollabHelper.write(_output,col);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.ExisteHelper.id()))
                    {
                        throw accessControl.ExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("ajouterCollabTemp",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAcceuilOperations _self = (accessControl.ServiceAcceuilOperations) _so.servant;
                try
                {
                    _self.ajouterCollabTemp( col);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation supprimerCollabTemp
     */
    public void supprimerCollabTemp(short idCol)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("supprimerCollabTemp",true);
                    _output.write_short(idCol);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("supprimerCollabTemp",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAcceuilOperations _self = (accessControl.ServiceAcceuilOperations) _so.servant;
                try
                {
                    _self.supprimerCollabTemp( idCol);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getCollabTemp
     */
    public accessControl.Collab getCollabTemp(short idCol)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getCollabTemp",true);
                    _output.write_short(idCol);
                    _input = this._invoke(_output);
                    accessControl.Collab _arg_ret = accessControl.CollabHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getCollabTemp",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAcceuilOperations _self = (accessControl.ServiceAcceuilOperations) _so.servant;
                try
                {
                    return _self.getCollabTemp( idCol);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getCollabTemps
     */
    public accessControl.Collab[] getCollabTemps()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getCollabTemps",true);
                    _input = this._invoke(_output);
                    accessControl.Collab[] _arg_ret = accessControl.CollabsHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getCollabTemps",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAcceuilOperations _self = (accessControl.ServiceAcceuilOperations) _so.servant;
                try
                {
                    return _self.getCollabTemps();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

}
