package accessControl;

/**
 * Interface definition : ServiceRH
 * 
 * @author OpenORB Compiler
 */
public interface ServiceRH extends ServiceRHOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
