package accessControl;

/**
 * Holder class for : Existe
 * 
 * @author OpenORB Compiler
 */
final public class ExisteHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal Existe value
     */
    public accessControl.Existe value;

    /**
     * Default constructor
     */
    public ExisteHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public ExisteHolder(accessControl.Existe initial)
    {
        value = initial;
    }

    /**
     * Read Existe from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = ExisteHelper.read(istream);
    }

    /**
     * Write Existe into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        ExisteHelper.write(ostream,value);
    }

    /**
     * Return the Existe TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return ExisteHelper.type();
    }

}
