package accessControl;

/**
 * Interface definition : ServiceAutorisation
 * 
 * @author OpenORB Compiler
 */
public interface ServiceAutorisationOperations
{
    /**
     * Operation getZones
     */
    public accessControl.Zone[] getZones();

    /**
     * Operation ajouterAutorisationTemp
     */
    public void ajouterAutorisationTemp(accessControl.AutorisationTemp a)
        throws accessControl.Existe;

    /**
     * Operation ajouterAutorisationPerm
     */
    public void ajouterAutorisationPerm(accessControl.AutorisationPerm a)
        throws accessControl.Existe;

    /**
     * Operation supprimerAutorisation
     */
    public void supprimerAutorisation(String idAut)
        throws accessControl.NonExiste;

    /**
     * Operation modifierAutorisationPerm
     */
    public void modifierAutorisationPerm(accessControl.AutorisationPerm a)
        throws accessControl.NonExiste;

    /**
     * Operation modifierAutorisationTemp
     */
    public void modifierAutorisationTemp(accessControl.AutorisationTemp a)
        throws accessControl.NonExiste;

    /**
     * Operation donnerRespPourZone
     */
    public void donnerRespPourZone(short idCollab, short idZone)
        throws accessControl.Existe;

    /**
     * Operation supprimerResp
     */
    public void supprimerResp(short idCollab, short idZone)
        throws accessControl.NonExiste;

    /**
     * Operation getResp
     */
    public short getResp(short idCollab);

    /**
     * Operation getAutorisationTemp
     */
    public accessControl.AutorisationTemp getAutorisationTemp(String idAut)
        throws accessControl.NonExiste;

    /**
     * Operation getAutorisationPerm
     */
    public accessControl.AutorisationPerm getAutorisationPerm(String idAut)
        throws accessControl.NonExiste;

    /**
     * Operation getAutorisationsPerms
     */
    public accessControl.AutorisationPerm[] getAutorisationsPerms(accessControl.Collab col)
        throws accessControl.NonExiste;

    /**
     * Operation getAutorisationsTemps
     */
    public accessControl.AutorisationTemp[] getAutorisationsTemps(accessControl.Collab col)
        throws accessControl.NonExiste;

    /**
     * Operation getAutorisationsPermsForZone
     */
    public accessControl.AutorisationPerm[] getAutorisationsPermsForZone(short idZone);

    /**
     * Operation getAutorisationsTempsForZone
     */
    public accessControl.AutorisationTemp[] getAutorisationsTempsForZone(short idZone);

    /**
     * Operation verifierAutorisation
     */
    public boolean verifierAutorisation(accessControl.Collab e, accessControl.Zone z);

}
