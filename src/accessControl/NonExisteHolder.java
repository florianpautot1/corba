package accessControl;

/**
 * Holder class for : NonExiste
 * 
 * @author OpenORB Compiler
 */
final public class NonExisteHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal NonExiste value
     */
    public accessControl.NonExiste value;

    /**
     * Default constructor
     */
    public NonExisteHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public NonExisteHolder(accessControl.NonExiste initial)
    {
        value = initial;
    }

    /**
     * Read NonExiste from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = NonExisteHelper.read(istream);
    }

    /**
     * Write NonExiste into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        NonExisteHelper.write(ostream,value);
    }

    /**
     * Return the NonExiste TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return NonExisteHelper.type();
    }

}
