package accessControl;

/**
 * Interface definition : ServiceHabilitation
 * 
 * @author OpenORB Compiler
 */
public interface ServiceHabilitation extends ServiceHabilitationOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
