package accessControl;

/**
 * Struct definition : AutorisationTemp
 * 
 * @author OpenORB Compiler
*/
public final class AutorisationTemp implements org.omg.CORBA.portable.IDLEntity
{
    @Override
	public String toString() {
		return "AutorisationTemp [id=" + id + ", idCollab=" + idCollab + ", hDeb=" + hDeb + ", hFin=" + hFin
				+ ", idZone=" + idZone + ", jourDeb=" + jourDeb + ", jourFin=" + jourFin + "]";
	}

	/**
     * Struct member id
     */
    public String id;

    /**
     * Struct member idCollab
     */
    public String idCollab;

    /**
     * Struct member hDeb
     */
    public String hDeb;

    /**
     * Struct member hFin
     */
    public String hFin;

    /**
     * Struct member idZone
     */
    public short idZone;

    /**
     * Struct member jourDeb
     */
    public String jourDeb;

    /**
     * Struct member jourFin
     */
    public String jourFin;

    /**
     * Default constructor
     */
    public AutorisationTemp()
    { }

    /**
     * Constructor with fields initialization
     * @param id id struct member
     * @param idCollab idCollab struct member
     * @param hDeb hDeb struct member
     * @param hFin hFin struct member
     * @param idZone idZone struct member
     * @param jourDeb jourDeb struct member
     * @param jourFin jourFin struct member
     */
    public AutorisationTemp(String id, String idCollab, String hDeb, String hFin, short idZone, String jourDeb, String jourFin)
    {
        this.id = id;
        this.idCollab = idCollab;
        this.hDeb = hDeb;
        this.hFin = hFin;
        this.idZone = idZone;
        this.jourDeb = jourDeb;
        this.jourFin = jourFin;
    }

}
