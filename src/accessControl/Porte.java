package accessControl;

/**
 * Struct definition : Porte
 * 
 * @author OpenORB Compiler
*/
public final class Porte implements org.omg.CORBA.portable.IDLEntity
{
    @Override
	public String toString() {
		return "Porte [id=" + id + ", name=" + name + ", z=" + z + "]";
	}

	/**
     * Struct member id
     */
    public short id;

    /**
     * Struct member name
     */
    public String name;

    /**
     * Struct member z
     */
    public accessControl.Zone z;

    /**
     * Default constructor
     */
    public Porte()
    { }

    /**
     * Constructor with fields initialization
     * @param id id struct member
     * @param name name struct member
     * @param z z struct member
     */
    public Porte(short id, String name, accessControl.Zone z)
    {
        this.id = id;
        this.name = name;
        this.z = z;
    }

}
