package accessControl;

/**
 * Interface definition : ServiceHabilitation
 * 
 * @author OpenORB Compiler
 */
public class ServiceHabilitationPOATie extends ServiceHabilitationPOA
{

    //
    // Private reference to implementation object
    //
    private ServiceHabilitationOperations _tie;

    //
    // Private reference to POA
    //
    private org.omg.PortableServer.POA _poa;

    /**
     * Constructor
     */
    public ServiceHabilitationPOATie(ServiceHabilitationOperations tieObject)
    {
        _tie = tieObject;
    }

    /**
     * Constructor
     */
    public ServiceHabilitationPOATie(ServiceHabilitationOperations tieObject, org.omg.PortableServer.POA poa)
    {
        _tie = tieObject;
        _poa = poa;
    }

    /**
     * Get the delegate
     */
    public ServiceHabilitationOperations _delegate()
    {
        return _tie;
    }

    /**
     * Set the delegate
     */
    public void _delegate(ServiceHabilitationOperations delegate_)
    {
        _tie = delegate_;
    }

    /**
     * _default_POA method
     */
    public org.omg.PortableServer.POA _default_POA()
    {
        if (_poa != null)
            return _poa;
        else
            return super._default_POA();
    }

    /**
     * Operation ajouterHabilitation
     */
    public void ajouterHabilitation(accessControl.Collab e, accessControl.Habilitation h)
        throws accessControl.Existe
    {
        _tie.ajouterHabilitation( e,  h);
    }

    /**
     * Operation supprimerHabilitation
     */
    public void supprimerHabilitation(accessControl.Collab e, accessControl.Habilitation h)
        throws accessControl.NonExiste
    {
        _tie.supprimerHabilitation( e,  h);
    }

    /**
     * Operation getHabilitations
     */
    public short[] getHabilitations(accessControl.Collab e)
        throws accessControl.NonExiste
    {
        return _tie.getHabilitations( e);
    }

    /**
     * Operation verifierHabilitation
     */
    public boolean verifierHabilitation(accessControl.Collab e, accessControl.Habilitation h)
    {
        return _tie.verifierHabilitation( e,  h);
    }

}
