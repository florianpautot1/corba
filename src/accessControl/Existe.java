package accessControl;

/**
 * Exception definition : Existe
 * 
 * @author OpenORB Compiler
 */
public final class Existe extends org.omg.CORBA.UserException
{
    /**
     * Exception member raison
     */
    public String raison;

    /**
     * Default constructor
     */
    public Existe()
    {
        super(ExisteHelper.id());
    }

    /**
     * Constructor with fields initialization
     * @param raison raison exception member
     */
    public Existe(String raison)
    {
        super(ExisteHelper.id());
        this.raison = raison;
    }

    /**
     * Full constructor with fields initialization
     * @param raison raison exception member
     */
    public Existe(String orb_reason, String raison)
    {
        super(ExisteHelper.id() +" " +  orb_reason);
        this.raison = raison;
    }

}
