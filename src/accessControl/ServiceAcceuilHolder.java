package accessControl;

/**
 * Holder class for : ServiceAcceuil
 * 
 * @author OpenORB Compiler
 */
final public class ServiceAcceuilHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal ServiceAcceuil value
     */
    public accessControl.ServiceAcceuil value;

    /**
     * Default constructor
     */
    public ServiceAcceuilHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public ServiceAcceuilHolder(accessControl.ServiceAcceuil initial)
    {
        value = initial;
    }

    /**
     * Read ServiceAcceuil from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = ServiceAcceuilHelper.read(istream);
    }

    /**
     * Write ServiceAcceuil into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        ServiceAcceuilHelper.write(ostream,value);
    }

    /**
     * Return the ServiceAcceuil TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return ServiceAcceuilHelper.type();
    }

}
