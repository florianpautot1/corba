package accessControl;

/**
 * Interface definition : ServiceHabilitation
 * 
 * @author OpenORB Compiler
 */
public abstract class ServiceHabilitationPOA extends org.omg.PortableServer.Servant
        implements ServiceHabilitationOperations, org.omg.CORBA.portable.InvokeHandler
{
    public ServiceHabilitation _this()
    {
        return ServiceHabilitationHelper.narrow(_this_object());
    }

    public ServiceHabilitation _this(org.omg.CORBA.ORB orb)
    {
        return ServiceHabilitationHelper.narrow(_this_object(orb));
    }

    private static String [] _ids_list =
    {
        "IDL:accessControl/ServiceHabilitation:1.0"
    };

    public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte [] objectId)
    {
        return _ids_list;
    }

    public final org.omg.CORBA.portable.OutputStream _invoke(final String opName,
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler)
    {

        if (opName.equals("ajouterHabilitation")) {
                return _invoke_ajouterHabilitation(_is, handler);
        } else if (opName.equals("getHabilitations")) {
                return _invoke_getHabilitations(_is, handler);
        } else if (opName.equals("supprimerHabilitation")) {
                return _invoke_supprimerHabilitation(_is, handler);
        } else if (opName.equals("verifierHabilitation")) {
                return _invoke_verifierHabilitation(_is, handler);
        } else {
            throw new org.omg.CORBA.BAD_OPERATION(opName);
        }
    }

    // helper methods
    private org.omg.CORBA.portable.OutputStream _invoke_ajouterHabilitation(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);
        accessControl.Habilitation arg1_in = accessControl.HabilitationHelper.read(_is);

        try
        {
            ajouterHabilitation(arg0_in, arg1_in);

            _output = handler.createReply();

        }
        catch (accessControl.Existe _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.ExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_supprimerHabilitation(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);
        accessControl.Habilitation arg1_in = accessControl.HabilitationHelper.read(_is);

        try
        {
            supprimerHabilitation(arg0_in, arg1_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getHabilitations(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);

        try
        {
            short[] _arg_result = getHabilitations(arg0_in);

            _output = handler.createReply();
            accessControl.ServiceHabilitationPackage.HabilitationsHelper.write(_output,_arg_result);

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_verifierHabilitation(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);
        accessControl.Habilitation arg1_in = accessControl.HabilitationHelper.read(_is);

        boolean _arg_result = verifierHabilitation(arg0_in, arg1_in);

        _output = handler.createReply();
        _output.write_boolean(_arg_result);

        return _output;
    }

}
