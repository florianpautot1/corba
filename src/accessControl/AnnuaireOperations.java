package accessControl;

/**
 * Interface definition : Annuaire
 * 
 * @author OpenORB Compiler
 */
public interface AnnuaireOperations
{
    /**
     * Operation ajouterCollab
     */
    public void ajouterCollab(accessControl.Collab col)
        throws accessControl.Existe;

    /**
     * Operation supprimerCollab
     */
    public void supprimerCollab(String idCol)
        throws accessControl.NonExiste;

    /**
     * Operation modifierCollab
     */
    public void modifierCollab(accessControl.Collab col)
        throws accessControl.NonExiste;

    /**
     * Operation verifierPhoto
     */
    public boolean verifierPhoto(String idCollab, String photo);

    /**
     * Operation getCollab
     */
    public accessControl.Collab getCollab(String idCol)
        throws accessControl.NonExiste;

    /**
     * Operation getCollabs
     */
    public accessControl.Collab[] getCollabs();

}
