package accessControl;

/**
 * Enum definition : Habilitation
 *
 * @author OpenORB Compiler
*/
public final class Habilitation implements org.omg.CORBA.portable.IDLEntity
{
    /**
     * Enum member RH value 
     */
    public static final int _RH = 0;

    /**
     * Enum member RH
     */
    public static final Habilitation RH = new Habilitation(_RH);

    /**
     * Enum member RESP value 
     */
    public static final int _RESP = 1;

    /**
     * Enum member RESP
     */
    public static final Habilitation RESP = new Habilitation(_RESP);

    /**
     * Enum member ACCU value 
     */
    public static final int _ACCU = 2;

    /**
     * Enum member ACCU
     */
    public static final Habilitation ACCU = new Habilitation(_ACCU);

    /**
     * Enum member ANNU value 
     */
    public static final int _ANNU = 3;

    /**
     * Enum member ANNU
     */
    public static final Habilitation ANNU = new Habilitation(_ANNU);

    /**
     * Enum member HISTO value 
     */
    public static final int _HISTO = 4;

    /**
     * Enum member HISTO
     */
    public static final Habilitation HISTO = new Habilitation(_HISTO);

    /**
     * Internal member value 
     */
    private final int _Habilitation_value;

    /**
     * Private constructor
     * @param  the enum value for this new member
     */
    private Habilitation( final int value )
    {
        _Habilitation_value = value;
    }

    /**
     * Maintains singleton property for serialized enums.
     * Issue 4271: IDL/Java issue, Mapping for IDL enum.
     */
    public java.lang.Object readResolve() throws java.io.ObjectStreamException
    {
        return from_int( value() );
    }

    /**
     * Return the internal member value
     * @return the member value
     */
    public int value()
    {
        return _Habilitation_value;
    }

    /**
     * Return a enum member from its value
     * @param  an enum value
     * @return an enum member
         */
    public static Habilitation from_int(int value)
    {
        switch (value)
        {
        case 0 :
            return RH;
        case 1 :
            return RESP;
        case 2 :
            return ACCU;
        case 3 :
            return ANNU;
        case 4 :
            return HISTO;
        }
        throw new org.omg.CORBA.BAD_OPERATION();
    }

    /**
     * Return a string representation
     * @return a string representation of the enumeration
     */
    public java.lang.String toString()
    {
        switch (_Habilitation_value)
        {
        case 0 :
            return "RH";
        case 1 :
            return "RESP";
        case 2 :
            return "ACCU";
        case 3 :
            return "ANNU";
        case 4 :
            return "HISTO";
        }
        throw new org.omg.CORBA.BAD_OPERATION();
    }

}
