package accessControl.servers;

import accessControl.AutorisationPerm;
import accessControl.AutorisationTemp;
import accessControl.Collab;
import accessControl.Existe;
import accessControl.Habilitation;
import accessControl.NonExiste;
import accessControl.Porte;
import accessControl.ServiceAcceuil;
import accessControl.ServiceAuthentification;
import accessControl.ServiceAutorisation;
import accessControl.ServiceHabilitation;
import accessControl.ServiceRH;
import accessControl.Zone;
import accessControl.clients.Client;

public class ServeurInit {

	public static Porte allPortes[];

	/* Initialise les premi�res donn�es lorsqu'on lance l'application */
	public static void main(String[] args) {

		// Initialisation a l'ouverture de la fenetre et recuperation du service
		// associe
		Client client = new Client();
		ServiceAcceuil SA;
		ServiceRH SRh;
		ServiceAuthentification SAuth;
		ServiceAutorisation SAut;
		ServiceHabilitation SHab;
		Collab CollabToInsert;

		// Recuperation des services
		SA = client.getServiceAccueil();
		SRh = client.getServiceRH();
		SAuth = client.getServiceAuthentification();
		SAut = client.getServiceAutorisation();
		SHab = client.getServiceHabilitation();

		// Creation des autorisations
		final AutorisationTemp autAFlo = new AutorisationTemp();
		autAFlo.id = "T";
		autAFlo.idCollab = "P-1";
		autAFlo.idZone = 1;
		autAFlo.hDeb = "10:00";
		autAFlo.hFin = "19:00";
		autAFlo.jourDeb = "01/06/2016";
		autAFlo.jourFin = "01/07/2016";

		final AutorisationPerm autBFlo = new AutorisationPerm();
		autBFlo.id = "P";
		autBFlo.idCollab = "P-1";
		autBFlo.idZone = 2;
		autBFlo.hDeb = "10:00";
		autBFlo.hFin = "19:00";

		final AutorisationPerm autBJul = new AutorisationPerm();
		autBJul.id = "P";
		autBJul.idCollab = "P-2";
		autBJul.idZone = 2;
		autBJul.hDeb = "10:00";
		autBJul.hFin = "19:00";

		final AutorisationPerm autAPal = new AutorisationPerm();
		autAPal.id = "P";
		autAPal.idCollab = "P-3";
		autAPal.idZone = 1;
		autAPal.hDeb = "10:00";
		autAPal.hFin = "19:00";

		final AutorisationPerm autCPal = new AutorisationPerm();
		autCPal.id = "P";
		autCPal.idCollab = "P-3";
		autCPal.idZone = 3;
		autCPal.hDeb = "10:00";
		autCPal.hFin = "19:00";

		// Creation des collaborateurs permanents
		/*******************************
		 * FLORIAN PAUTOT
		 *******************************/
		CollabToInsert = new Collab();
		CollabToInsert.id = "P-1";
		CollabToInsert.nom = "Pautot";
		CollabToInsert.prenom = "Florian";
		CollabToInsert.photo = "P1.png";
		String[] idParts = CollabToInsert.id.split("-");

		try {
			SRh.ajouterCollabPerm(CollabToInsert);
		} catch (Existe e) {
			e.printStackTrace();
		}
		try {
			SAuth.definirAuthentification(CollabToInsert, "empPautot", "azerty");
		} catch (NonExiste e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		} catch (Existe e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		Habilitation hab1;
		hab1 = Habilitation.from_int(Habilitation._RH);
		Habilitation hab1bis = Habilitation.from_int(Habilitation._RESP);
		try {
			SHab.ajouterHabilitation(CollabToInsert, hab1);
			SHab.ajouterHabilitation(CollabToInsert, hab1bis);

		} catch (Existe e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		try {
			SAut.ajouterAutorisationTemp(autAFlo);
			SAut.donnerRespPourZone(Short.parseShort(idParts[1]), (short) 1);
		} catch (Existe e4) {
			e4.printStackTrace();
		}
		try {
			SAut.ajouterAutorisationPerm(autBFlo);
		} catch (Existe e4) {
			e4.printStackTrace();
		}

		/*******************************
		 * JULIEN KALFON
		 *******************************/
		CollabToInsert = new Collab();
		CollabToInsert.id = "P-2";
		CollabToInsert.nom = "Kalfon";
		CollabToInsert.prenom = "Julien";
		CollabToInsert.photo = "P2.png";
		try {
			SRh.ajouterCollabPerm(CollabToInsert);
		} catch (Existe e) {
			e.printStackTrace();
		}
		try {
			SAuth.definirAuthentification(CollabToInsert, "empKalfon", "qsdfg");
		} catch (NonExiste e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (Existe e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		Habilitation hab2;
		hab2 = Habilitation.from_int(Habilitation._ACCU);
		try {
			SHab.ajouterHabilitation(CollabToInsert, hab2);
		} catch (Existe e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		try {
			SAut.ajouterAutorisationPerm(autBJul);
		} catch (Existe e4) {
			e4.printStackTrace();
		}

		/*******************************
		 * PIERRE ANDRE LEMOINE
		 *******************************/
		CollabToInsert = new Collab();
		CollabToInsert.id = "P-3";
		CollabToInsert.nom = "Lemoine";
		CollabToInsert.prenom = "Pierre Andre";
		CollabToInsert.photo = "P3.png";
		try {
			SRh.ajouterCollabPerm(CollabToInsert);
		} catch (Existe e) {
			e.printStackTrace();
		}
		try {
			SAuth.definirAuthentification(CollabToInsert, "empLemoine", "wxcvb");
		} catch (NonExiste e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Existe e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Habilitation hab3;
		hab3 = Habilitation.from_int(Habilitation._RESP);
		try {
			SHab.ajouterHabilitation(CollabToInsert, hab3);
		} catch (Existe e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		try {
			SAut.ajouterAutorisationPerm(autAPal);

		} catch (Existe e4) {
			e4.printStackTrace();
		}
		try {
			SAut.ajouterAutorisationPerm(autCPal);
		} catch (Existe e4) {
			e4.printStackTrace();
		}

		/*******************************
		 * COLLAB TEMPORAIRES MELANIE TANGUY
		 *******************************/
		// Creation des collaborateurs temporaires
		CollabToInsert = new Collab();
		CollabToInsert.id = "T-1";
		CollabToInsert.nom = "Tanguy";
		CollabToInsert.prenom = "Melanie";
		CollabToInsert.photo = "T1.png";
		try {
			SA.ajouterCollabTemp(CollabToInsert);
		} catch (Existe e) {
			e.printStackTrace();
		}
		try {
			SAuth.definirAuthentification(CollabToInsert, "empTanguy", "melta");
		} catch (NonExiste e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Existe e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/*******************************
		 * COLLAB TEMPORAIRES
		 *******************************/
		CollabToInsert = new Collab();
		CollabToInsert.id = "T-2";
		CollabToInsert.nom = "Meca";
		CollabToInsert.prenom = "Benjamin";
		CollabToInsert.photo = "T2.png";
		try {
			SA.ajouterCollabTemp(CollabToInsert);
		} catch (Existe e) {
			e.printStackTrace();
		}

		CollabToInsert = new Collab();
		CollabToInsert.id = "T-3";
		CollabToInsert.nom = "Murat";
		CollabToInsert.prenom = "Guillaume";
		CollabToInsert.photo = "T3.png";
		try {
			SA.ajouterCollabTemp(CollabToInsert);
		} catch (Existe e) {
			e.printStackTrace();
		}

		CollabToInsert = new Collab();
		CollabToInsert.id = "T-4";
		CollabToInsert.nom = "Correia";
		CollabToInsert.prenom = "Sebastien";
		CollabToInsert.photo = "T4.png";
		try {
			SA.ajouterCollabTemp(CollabToInsert);
		} catch (Existe e) {
			e.printStackTrace();
		}

		/* Creation des zones */
		// S'effectue en dur depuis la classe Client.java en appelant la m�thode
		// getZones()

		/* Creation des portes */
		// S'effectue en dur depuis la classe Client.java en appelant la m�thode
		// getPortes()
	}

}
