package accessControl.servers;

import org.omg.CosNaming.NamingContext;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import accessControl.impl.ServiceAuthentificationImpl;

public class ServeurAuthentification {

	public static void main(String args[]) {
	    try{
	    	
	    	System.out.println("Initializing Authentification Server");

	    	 // Intialisation de l'ORB
	        //************************
	        org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init(args,null);

	        // Gestion du POA
	        //****************
	        // Recuperation du POA
	        POA rootPOA = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));

	        // Creation du servant
	        //*********************
	        ServiceAuthentificationImpl authentificationImpl = new ServiceAuthentificationImpl();
	        authentificationImpl.initDB();

	        // Activer le POA manager
	        rootPOA.the_POAManager().activate();
		        // Enregistrement dans le service de nommage
	        //*******************************************
	        // Recuperation du naming service
	        NamingContext nameRoot=org.omg.CosNaming.NamingContextHelper.narrow(orb.string_to_object("corbaloc:iiop:1.2@"+Serveur.IP_NS+":2001/NameService"));

	    	System.out.println("--- Getting Naming Service OK");

	        // Construction du nom a enregistrer
	        org.omg.CosNaming.NameComponent[] nameToRegister = new org.omg.CosNaming.NameComponent[1];
	        String nomObj = "Authentification";
	        nameToRegister[0] = new org.omg.CosNaming.NameComponent(nomObj,"");

	        // Enregistrement de l'objet CORBA dans le service de noms
	        nameRoot.rebind(nameToRegister,rootPOA.servant_to_reference(authentificationImpl));
	        System.out.println("--- '"+ nomObj + "' successfully connected to Naming Service");
	        
	        // Lancement de l'ORB et mise en attente de requete
	        //**************************************************
	        orb.run();
	    } 
	        
	      catch (Exception e) {
	        System.err.println("ERROR: " + e);
	        e.printStackTrace(System.out);
	      }
	          
	      System.out.println("Authentification Exiting ...");
	        
	  }
}
