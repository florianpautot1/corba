package accessControl;

/**
 * Holder class for : Collabs
 * 
 * @author OpenORB Compiler
 */
final public class CollabsHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal Collabs value
     */
    public accessControl.Collab[] value;

    /**
     * Default constructor
     */
    public CollabsHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public CollabsHolder(accessControl.Collab[] initial)
    {
        value = initial;
    }

    /**
     * Read Collabs from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = CollabsHelper.read(istream);
    }

    /**
     * Write Collabs into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        CollabsHelper.write(ostream,value);
    }

    /**
     * Return the Collabs TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return CollabsHelper.type();
    }

}
