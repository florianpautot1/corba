package accessControl;

/**
 * Interface definition : ServiceAuthentification
 * 
 * @author OpenORB Compiler
 */
public class _ServiceAuthentificationStub extends org.omg.CORBA.portable.ObjectImpl
        implements ServiceAuthentification
{
    static final String[] _ids_list =
    {
        "IDL:accessControl/ServiceAuthentification:1.0"
    };

    public String[] _ids()
    {
     return _ids_list;
    }

    private final static Class _opsClass = accessControl.ServiceAuthentificationOperations.class;

    /**
     * Operation verificationEmpreinte
     */
    public String verificationEmpreinte(String empreinte)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("verificationEmpreinte",true);
                    _output.write_string(empreinte);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("verificationEmpreinte",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAuthentificationOperations _self = (accessControl.ServiceAuthentificationOperations) _so.servant;
                try
                {
                    return _self.verificationEmpreinte( empreinte);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation definirAuthentification
     */
    public void definirAuthentification(accessControl.Collab e, String empreinte, String mdp)
        throws accessControl.NonExiste, accessControl.Existe
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("definirAuthentification",true);
                    accessControl.CollabHelper.write(_output,e);
                    _output.write_string(empreinte);
                    _output.write_string(mdp);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    if (_exception_id.equals(accessControl.ExisteHelper.id()))
                    {
                        throw accessControl.ExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("definirAuthentification",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAuthentificationOperations _self = (accessControl.ServiceAuthentificationOperations) _so.servant;
                try
                {
                    _self.definirAuthentification( e,  empreinte,  mdp);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation modifierEmpreinte
     */
    public void modifierEmpreinte(accessControl.Collab e, String empreinte)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("modifierEmpreinte",true);
                    accessControl.CollabHelper.write(_output,e);
                    _output.write_string(empreinte);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("modifierEmpreinte",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAuthentificationOperations _self = (accessControl.ServiceAuthentificationOperations) _so.servant;
                try
                {
                    _self.modifierEmpreinte( e,  empreinte);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation modifierMdp
     */
    public void modifierMdp(accessControl.Collab e, String mdp)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("modifierMdp",true);
                    accessControl.CollabHelper.write(_output,e);
                    _output.write_string(mdp);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("modifierMdp",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAuthentificationOperations _self = (accessControl.ServiceAuthentificationOperations) _so.servant;
                try
                {
                    _self.modifierMdp( e,  mdp);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation supprimerEmpreinte
     */
    public void supprimerEmpreinte(accessControl.Collab e)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("supprimerEmpreinte",true);
                    accessControl.CollabHelper.write(_output,e);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("supprimerEmpreinte",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAuthentificationOperations _self = (accessControl.ServiceAuthentificationOperations) _so.servant;
                try
                {
                    _self.supprimerEmpreinte( e);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation verificationMdp
     */
    public boolean verificationMdp(accessControl.Collab e, String mdp)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("verificationMdp",true);
                    accessControl.CollabHelper.write(_output,e);
                    _output.write_string(mdp);
                    _input = this._invoke(_output);
                    boolean _arg_ret = _input.read_boolean();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("verificationMdp",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAuthentificationOperations _self = (accessControl.ServiceAuthentificationOperations) _so.servant;
                try
                {
                    return _self.verificationMdp( e,  mdp);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getAuthentification
     */
    public accessControl.Authentification getAuthentification(accessControl.Collab e)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getAuthentification",true);
                    accessControl.CollabHelper.write(_output,e);
                    _input = this._invoke(_output);
                    accessControl.Authentification _arg_ret = accessControl.AuthentificationHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getAuthentification",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceAuthentificationOperations _self = (accessControl.ServiceAuthentificationOperations) _so.servant;
                try
                {
                    return _self.getAuthentification( e);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

}
