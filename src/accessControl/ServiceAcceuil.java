package accessControl;

/**
 * Interface definition : ServiceAcceuil
 * 
 * @author OpenORB Compiler
 */
public interface ServiceAcceuil extends ServiceAcceuilOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
