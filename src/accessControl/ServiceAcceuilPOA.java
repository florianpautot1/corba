package accessControl;

/**
 * Interface definition : ServiceAcceuil
 * 
 * @author OpenORB Compiler
 */
public abstract class ServiceAcceuilPOA extends org.omg.PortableServer.Servant
        implements ServiceAcceuilOperations, org.omg.CORBA.portable.InvokeHandler
{
    public ServiceAcceuil _this()
    {
        return ServiceAcceuilHelper.narrow(_this_object());
    }

    public ServiceAcceuil _this(org.omg.CORBA.ORB orb)
    {
        return ServiceAcceuilHelper.narrow(_this_object(orb));
    }

    private static String [] _ids_list =
    {
        "IDL:accessControl/ServiceAcceuil:1.0"
    };

    public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte [] objectId)
    {
        return _ids_list;
    }

    public final org.omg.CORBA.portable.OutputStream _invoke(final String opName,
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler)
    {

        if (opName.equals("ajouterCollabTemp")) {
                return _invoke_ajouterCollabTemp(_is, handler);
        } else if (opName.equals("getCollabTemp")) {
                return _invoke_getCollabTemp(_is, handler);
        } else if (opName.equals("getCollabTemps")) {
                return _invoke_getCollabTemps(_is, handler);
        } else if (opName.equals("supprimerCollabTemp")) {
                return _invoke_supprimerCollabTemp(_is, handler);
        } else {
            throw new org.omg.CORBA.BAD_OPERATION(opName);
        }
    }

    // helper methods
    private org.omg.CORBA.portable.OutputStream _invoke_ajouterCollabTemp(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);

        try
        {
            ajouterCollabTemp(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.Existe _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.ExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_supprimerCollabTemp(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        short arg0_in = _is.read_short();

        try
        {
            supprimerCollabTemp(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getCollabTemp(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        short arg0_in = _is.read_short();

        try
        {
            accessControl.Collab _arg_result = getCollabTemp(arg0_in);

            _output = handler.createReply();
            accessControl.CollabHelper.write(_output,_arg_result);

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getCollabTemps(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        accessControl.Collab[] _arg_result = getCollabTemps();

        _output = handler.createReply();
        accessControl.CollabsHelper.write(_output,_arg_result);

        return _output;
    }

}
