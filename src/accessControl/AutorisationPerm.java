package accessControl;

/**
 * Struct definition : AutorisationPerm
 * 
 * @author OpenORB Compiler
*/
public final class AutorisationPerm implements org.omg.CORBA.portable.IDLEntity
{
    @Override
	public String toString() {
		return "AutorisationPerm [id=" + id + ", idCollab=" + idCollab + ", hDeb=" + hDeb + ", hFin=" + hFin
				+ ", idZone=" + idZone + "]";
	}

	/**
     * Struct member id
     */
    public String id;

    /**
     * Struct member idCollab
     */
    public String idCollab;

    /**
     * Struct member hDeb
     */
    public String hDeb;

    /**
     * Struct member hFin
     */
    public String hFin;

    /**
     * Struct member idZone
     */
    public short idZone;

    /**
     * Default constructor
     */
    public AutorisationPerm()
    { }

    /**
     * Constructor with fields initialization
     * @param id id struct member
     * @param idCollab idCollab struct member
     * @param hDeb hDeb struct member
     * @param hFin hFin struct member
     * @param idZone idZone struct member
     */
    public AutorisationPerm(String id, String idCollab, String hDeb, String hFin, short idZone)
    {
        this.id = id;
        this.idCollab = idCollab;
        this.hDeb = hDeb;
        this.hFin = hFin;
        this.idZone = idZone;
    }

}
