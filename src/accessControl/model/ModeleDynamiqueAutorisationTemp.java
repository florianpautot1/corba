package accessControl.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import accessControl.AutorisationPerm;
import accessControl.AutorisationTemp;
import accessControl.Existe;
import accessControl.NonExiste;
import accessControl.ServiceAutorisation;
import accessControl.clients.Client;

public class ModeleDynamiqueAutorisationTemp extends AbstractTableModel {

	private LinkedList<AutorisationTemp> allAutorisations = new LinkedList<AutorisationTemp>();


	private final String[] entetes = { "Id", "Id collab", "Heure deb", "Heure Fin", "Id Zone", "Jour Deb", "Jour fin" };
	private ServiceAutorisation SA;

	public ModeleDynamiqueAutorisationTemp() {

		super();		

		// Initialisation a l'ouverture de la fenetre et recuperation du service associe
		SA = Client.getServiceAutorisation();
		
		ArrayList<AutorisationTemp> collabsTemp = new ArrayList<AutorisationTemp> (Arrays.asList(/* SA.getAllAutorisations */ ));
		if(collabsTemp.size() == 0) {		
			System.out.println("-- Aucune autorisation temporaire dans la BD.");
		}
		else {
			for (int i = 0; i < collabsTemp.size(); i++) {
				allAutorisations.add(collabsTemp.get(i));
			}
		}
    }

	public List<AutorisationTemp> getAllAutorisations() {
		return allAutorisations;
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return entetes.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return allAutorisations.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return allAutorisations.get(rowIndex).id;
		case 1:
			return allAutorisations.get(rowIndex).idCollab;
		case 2:
			return allAutorisations.get(rowIndex).hDeb;
		case 3:
			return allAutorisations.get(rowIndex).hFin;
		case 4:
			return allAutorisations.get(rowIndex).idZone;
		case 5:
			return allAutorisations.get(rowIndex).jourDeb;
		case 6:
			return allAutorisations.get(rowIndex).jourFin;
		default:
			return null;
		}
	}

	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}

	public void addAutorisationTemp(AutorisationTemp ap) {
		try {
			SA.ajouterAutorisationTemp(ap);
			String lastId = "P-" + ap.idCollab + "-" + ap.idZone;
			allAutorisations.add(ap);
			fireTableRowsInserted(allAutorisations.size() - 1, allAutorisations.size() - 1);
			fireTableDataChanged();
		} catch (Existe e) {
			System.out.println("--- [ERROR] Error adding autorisation temp : " + e.raison);
		}
	}

	public void removeAutorisationTemp(int rowIndex) {
		try {
			SA.supprimerAutorisation(getLoginSelection(rowIndex));
		} catch (NonExiste e) {
			System.out.println("--- [ERROR] Error deleting autorisation temp : " + e.raison);
		}
		allAutorisations.remove(rowIndex);
		fireTableRowsDeleted(rowIndex, rowIndex);
	}

	public String getLoginSelection(int rowIndex) {
		return allAutorisations.get(rowIndex).id;
	}

	public AutorisationTemp getAutorisationTemp(int rowIndex) {
		AutorisationTemp a = new AutorisationTemp();
		a = allAutorisations.get(rowIndex);
		try {
			a = SA.getAutorisationTemp(getLoginSelection(rowIndex));
		} catch (NonExiste e) {
			System.out.println("--- [ERROR] Error getting autorisation temp : " + e.raison);
		}
		return a;
	}
	
	public void setAllAutorisations(AutorisationTemp[] lesAut) {
		allAutorisations = new LinkedList<AutorisationTemp>(Arrays.asList(lesAut));

	}
	
	public void modifyAutorisationTemp(int rowIndex, AutorisationTemp at){
		allAutorisations.get(rowIndex).hDeb = at.hDeb;
		allAutorisations.get(rowIndex).hFin = at.hFin;
		allAutorisations.get(rowIndex).jourDeb = at.jourDeb;
		allAutorisations.get(rowIndex).jourFin = at.jourFin;
		
    	try {
    		SA.modifierAutorisationTemp(at);
		} catch (NonExiste e) {
			System.out.println("--- [ERROR] Error updating autorisation temp : " + e.raison);
		}
    	fireTableDataChanged();
    }
}
