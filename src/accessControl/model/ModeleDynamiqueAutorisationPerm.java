package accessControl.model;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import accessControl.AutorisationPerm;
import accessControl.AutorisationTemp;
import accessControl.Collab;
import accessControl.Existe;
import accessControl.NonExiste;
import accessControl.ServiceAcceuil;
import accessControl.ServiceAutorisation;
import accessControl.ServiceRH;
import accessControl.clients.Client;
import accessControl.metier.CollabPerm;

public class ModeleDynamiqueAutorisationPerm extends AbstractTableModel{
	
	private LinkedList<AutorisationPerm> allAutorisations = new LinkedList<AutorisationPerm>();
    
	public LinkedList<AutorisationPerm> getAllAutorisations() {
		return allAutorisations;
	}

	public void setAllAutorisations(AutorisationPerm[] lesAut) {
		allAutorisations = new LinkedList<AutorisationPerm>(Arrays.asList(lesAut));
	}

	private final String[] entetes = {"Id", "Id Collab", "Heure deb", "Heure fin", "Id Zone"};
    private ServiceAutorisation SA;
	
	public ModeleDynamiqueAutorisationPerm() {

		super();		

		// Initialisation a l'ouverture de la fenetre et recuperation du service associe
		SA = Client.getServiceAutorisation();
		
		ArrayList<AutorisationPerm> collabsTemp = new ArrayList<AutorisationPerm> (Arrays.asList(/* SA.getAllAutorisations */ ));
		if(collabsTemp.size() == 0) {		
			System.out.println("-- Aucune autorisation temporaire dans la BD.");
		}
		else {
			for (int i = 0; i < collabsTemp.size(); i++) {
				allAutorisations.add(collabsTemp.get(i));
			}
		}
    }
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return entetes.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return allAutorisations.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
        case 0:
            return allAutorisations.get(rowIndex).id;
        case 1:
            return allAutorisations.get(rowIndex).idCollab;
        case 2:
            return allAutorisations.get(rowIndex).hDeb;
        case 3:
            return allAutorisations.get(rowIndex).hFin;
        case 4:
            return allAutorisations.get(rowIndex).idZone;
        default:
            return null;
		}
	}

	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}

    public void addAutorisationPerm(AutorisationPerm ap) {
    	try {
			SA.ajouterAutorisationPerm(ap);
			String lastId = "P-"+ap.idCollab+"-"+ap.idZone;
			allAutorisations.add(ap);
			fireTableRowsInserted(allAutorisations.size() -1, allAutorisations.size() -1);
			fireTableDataChanged();
		} catch (Existe e) {
			System.out.println("--- [ERROR] Error adding autorisation perm : " + e.raison);
		}
    }
 
    public void removeAutorisationPerm(int rowIndex) {
    	try {
			SA.supprimerAutorisation(getLoginSelection(rowIndex));
		} catch (NonExiste e) {
			System.out.println("--- [ERROR] Error adding deleting perm : " + e.raison);
		}
    	allAutorisations.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }
	
	public String getLoginSelection(int rowIndex) {
		return allAutorisations.get(rowIndex).id;
	}
	
	
	public AutorisationPerm getAutorisationPerm(int rowIndex) {
		AutorisationPerm a = new AutorisationPerm();
		a = allAutorisations.get(rowIndex);
		try {
			a = SA.getAutorisationPerm(getLoginSelection(rowIndex));
		} catch (NonExiste e) {
			System.out.println("--- [ERROR] Error getting autorisation perm : " + e.raison);
		
		}
		return a;
	}
	
	public void modifyAutorisationPerm(int rowIndex, AutorisationPerm ap){
		allAutorisations.get(rowIndex).hDeb = ap.hDeb;
		allAutorisations.get(rowIndex).hFin = ap.hFin;
    	try {
    		SA.modifierAutorisationPerm(ap);
		} catch (NonExiste e) {
			System.out.println("--- [ERROR] Error updating autorisation perm");
		
		}
    	fireTableDataChanged();
    }
}
