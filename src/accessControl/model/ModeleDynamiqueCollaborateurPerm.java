package accessControl.model;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import accessControl.Collab;
import accessControl.Existe;
import accessControl.NonExiste;
import accessControl.ServiceRH;
import accessControl.clients.Client;
import accessControl.metier.CollabPerm;

public class ModeleDynamiqueCollaborateurPerm extends AbstractTableModel{
	
	private final List<Collab> allCollabPerm = new ArrayList<Collab>();
    private final String[] entetes = {"Id", "Prenom", "Nom", "Photo"};
    private ServiceRH SRH;
	
	public ModeleDynamiqueCollaborateurPerm() {

		super();
		    
		// Initialisation � l'ouverture de la fen�tre et r�cup�ration du service associ�
		SRH = Client.getServiceRH();
		
		ArrayList<Collab> collabsPerm = new ArrayList<Collab> (Arrays.asList(SRH.getCollabPerms()));
		if(collabsPerm.size() == 0) {		
			System.out.println("---Aucun collaborateur permanent dans la BD.");
		}
		else {
			for (int i = 0; i < collabsPerm.size(); i++) {
				allCollabPerm.add(collabsPerm.get(i));
			}
		}
    }
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return entetes.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return allCollabPerm.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
        case 0:
        	String splitId[] = allCollabPerm.get(rowIndex).id.split("-");
            return splitId[1];
        case 1:
            return allCollabPerm.get(rowIndex).prenom;
        case 2:
            return allCollabPerm.get(rowIndex).nom;
        case 3:
            return allCollabPerm.get(rowIndex).photo;
        default:
            return null;
		}
	}
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
	    return true; //Toutes les cellules �ditables
	}

	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}

    public void addCollabPerm(Collab c) {
    	try {
			SRH.ajouterCollabPerm(c);
			String lastId = this.getLastIDPermInserted();
			String[] splitId = lastId.split("-");
			short id = Short.parseShort(splitId[1]);
			try {
				Collab lastCollabPerm = SRH.getCollabPerm(id);
				allCollabPerm.add(lastCollabPerm);
		        fireTableRowsInserted(allCollabPerm.size() -1, allCollabPerm.size() -1);
		    	fireTableDataChanged();
			} catch (NonExiste e) {
				System.out.println("--- [ERROR] Error adding collab perm : " + e.raison);
			}
		} catch (Existe e) {
			System.out.println("--- [ERROR] Error adding collab perm : " + e.raison);

		}
    }
 
    public void removeCollabPerm(int rowIndex) {
    	try {
    		String[] splitId = getLoginSelection(rowIndex).split("-");
    		short id = Short.parseShort(splitId[1]);
			SRH.supprimerCollabPerm(id);
		} catch (NonExiste e) {
			System.out.println("--- [ERROR] Error deleting collab perm : " + e.raison);

		}
    	allCollabPerm.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }
    public void modifyCollabPerm(int rowIndex, CollabPerm c){
    	allCollabPerm.get(rowIndex).nom = c.nom;
    	allCollabPerm.get(rowIndex).prenom = c.prenom;
    	allCollabPerm.get(rowIndex).photo = c.photo;
    	try {
			SRH.modifierCollabPerm(c);
		} catch (NonExiste e) {
			System.out.println("--- [ERROR] Error updating collab perm : " + e.raison);

		}
    	fireTableDataChanged();
    }
	public String getLoginSelection(int rowIndex) {
		return allCollabPerm.get(rowIndex).id;
	}
	
	public String getLastIDPermInserted() {
		
		Collab allCollabPerm[];
		ServiceRH SRH = Client.getServiceRH();
		String lastIDInserted;
		
		allCollabPerm = SRH.getCollabPerms();
		lastIDInserted = allCollabPerm[allCollabPerm.length-1].id;
		return lastIDInserted;
	}
	public Collab getCollabPerm(int rowIndex) {
		Collab c = new Collab();
		c = allCollabPerm.get(rowIndex);
		String[] splitId = getLoginSelection(rowIndex).split("-");
		short id = Short.parseShort(splitId[1]);
		try {
			c = SRH.getCollabPerm(id);
		} catch (NonExiste e) {
			System.out.println("--- [ERROR] Error getting collab perm : " + e.raison);

		}
		return c;
	}
}
