package accessControl.model;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import accessControl.Collab;
import accessControl.NonExiste;
import accessControl.Passage;
import accessControl.Porte;
import accessControl.ServiceAcceuil;
import accessControl.ServiceHistorisation;
import accessControl.ServiceRH;
import accessControl.Zone;
import accessControl.clients.Client;

public class ModeleDynamiquePassage extends AbstractTableModel{
	
	private final List<Passage> allPassages = new ArrayList<Passage>();
    private final String[] entetes = {"Collab.", "Date", "Porte - Zone", "Autorisation"};
	private ServiceHistorisation SH;
	private ServiceRH SRh;
	private ServiceAcceuil SAccu;
	
	public ModeleDynamiquePassage() {

		super();		

		// Initialisation a l'ouverture de la fenetre et recuperation du service associe
		SH = Client.getServiceHistorisation();
		SRh = Client.getServiceRH();
		SAccu = Client.getServiceAccueil();
		
		ArrayList<Passage> passages = new ArrayList<Passage> (Arrays.asList(SH.getPassages()));
		if(passages.size() == 0) {		
			System.out.println("-- Aucun passage dans la BD.");
		}
		else {
			for (int i = 0; i < passages.size(); i++) {
				allPassages.add(passages.get(i));
			}
		}
    }
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return entetes.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return allPassages.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
        case 0:
        	Collab c = allPassages.get(rowIndex).col;
        	
        	/* R�cup�ration du collaborateur */
        	String[] idParts = c.id.split("-");
			Collab collab;
			if (idParts[0].contains("P")){
				try {
					collab = SRh.getCollabPerm(Short.parseShort(idParts[1]));
					Client.setCollab(collab);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NonExiste e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					collab = SAccu.getCollabTemp(Short.parseShort(idParts[1]));
					Client.setCollab(collab);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NonExiste e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
        	String resC = c.id + " (" + Client.getCollab().prenom + " " + Client.getCollab().nom +")";
        	return resC;
        case 1:
            return allPassages.get(rowIndex).timestamp;
        case 2:
        	Porte p = allPassages.get(rowIndex).p;
        	Zone z = p.z;
        	String resPZ = Client.getPorte(p.id).name + " - " + Client.getZone(z.id).name;
            return resPZ;
        case 3:
        	if (allPassages.get(rowIndex).autorise== true)
        		return "Authorized";
        	else
        		return "Denied";
        default:
            return null;
		}
	}

	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}
}
