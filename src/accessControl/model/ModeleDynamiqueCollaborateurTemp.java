package accessControl.model;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import accessControl.Collab;
import accessControl.Existe;
import accessControl.NonExiste;
import accessControl.ServiceAcceuil;
import accessControl.ServiceRH;
import accessControl.clients.Client;

public class ModeleDynamiqueCollaborateurTemp extends AbstractTableModel{
	
	private final List<Collab> allCollabTemp = new ArrayList<Collab>();
    private final String[] entetes = {"Id", "Prenom", "Nom", "Photo"};
    private ServiceAcceuil SA;
	
	public ModeleDynamiqueCollaborateurTemp() {

		super();		

		// Initialisation a l'ouverture de la fenetre et recuperation du service associe
		SA = Client.getServiceAccueil();
		
		ArrayList<Collab> collabsTemp = new ArrayList<Collab> (Arrays.asList(SA.getCollabTemps()));
		if(collabsTemp.size() == 0) {		
			System.out.println("-- Aucun collaborateur temporaire dans la BD.");
		}
		else {
			for (int i = 0; i < collabsTemp.size(); i++) {
				allCollabTemp.add(collabsTemp.get(i));
			}
		}
    }
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return entetes.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return allCollabTemp.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
        case 0:
        	String splitId[] = allCollabTemp.get(rowIndex).id.split("-");
            return splitId[1];
        case 1:
            return allCollabTemp.get(rowIndex).prenom;
        case 2:
            return allCollabTemp.get(rowIndex).nom;
        case 3:
            return allCollabTemp.get(rowIndex).photo;
        default:
            return null;
		}
	}

	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}

    public void addCollabTemp(Collab c) {
    	try {
			SA.ajouterCollabTemp(c);
			String lastId = this.getLastIDTempInserted();
			String[] splitId = lastId.split("-");
			short id = Short.parseShort(splitId[1]);
			try {
				Collab lastCollabTemp = SA.getCollabTemp(id);
				allCollabTemp.add(lastCollabTemp);
		        fireTableRowsInserted(allCollabTemp.size() -1, allCollabTemp.size() -1);
		    	fireTableDataChanged();
			} catch (NonExiste e) {
				System.out.println("--- [ERROR] Error adding collab temp : " + e.raison);

			}
		} catch (Existe e) {
			System.out.println("--- [ERROR] Error adding collab temp : " + e.raison);

		}
    }
 
    public void removeCollabTemp(int rowIndex) {
    	try {
    		String[] splitId = getLoginSelection(rowIndex).split("-");
    		short id = Short.parseShort(splitId[1]);
			SA.supprimerCollabTemp(id);
		} catch (NonExiste e) {
			System.out.println("--- [ERROR] Error deleting collab temp : " + e.raison);

		}
    	allCollabTemp.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }
	
	public String getLoginSelection(int rowIndex) {
		return allCollabTemp.get(rowIndex).id;
	}
	
	public String getLastIDTempInserted() {
		
		Collab allCollabTemp[];
		ServiceAcceuil SAcc = Client.getServiceAccueil();
		String lastIDInserted;
		
		allCollabTemp = SAcc.getCollabTemps();
		lastIDInserted = allCollabTemp[allCollabTemp.length-1].id;
		return lastIDInserted;
	}
	public Collab getCollabTemp(int rowIndex) {
		Collab c = new Collab();
		c = allCollabTemp.get(rowIndex);
		String[] splitId = getLoginSelection(rowIndex).split("-");
		short id = Short.parseShort(splitId[1]);
		try {
			c = SA.getCollabTemp(id);
		} catch (NonExiste e) {
			System.out.println("--- [ERROR] Error getting collab temp : " + e.raison);

		}
		return c;
	}
}
