package accessControl;

/**
 * Exception definition : NonExiste
 * 
 * @author OpenORB Compiler
 */
public final class NonExiste extends org.omg.CORBA.UserException
{
    /**
     * Exception member raison
     */
    public String raison;

    /**
     * Default constructor
     */
    public NonExiste()
    {
        super(NonExisteHelper.id());
    }

    /**
     * Constructor with fields initialization
     * @param raison raison exception member
     */
    public NonExiste(String raison)
    {
        super(NonExisteHelper.id());
        this.raison = raison;
    }

    /**
     * Full constructor with fields initialization
     * @param raison raison exception member
     */
    public NonExiste(String orb_reason, String raison)
    {
        super(NonExisteHelper.id() +" " +  orb_reason);
        this.raison = raison;
    }

}
