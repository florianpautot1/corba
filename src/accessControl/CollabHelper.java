package accessControl;

/** 
 * Helper class for : Collab
 *  
 * @author OpenORB Compiler
 */ 
public class CollabHelper
{
    private static final boolean HAS_OPENORB;
    static {
        boolean hasOpenORB = false;
        try {
            Thread.currentThread().getContextClassLoader().loadClass("org.openorb.CORBA.Any");
            hasOpenORB = true;
        }
        catch(ClassNotFoundException ex) {
        }
        HAS_OPENORB = hasOpenORB;
    }
    /**
     * Insert Collab into an any
     * @param a an any
     * @param t Collab value
     */
    public static void insert(org.omg.CORBA.Any a, accessControl.Collab t)
    {
        a.insert_Streamable(new accessControl.CollabHolder(t));
    }

    /**
     * Extract Collab from an any
     * @param a an any
     * @return the extracted Collab value
     */
    public static accessControl.Collab extract(org.omg.CORBA.Any a)
    {
        if (!a.type().equal(type()))
            throw new org.omg.CORBA.MARSHAL();
        if (HAS_OPENORB && a instanceof org.openorb.CORBA.Any) {
            // streamable extraction. The jdk stubs incorrectly define the Any stub
            org.openorb.CORBA.Any any = (org.openorb.CORBA.Any)a;
            try {
                org.omg.CORBA.portable.Streamable s = any.extract_Streamable();
                if(s instanceof accessControl.CollabHolder)
                    return ((accessControl.CollabHolder)s).value;
            } catch (org.omg.CORBA.BAD_INV_ORDER ex) {
            }
            accessControl.CollabHolder h = new accessControl.CollabHolder(read(a.create_input_stream()));
            a.insert_Streamable(h);
            return h.value;
        }
        return read(a.create_input_stream());
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;
    private static boolean _working = false;

    /**
     * Return the Collab TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            synchronized(org.omg.CORBA.TypeCode.class) {
                if (_tc != null)
                    return _tc;
                if (_working)
                    return org.omg.CORBA.ORB.init().create_recursive_tc(id());
                _working = true;
                org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
                org.omg.CORBA.StructMember []_members = new org.omg.CORBA.StructMember[4];

                _members[0] = new org.omg.CORBA.StructMember();
                _members[0].name = "id";
                _members[0].type = orb.get_primitive_tc(org.omg.CORBA.TCKind.tk_string);
                _members[1] = new org.omg.CORBA.StructMember();
                _members[1].name = "nom";
                _members[1].type = orb.get_primitive_tc(org.omg.CORBA.TCKind.tk_string);
                _members[2] = new org.omg.CORBA.StructMember();
                _members[2].name = "prenom";
                _members[2].type = orb.get_primitive_tc(org.omg.CORBA.TCKind.tk_string);
                _members[3] = new org.omg.CORBA.StructMember();
                _members[3].name = "photo";
                _members[3].type = orb.get_primitive_tc(org.omg.CORBA.TCKind.tk_string);
                _tc = orb.create_struct_tc(id(),"Collab",_members);
                _working = false;
            }
        }
        return _tc;
    }

    /**
     * Return the Collab IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:accessControl/Collab:1.0";

    /**
     * Read Collab from a marshalled stream
     * @param istream the input stream
     * @return the readed Collab value
     */
    public static accessControl.Collab read(org.omg.CORBA.portable.InputStream istream)
    {
        accessControl.Collab new_one = new accessControl.Collab();

        new_one.id = istream.read_string();
        new_one.nom = istream.read_string();
        new_one.prenom = istream.read_string();
        new_one.photo = istream.read_string();

        return new_one;
    }

    /**
     * Write Collab into a marshalled stream
     * @param ostream the output stream
     * @param value Collab value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, accessControl.Collab value)
    {
        ostream.write_string(value.id);
        ostream.write_string(value.nom);
        ostream.write_string(value.prenom);
        ostream.write_string(value.photo);
    }

}
