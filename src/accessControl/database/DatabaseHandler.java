package accessControl.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseHandler {

	Connection conn = null;
	
	public void connectDatabase(String name) throws ClassNotFoundException, SQLException
	{
	        Class.forName("org.h2.Driver");
	        conn = DriverManager.
	            getConnection("jdbc:h2:~/"+name, "sa", "");
	}
	
	public Connection getConn() {
		return conn;
	}
	
	public void executeQuery(String sql) throws SQLException
	{
		Statement st = getConn().createStatement();
		st.execute(sql);
		getConn().commit();
	}

}
