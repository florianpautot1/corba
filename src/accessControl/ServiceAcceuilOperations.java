package accessControl;

/**
 * Interface definition : ServiceAcceuil
 * 
 * @author OpenORB Compiler
 */
public interface ServiceAcceuilOperations
{
    /**
     * Operation ajouterCollabTemp
     */
    public void ajouterCollabTemp(accessControl.Collab col)
        throws accessControl.Existe;

    /**
     * Operation supprimerCollabTemp
     */
    public void supprimerCollabTemp(short idCol)
        throws accessControl.NonExiste;

    /**
     * Operation getCollabTemp
     */
    public accessControl.Collab getCollabTemp(short idCol)
        throws accessControl.NonExiste;

    /**
     * Operation getCollabTemps
     */
    public accessControl.Collab[] getCollabTemps();

}
