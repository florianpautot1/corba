package accessControl;

/** 
 * Helper class for : ServiceAcceuil
 *  
 * @author OpenORB Compiler
 */ 
public class ServiceAcceuilHelper
{
    /**
     * Insert ServiceAcceuil into an any
     * @param a an any
     * @param t ServiceAcceuil value
     */
    public static void insert(org.omg.CORBA.Any a, accessControl.ServiceAcceuil t)
    {
        a.insert_Object(t , type());
    }

    /**
     * Extract ServiceAcceuil from an any
     * @param a an any
     * @return the extracted ServiceAcceuil value
     */
    public static accessControl.ServiceAcceuil extract(org.omg.CORBA.Any a)
    {
        if (!a.type().equal(type()))
            throw new org.omg.CORBA.MARSHAL();
        try {
            return accessControl.ServiceAcceuilHelper.narrow(a.extract_Object());
        } catch (final org.omg.CORBA.BAD_PARAM e) {
            throw new org.omg.CORBA.MARSHAL(e.getMessage());
        }
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the ServiceAcceuil TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            _tc = orb.create_interface_tc(id(),"ServiceAcceuil");
        }
        return _tc;
    }

    /**
     * Return the ServiceAcceuil IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:accessControl/ServiceAcceuil:1.0";

    /**
     * Read ServiceAcceuil from a marshalled stream
     * @param istream the input stream
     * @return the readed ServiceAcceuil value
     */
    public static accessControl.ServiceAcceuil read(org.omg.CORBA.portable.InputStream istream)
    {
        return(accessControl.ServiceAcceuil)istream.read_Object(accessControl._ServiceAcceuilStub.class);
    }

    /**
     * Write ServiceAcceuil into a marshalled stream
     * @param ostream the output stream
     * @param value ServiceAcceuil value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, accessControl.ServiceAcceuil value)
    {
        ostream.write_Object((org.omg.CORBA.portable.ObjectImpl)value);
    }

    /**
     * Narrow CORBA::Object to ServiceAcceuil
     * @param obj the CORBA Object
     * @return ServiceAcceuil Object
     */
    public static ServiceAcceuil narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof ServiceAcceuil)
            return (ServiceAcceuil)obj;

        if (obj._is_a(id()))
        {
            _ServiceAcceuilStub stub = new _ServiceAcceuilStub();
            stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
            return stub;
        }

        throw new org.omg.CORBA.BAD_PARAM();
    }

    /**
     * Unchecked Narrow CORBA::Object to ServiceAcceuil
     * @param obj the CORBA Object
     * @return ServiceAcceuil Object
     */
    public static ServiceAcceuil unchecked_narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof ServiceAcceuil)
            return (ServiceAcceuil)obj;

        _ServiceAcceuilStub stub = new _ServiceAcceuilStub();
        stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
        return stub;

    }

}
