package accessControl;

/**
 * Interface definition : ServiceHistorisation
 * 
 * @author OpenORB Compiler
 */
public class ServiceHistorisationPOATie extends ServiceHistorisationPOA
{

    //
    // Private reference to implementation object
    //
    private ServiceHistorisationOperations _tie;

    //
    // Private reference to POA
    //
    private org.omg.PortableServer.POA _poa;

    /**
     * Constructor
     */
    public ServiceHistorisationPOATie(ServiceHistorisationOperations tieObject)
    {
        _tie = tieObject;
    }

    /**
     * Constructor
     */
    public ServiceHistorisationPOATie(ServiceHistorisationOperations tieObject, org.omg.PortableServer.POA poa)
    {
        _tie = tieObject;
        _poa = poa;
    }

    /**
     * Get the delegate
     */
    public ServiceHistorisationOperations _delegate()
    {
        return _tie;
    }

    /**
     * Set the delegate
     */
    public void _delegate(ServiceHistorisationOperations delegate_)
    {
        _tie = delegate_;
    }

    /**
     * _default_POA method
     */
    public org.omg.PortableServer.POA _default_POA()
    {
        if (_poa != null)
            return _poa;
        else
            return super._default_POA();
    }

    /**
     * Operation sauvegarderPassage
     */
    public void sauvegarderPassage(accessControl.Passage p)
    {
        _tie.sauvegarderPassage( p);
    }

    /**
     * Operation getPassages
     */
    public accessControl.Passage[] getPassages()
    {
        return _tie.getPassages();
    }

    /**
     * Operation getPassagesParZones
     */
    public accessControl.Passage[] getPassagesParZones(short idZone)
    {
        return _tie.getPassagesParZones( idZone);
    }

}
