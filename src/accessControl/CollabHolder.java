package accessControl;

/**
 * Holder class for : Collab
 * 
 * @author OpenORB Compiler
 */
final public class CollabHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal Collab value
     */
    public accessControl.Collab value;

    /**
     * Default constructor
     */
    public CollabHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public CollabHolder(accessControl.Collab initial)
    {
        value = initial;
    }

    /**
     * Read Collab from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = CollabHelper.read(istream);
    }

    /**
     * Write Collab into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        CollabHelper.write(ostream,value);
    }

    /**
     * Return the Collab TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return CollabHelper.type();
    }

}
