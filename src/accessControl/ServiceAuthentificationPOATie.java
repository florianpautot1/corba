package accessControl;

/**
 * Interface definition : ServiceAuthentification
 * 
 * @author OpenORB Compiler
 */
public class ServiceAuthentificationPOATie extends ServiceAuthentificationPOA
{

    //
    // Private reference to implementation object
    //
    private ServiceAuthentificationOperations _tie;

    //
    // Private reference to POA
    //
    private org.omg.PortableServer.POA _poa;

    /**
     * Constructor
     */
    public ServiceAuthentificationPOATie(ServiceAuthentificationOperations tieObject)
    {
        _tie = tieObject;
    }

    /**
     * Constructor
     */
    public ServiceAuthentificationPOATie(ServiceAuthentificationOperations tieObject, org.omg.PortableServer.POA poa)
    {
        _tie = tieObject;
        _poa = poa;
    }

    /**
     * Get the delegate
     */
    public ServiceAuthentificationOperations _delegate()
    {
        return _tie;
    }

    /**
     * Set the delegate
     */
    public void _delegate(ServiceAuthentificationOperations delegate_)
    {
        _tie = delegate_;
    }

    /**
     * _default_POA method
     */
    public org.omg.PortableServer.POA _default_POA()
    {
        if (_poa != null)
            return _poa;
        else
            return super._default_POA();
    }

    /**
     * Operation verificationEmpreinte
     */
    public String verificationEmpreinte(String empreinte)
    {
        return _tie.verificationEmpreinte( empreinte);
    }

    /**
     * Operation definirAuthentification
     */
    public void definirAuthentification(accessControl.Collab e, String empreinte, String mdp)
        throws accessControl.NonExiste, accessControl.Existe
    {
        _tie.definirAuthentification( e,  empreinte,  mdp);
    }

    /**
     * Operation modifierEmpreinte
     */
    public void modifierEmpreinte(accessControl.Collab e, String empreinte)
        throws accessControl.NonExiste
    {
        _tie.modifierEmpreinte( e,  empreinte);
    }

    /**
     * Operation modifierMdp
     */
    public void modifierMdp(accessControl.Collab e, String mdp)
        throws accessControl.NonExiste
    {
        _tie.modifierMdp( e,  mdp);
    }

    /**
     * Operation supprimerEmpreinte
     */
    public void supprimerEmpreinte(accessControl.Collab e)
        throws accessControl.NonExiste
    {
        _tie.supprimerEmpreinte( e);
    }

    /**
     * Operation verificationMdp
     */
    public boolean verificationMdp(accessControl.Collab e, String mdp)
    {
        return _tie.verificationMdp( e,  mdp);
    }

    /**
     * Operation getAuthentification
     */
    public accessControl.Authentification getAuthentification(accessControl.Collab e)
        throws accessControl.NonExiste
    {
        return _tie.getAuthentification( e);
    }

}
