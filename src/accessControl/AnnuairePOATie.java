package accessControl;

/**
 * Interface definition : Annuaire
 * 
 * @author OpenORB Compiler
 */
public class AnnuairePOATie extends AnnuairePOA
{

    //
    // Private reference to implementation object
    //
    private AnnuaireOperations _tie;

    //
    // Private reference to POA
    //
    private org.omg.PortableServer.POA _poa;

    /**
     * Constructor
     */
    public AnnuairePOATie(AnnuaireOperations tieObject)
    {
        _tie = tieObject;
    }

    /**
     * Constructor
     */
    public AnnuairePOATie(AnnuaireOperations tieObject, org.omg.PortableServer.POA poa)
    {
        _tie = tieObject;
        _poa = poa;
    }

    /**
     * Get the delegate
     */
    public AnnuaireOperations _delegate()
    {
        return _tie;
    }

    /**
     * Set the delegate
     */
    public void _delegate(AnnuaireOperations delegate_)
    {
        _tie = delegate_;
    }

    /**
     * _default_POA method
     */
    public org.omg.PortableServer.POA _default_POA()
    {
        if (_poa != null)
            return _poa;
        else
            return super._default_POA();
    }

    /**
     * Operation ajouterCollab
     */
    public void ajouterCollab(accessControl.Collab col)
        throws accessControl.Existe
    {
        _tie.ajouterCollab( col);
    }

    /**
     * Operation supprimerCollab
     */
    public void supprimerCollab(String idCol)
        throws accessControl.NonExiste
    {
        _tie.supprimerCollab( idCol);
    }

    /**
     * Operation modifierCollab
     */
    public void modifierCollab(accessControl.Collab col)
        throws accessControl.NonExiste
    {
        _tie.modifierCollab( col);
    }

    /**
     * Operation verifierPhoto
     */
    public boolean verifierPhoto(String idCollab, String photo)
    {
        return _tie.verifierPhoto( idCollab,  photo);
    }

    /**
     * Operation getCollab
     */
    public accessControl.Collab getCollab(String idCol)
        throws accessControl.NonExiste
    {
        return _tie.getCollab( idCol);
    }

    /**
     * Operation getCollabs
     */
    public accessControl.Collab[] getCollabs()
    {
        return _tie.getCollabs();
    }

}
