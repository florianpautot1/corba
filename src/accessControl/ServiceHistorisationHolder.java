package accessControl;

/**
 * Holder class for : ServiceHistorisation
 * 
 * @author OpenORB Compiler
 */
final public class ServiceHistorisationHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal ServiceHistorisation value
     */
    public accessControl.ServiceHistorisation value;

    /**
     * Default constructor
     */
    public ServiceHistorisationHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public ServiceHistorisationHolder(accessControl.ServiceHistorisation initial)
    {
        value = initial;
    }

    /**
     * Read ServiceHistorisation from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = ServiceHistorisationHelper.read(istream);
    }

    /**
     * Write ServiceHistorisation into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        ServiceHistorisationHelper.write(ostream,value);
    }

    /**
     * Return the ServiceHistorisation TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return ServiceHistorisationHelper.type();
    }

}
