package accessControl;

/**
 * Holder class for : Habilitation
 * 
 * @author OpenORB Compiler
 */
final public class HabilitationHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal Habilitation value
     */
    public accessControl.Habilitation value;

    /**
     * Default constructor
     */
    public HabilitationHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public HabilitationHolder(accessControl.Habilitation initial)
    {
        value = initial;
    }

    /**
     * Read Habilitation from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = HabilitationHelper.read(istream);
    }

    /**
     * Write Habilitation into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        HabilitationHelper.write(ostream,value);
    }

    /**
     * Return the Habilitation TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return HabilitationHelper.type();
    }

}
