package accessControl;

/**
 * Interface definition : ServiceAuthentification
 * 
 * @author OpenORB Compiler
 */
public interface ServiceAuthentificationOperations
{
    /**
     * Operation verificationEmpreinte
     */
    public String verificationEmpreinte(String empreinte);

    /**
     * Operation definirAuthentification
     */
    public void definirAuthentification(accessControl.Collab e, String empreinte, String mdp)
        throws accessControl.NonExiste, accessControl.Existe;

    /**
     * Operation modifierEmpreinte
     */
    public void modifierEmpreinte(accessControl.Collab e, String empreinte)
        throws accessControl.NonExiste;

    /**
     * Operation modifierMdp
     */
    public void modifierMdp(accessControl.Collab e, String mdp)
        throws accessControl.NonExiste;

    /**
     * Operation supprimerEmpreinte
     */
    public void supprimerEmpreinte(accessControl.Collab e)
        throws accessControl.NonExiste;

    /**
     * Operation verificationMdp
     */
    public boolean verificationMdp(accessControl.Collab e, String mdp);

    /**
     * Operation getAuthentification
     */
    public accessControl.Authentification getAuthentification(accessControl.Collab e)
        throws accessControl.NonExiste;

}
