package accessControl;

/** 
 * Helper class for : Habilitation
 *  
 * @author OpenORB Compiler
 */ 
public class HabilitationHelper
{
    /**
     * Insert Habilitation into an any
     * @param a an any
     * @param t Habilitation value
     */
    public static void insert(org.omg.CORBA.Any a, accessControl.Habilitation t)
    {
        a.type(type());
        write(a.create_output_stream(),t);
    }

    /**
     * Extract Habilitation from an any
     * @param a an any
     * @return the extracted Habilitation value
     */
    public static accessControl.Habilitation extract(org.omg.CORBA.Any a)
    {
        if (!a.type().equal(type()))
            throw new org.omg.CORBA.MARSHAL();
        return read(a.create_input_stream());
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the Habilitation TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            String []_members = new String[5];
            _members[0] = "RH";
            _members[1] = "RESP";
            _members[2] = "ACCU";
            _members[3] = "ANNU";
            _members[4] = "HISTO";
            _tc = orb.create_enum_tc(id(),"Habilitation",_members);
        }
        return _tc;
    }

    /**
     * Return the Habilitation IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:accessControl/Habilitation:1.0";

    /**
     * Read Habilitation from a marshalled stream
     * @param istream the input stream
     * @return the readed Habilitation value
     */
    public static accessControl.Habilitation read(org.omg.CORBA.portable.InputStream istream)
    {
        return Habilitation.from_int(istream.read_ulong());
    }

    /**
     * Write Habilitation into a marshalled stream
     * @param ostream the output stream
     * @param value Habilitation value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, accessControl.Habilitation value)
    {
        ostream.write_ulong(value.value());
    }

}
