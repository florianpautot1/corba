package accessControl;

/** 
 * Helper class for : Collabs
 *  
 * @author OpenORB Compiler
 */ 
public class CollabsHelper
{
    private static final boolean HAS_OPENORB;
    static {
        boolean hasOpenORB = false;
        try {
            Thread.currentThread().getContextClassLoader().loadClass("org.openorb.CORBA.Any");
            hasOpenORB = true;
        }
        catch(ClassNotFoundException ex) {
        }
        HAS_OPENORB = hasOpenORB;
    }
    /**
     * Insert Collabs into an any
     * @param a an any
     * @param t Collabs value
     */
    public static void insert(org.omg.CORBA.Any a, accessControl.Collab[] t)
    {
        a.insert_Streamable(new accessControl.CollabsHolder(t));
    }

    /**
     * Extract Collabs from an any
     * @param a an any
     * @return the extracted Collabs value
     */
    public static accessControl.Collab[] extract(org.omg.CORBA.Any a)
    {
        if (!a.type().equal(type()))
            throw new org.omg.CORBA.MARSHAL();
        if(HAS_OPENORB && a instanceof org.openorb.CORBA.Any) {
            // streamable extraction. The jdk stubs incorrectly define the Any stub
            org.openorb.CORBA.Any any = (org.openorb.CORBA.Any)a;
            try {
                org.omg.CORBA.portable.Streamable s = any.extract_Streamable();
                if(s instanceof accessControl.CollabsHolder)
                    return ((accessControl.CollabsHolder)s).value;
            } catch (org.omg.CORBA.BAD_INV_ORDER ex) {
            }
            accessControl.CollabsHolder h = new accessControl.CollabsHolder(read(a.create_input_stream()));
            a.insert_Streamable(h);
            return h.value;
        }
        return read(a.create_input_stream());
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the Collabs TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            _tc = orb.create_alias_tc(id(),"Collabs",orb.create_sequence_tc(0,accessControl.CollabHelper.type()));
        }
        return _tc;
    }

    /**
     * Return the Collabs IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:accessControl/Collabs:1.0";

    /**
     * Read Collabs from a marshalled stream
     * @param istream the input stream
     * @return the readed Collabs value
     */
    public static accessControl.Collab[] read(org.omg.CORBA.portable.InputStream istream)
    {
        accessControl.Collab[] new_one;
        {
        int size7 = istream.read_ulong();
        new_one = new accessControl.Collab[size7];
        for (int i7=0; i7<new_one.length; i7++)
         {
            new_one[i7] = accessControl.CollabHelper.read(istream);

         }
        }

        return new_one;
    }

    /**
     * Write Collabs into a marshalled stream
     * @param ostream the output stream
     * @param value Collabs value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, accessControl.Collab[] value)
    {
        ostream.write_ulong(value.length);
        for (int i7=0; i7<value.length; i7++)
        {
            accessControl.CollabHelper.write(ostream,value[i7]);

        }
    }

}
