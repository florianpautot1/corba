package accessControl;

/**
 * Interface definition : ServiceAutorisation
 * 
 * @author OpenORB Compiler
 */
public abstract class ServiceAutorisationPOA extends org.omg.PortableServer.Servant
        implements ServiceAutorisationOperations, org.omg.CORBA.portable.InvokeHandler
{
    public ServiceAutorisation _this()
    {
        return ServiceAutorisationHelper.narrow(_this_object());
    }

    public ServiceAutorisation _this(org.omg.CORBA.ORB orb)
    {
        return ServiceAutorisationHelper.narrow(_this_object(orb));
    }

    private static String [] _ids_list =
    {
        "IDL:accessControl/ServiceAutorisation:1.0"
    };

    public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte [] objectId)
    {
        return _ids_list;
    }

    private static final java.util.Map operationMap = new java.util.HashMap();

    static {
            operationMap.put("ajouterAutorisationPerm",
                    new Operation_ajouterAutorisationPerm());
            operationMap.put("ajouterAutorisationTemp",
                    new Operation_ajouterAutorisationTemp());
            operationMap.put("donnerRespPourZone",
                    new Operation_donnerRespPourZone());
            operationMap.put("getAutorisationPerm",
                    new Operation_getAutorisationPerm());
            operationMap.put("getAutorisationTemp",
                    new Operation_getAutorisationTemp());
            operationMap.put("getAutorisationsPerms",
                    new Operation_getAutorisationsPerms());
            operationMap.put("getAutorisationsPermsForZone",
                    new Operation_getAutorisationsPermsForZone());
            operationMap.put("getAutorisationsTemps",
                    new Operation_getAutorisationsTemps());
            operationMap.put("getAutorisationsTempsForZone",
                    new Operation_getAutorisationsTempsForZone());
            operationMap.put("getResp",
                    new Operation_getResp());
            operationMap.put("getZones",
                    new Operation_getZones());
            operationMap.put("modifierAutorisationPerm",
                    new Operation_modifierAutorisationPerm());
            operationMap.put("modifierAutorisationTemp",
                    new Operation_modifierAutorisationTemp());
            operationMap.put("supprimerAutorisation",
                    new Operation_supprimerAutorisation());
            operationMap.put("supprimerResp",
                    new Operation_supprimerResp());
            operationMap.put("verifierAutorisation",
                    new Operation_verifierAutorisation());
    }

    public final org.omg.CORBA.portable.OutputStream _invoke(final String opName,
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler)
    {

        final AbstractOperation operation = (AbstractOperation)operationMap.get(opName);

        if (null == operation) {
            throw new org.omg.CORBA.BAD_OPERATION(opName);
        }

        return operation.invoke(this, _is, handler);
    }

    // helper methods
    private org.omg.CORBA.portable.OutputStream _invoke_getZones(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        accessControl.Zone[] _arg_result = getZones();

        _output = handler.createReply();
        accessControl.ServiceAutorisationPackage.ZonesHelper.write(_output,_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_ajouterAutorisationTemp(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.AutorisationTemp arg0_in = accessControl.AutorisationTempHelper.read(_is);

        try
        {
            ajouterAutorisationTemp(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.Existe _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.ExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_ajouterAutorisationPerm(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.AutorisationPerm arg0_in = accessControl.AutorisationPermHelper.read(_is);

        try
        {
            ajouterAutorisationPerm(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.Existe _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.ExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_supprimerAutorisation(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();

        try
        {
            supprimerAutorisation(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_modifierAutorisationPerm(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.AutorisationPerm arg0_in = accessControl.AutorisationPermHelper.read(_is);

        try
        {
            modifierAutorisationPerm(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_modifierAutorisationTemp(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.AutorisationTemp arg0_in = accessControl.AutorisationTempHelper.read(_is);

        try
        {
            modifierAutorisationTemp(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_donnerRespPourZone(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        short arg0_in = _is.read_short();
        short arg1_in = _is.read_short();

        try
        {
            donnerRespPourZone(arg0_in, arg1_in);

            _output = handler.createReply();

        }
        catch (accessControl.Existe _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.ExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_supprimerResp(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        short arg0_in = _is.read_short();
        short arg1_in = _is.read_short();

        try
        {
            supprimerResp(arg0_in, arg1_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getResp(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        short arg0_in = _is.read_short();

        short _arg_result = getResp(arg0_in);

        _output = handler.createReply();
        _output.write_short(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getAutorisationTemp(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();

        try
        {
            accessControl.AutorisationTemp _arg_result = getAutorisationTemp(arg0_in);

            _output = handler.createReply();
            accessControl.AutorisationTempHelper.write(_output,_arg_result);

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getAutorisationPerm(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();

        try
        {
            accessControl.AutorisationPerm _arg_result = getAutorisationPerm(arg0_in);

            _output = handler.createReply();
            accessControl.AutorisationPermHelper.write(_output,_arg_result);

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getAutorisationsPerms(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);

        try
        {
            accessControl.AutorisationPerm[] _arg_result = getAutorisationsPerms(arg0_in);

            _output = handler.createReply();
            accessControl.ServiceAutorisationPackage.AutorisationsPermsHelper.write(_output,_arg_result);

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getAutorisationsTemps(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);

        try
        {
            accessControl.AutorisationTemp[] _arg_result = getAutorisationsTemps(arg0_in);

            _output = handler.createReply();
            accessControl.ServiceAutorisationPackage.AutorisationsTempsHelper.write(_output,_arg_result);

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getAutorisationsPermsForZone(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        short arg0_in = _is.read_short();

        accessControl.AutorisationPerm[] _arg_result = getAutorisationsPermsForZone(arg0_in);

        _output = handler.createReply();
        accessControl.ServiceAutorisationPackage.AutorisationsPermsHelper.write(_output,_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getAutorisationsTempsForZone(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        short arg0_in = _is.read_short();

        accessControl.AutorisationTemp[] _arg_result = getAutorisationsTempsForZone(arg0_in);

        _output = handler.createReply();
        accessControl.ServiceAutorisationPackage.AutorisationsTempsHelper.write(_output,_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_verifierAutorisation(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);
        accessControl.Zone arg1_in = accessControl.ZoneHelper.read(_is);

        boolean _arg_result = verifierAutorisation(arg0_in, arg1_in);

        _output = handler.createReply();
        _output.write_boolean(_arg_result);

        return _output;
    }

    // operation classes
    private abstract static class AbstractOperation {
        protected abstract org.omg.CORBA.portable.OutputStream invoke(
                ServiceAutorisationPOA target,
                org.omg.CORBA.portable.InputStream _is,
                org.omg.CORBA.portable.ResponseHandler handler);
    }

    private static final class Operation_getZones extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getZones(_is, handler);
        }
    }

    private static final class Operation_ajouterAutorisationTemp extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_ajouterAutorisationTemp(_is, handler);
        }
    }

    private static final class Operation_ajouterAutorisationPerm extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_ajouterAutorisationPerm(_is, handler);
        }
    }

    private static final class Operation_supprimerAutorisation extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_supprimerAutorisation(_is, handler);
        }
    }

    private static final class Operation_modifierAutorisationPerm extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_modifierAutorisationPerm(_is, handler);
        }
    }

    private static final class Operation_modifierAutorisationTemp extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_modifierAutorisationTemp(_is, handler);
        }
    }

    private static final class Operation_donnerRespPourZone extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_donnerRespPourZone(_is, handler);
        }
    }

    private static final class Operation_supprimerResp extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_supprimerResp(_is, handler);
        }
    }

    private static final class Operation_getResp extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getResp(_is, handler);
        }
    }

    private static final class Operation_getAutorisationTemp extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getAutorisationTemp(_is, handler);
        }
    }

    private static final class Operation_getAutorisationPerm extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getAutorisationPerm(_is, handler);
        }
    }

    private static final class Operation_getAutorisationsPerms extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getAutorisationsPerms(_is, handler);
        }
    }

    private static final class Operation_getAutorisationsTemps extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getAutorisationsTemps(_is, handler);
        }
    }

    private static final class Operation_getAutorisationsPermsForZone extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getAutorisationsPermsForZone(_is, handler);
        }
    }

    private static final class Operation_getAutorisationsTempsForZone extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getAutorisationsTempsForZone(_is, handler);
        }
    }

    private static final class Operation_verifierAutorisation extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final ServiceAutorisationPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_verifierAutorisation(_is, handler);
        }
    }

}
