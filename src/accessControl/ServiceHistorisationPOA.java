package accessControl;

/**
 * Interface definition : ServiceHistorisation
 * 
 * @author OpenORB Compiler
 */
public abstract class ServiceHistorisationPOA extends org.omg.PortableServer.Servant
        implements ServiceHistorisationOperations, org.omg.CORBA.portable.InvokeHandler
{
    public ServiceHistorisation _this()
    {
        return ServiceHistorisationHelper.narrow(_this_object());
    }

    public ServiceHistorisation _this(org.omg.CORBA.ORB orb)
    {
        return ServiceHistorisationHelper.narrow(_this_object(orb));
    }

    private static String [] _ids_list =
    {
        "IDL:accessControl/ServiceHistorisation:1.0"
    };

    public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte [] objectId)
    {
        return _ids_list;
    }

    public final org.omg.CORBA.portable.OutputStream _invoke(final String opName,
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler)
    {

        if (opName.equals("getPassages")) {
                return _invoke_getPassages(_is, handler);
        } else if (opName.equals("getPassagesParZones")) {
                return _invoke_getPassagesParZones(_is, handler);
        } else if (opName.equals("sauvegarderPassage")) {
                return _invoke_sauvegarderPassage(_is, handler);
        } else {
            throw new org.omg.CORBA.BAD_OPERATION(opName);
        }
    }

    // helper methods
    private org.omg.CORBA.portable.OutputStream _invoke_sauvegarderPassage(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Passage arg0_in = accessControl.PassageHelper.read(_is);

        sauvegarderPassage(arg0_in);

        _output = handler.createReply();

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getPassages(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        accessControl.Passage[] _arg_result = getPassages();

        _output = handler.createReply();
        accessControl.ServiceHistorisationPackage.PassagesHelper.write(_output,_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getPassagesParZones(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        short arg0_in = _is.read_short();

        accessControl.Passage[] _arg_result = getPassagesParZones(arg0_in);

        _output = handler.createReply();
        accessControl.ServiceHistorisationPackage.PassagesHelper.write(_output,_arg_result);

        return _output;
    }

}
