package accessControl;

/**
 * Interface definition : ServiceAutorisation
 * 
 * @author OpenORB Compiler
 */
public class ServiceAutorisationPOATie extends ServiceAutorisationPOA
{

    //
    // Private reference to implementation object
    //
    private ServiceAutorisationOperations _tie;

    //
    // Private reference to POA
    //
    private org.omg.PortableServer.POA _poa;

    /**
     * Constructor
     */
    public ServiceAutorisationPOATie(ServiceAutorisationOperations tieObject)
    {
        _tie = tieObject;
    }

    /**
     * Constructor
     */
    public ServiceAutorisationPOATie(ServiceAutorisationOperations tieObject, org.omg.PortableServer.POA poa)
    {
        _tie = tieObject;
        _poa = poa;
    }

    /**
     * Get the delegate
     */
    public ServiceAutorisationOperations _delegate()
    {
        return _tie;
    }

    /**
     * Set the delegate
     */
    public void _delegate(ServiceAutorisationOperations delegate_)
    {
        _tie = delegate_;
    }

    /**
     * _default_POA method
     */
    public org.omg.PortableServer.POA _default_POA()
    {
        if (_poa != null)
            return _poa;
        else
            return super._default_POA();
    }

    /**
     * Operation getZones
     */
    public accessControl.Zone[] getZones()
    {
        return _tie.getZones();
    }

    /**
     * Operation ajouterAutorisationTemp
     */
    public void ajouterAutorisationTemp(accessControl.AutorisationTemp a)
        throws accessControl.Existe
    {
        _tie.ajouterAutorisationTemp( a);
    }

    /**
     * Operation ajouterAutorisationPerm
     */
    public void ajouterAutorisationPerm(accessControl.AutorisationPerm a)
        throws accessControl.Existe
    {
        _tie.ajouterAutorisationPerm( a);
    }

    /**
     * Operation supprimerAutorisation
     */
    public void supprimerAutorisation(String idAut)
        throws accessControl.NonExiste
    {
        _tie.supprimerAutorisation( idAut);
    }

    /**
     * Operation modifierAutorisationPerm
     */
    public void modifierAutorisationPerm(accessControl.AutorisationPerm a)
        throws accessControl.NonExiste
    {
        _tie.modifierAutorisationPerm( a);
    }

    /**
     * Operation modifierAutorisationTemp
     */
    public void modifierAutorisationTemp(accessControl.AutorisationTemp a)
        throws accessControl.NonExiste
    {
        _tie.modifierAutorisationTemp( a);
    }

    /**
     * Operation donnerRespPourZone
     */
    public void donnerRespPourZone(short idCollab, short idZone)
        throws accessControl.Existe
    {
        _tie.donnerRespPourZone( idCollab,  idZone);
    }

    /**
     * Operation supprimerResp
     */
    public void supprimerResp(short idCollab, short idZone)
        throws accessControl.NonExiste
    {
        _tie.supprimerResp( idCollab,  idZone);
    }

    /**
     * Operation getResp
     */
    public short getResp(short idCollab)
    {
        return _tie.getResp( idCollab);
    }

    /**
     * Operation getAutorisationTemp
     */
    public accessControl.AutorisationTemp getAutorisationTemp(String idAut)
        throws accessControl.NonExiste
    {
        return _tie.getAutorisationTemp( idAut);
    }

    /**
     * Operation getAutorisationPerm
     */
    public accessControl.AutorisationPerm getAutorisationPerm(String idAut)
        throws accessControl.NonExiste
    {
        return _tie.getAutorisationPerm( idAut);
    }

    /**
     * Operation getAutorisationsPerms
     */
    public accessControl.AutorisationPerm[] getAutorisationsPerms(accessControl.Collab col)
        throws accessControl.NonExiste
    {
        return _tie.getAutorisationsPerms( col);
    }

    /**
     * Operation getAutorisationsTemps
     */
    public accessControl.AutorisationTemp[] getAutorisationsTemps(accessControl.Collab col)
        throws accessControl.NonExiste
    {
        return _tie.getAutorisationsTemps( col);
    }

    /**
     * Operation getAutorisationsPermsForZone
     */
    public accessControl.AutorisationPerm[] getAutorisationsPermsForZone(short idZone)
    {
        return _tie.getAutorisationsPermsForZone( idZone);
    }

    /**
     * Operation getAutorisationsTempsForZone
     */
    public accessControl.AutorisationTemp[] getAutorisationsTempsForZone(short idZone)
    {
        return _tie.getAutorisationsTempsForZone( idZone);
    }

    /**
     * Operation verifierAutorisation
     */
    public boolean verifierAutorisation(accessControl.Collab e, accessControl.Zone z)
    {
        return _tie.verifierAutorisation( e,  z);
    }

}
