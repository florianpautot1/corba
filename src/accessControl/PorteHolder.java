package accessControl;

/**
 * Holder class for : Porte
 * 
 * @author OpenORB Compiler
 */
final public class PorteHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal Porte value
     */
    public accessControl.Porte value;

    /**
     * Default constructor
     */
    public PorteHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public PorteHolder(accessControl.Porte initial)
    {
        value = initial;
    }

    /**
     * Read Porte from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = PorteHelper.read(istream);
    }

    /**
     * Write Porte into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        PorteHelper.write(ostream,value);
    }

    /**
     * Return the Porte TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return PorteHelper.type();
    }

}
