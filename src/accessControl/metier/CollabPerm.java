package accessControl.metier;

import accessControl.Collab;

public class CollabPerm extends Collab{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6870951417863261421L;

	public CollabPerm() {
		super();
	}

	public CollabPerm(String _id, String _nom, String _prenom, String _photo) {
		super(_id, _nom, _prenom, _photo);
	}

}
