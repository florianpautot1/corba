package accessControl.metier;

import accessControl.Collab;

public class CollabTemp extends Collab{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2361552231575849568L;

	public CollabTemp() {
		super();
	}

	public CollabTemp(String _id, String _nom, String _prenom, String _photo) {
		super(_id, _nom, _prenom, _photo);
	}

}
