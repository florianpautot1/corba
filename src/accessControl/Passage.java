package accessControl;

/**
 * Struct definition : Passage
 * 
 * @author OpenORB Compiler
*/
public final class Passage implements org.omg.CORBA.portable.IDLEntity
{
    @Override
	public String toString() {
		return "Passage [col=" + col + ", timestamp=" + timestamp + ", p=" + p + ", autorise=" + autorise + "]";
	}

	/**
     * Struct member col
     */
    public accessControl.Collab col;

    /**
     * Struct member timestamp
     */
    public String timestamp;

    /**
     * Struct member p
     */
    public accessControl.Porte p;

    /**
     * Struct member autorise
     */
    public boolean autorise;

    /**
     * Default constructor
     */
    public Passage()
    { }

    /**
     * Constructor with fields initialization
     * @param col col struct member
     * @param timestamp timestamp struct member
     * @param p p struct member
     * @param autorise autorise struct member
     */
    public Passage(accessControl.Collab col, String timestamp, accessControl.Porte p, boolean autorise)
    {
        this.col = col;
        this.timestamp = timestamp;
        this.p = p;
        this.autorise = autorise;
    }

}
