package accessControl.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import org.omg.CORBA.ORB;

import accessControl.AutorisationPerm;
import accessControl.Collab;
import accessControl.Existe;
import accessControl.Passage;
import accessControl.Porte;
import accessControl.ServiceHistorisationOperations;
import accessControl.ServiceHistorisationPOA;
import accessControl.Zone;
import accessControl.database.DatabaseHandler;

public class ServiceHistorisationImpl extends ServiceHistorisationPOA{
	private ORB orb;
	DatabaseHandler dh = new DatabaseHandler();
	
    public void setORB(ORB orb_val) {
		orb = orb_val; 
		}
    
    public void initDB()
	{
    	System.out.println("Initialzing PASSAGE DB");
		try
		{
			System.out.println("--- Connecting");
			dh.connectDatabase("Passage");
			System.out.println("--- Connected");

		}
		catch(Exception e)
		{
			System.out.println("-- [ERROR] Error creating db Passage : " + e.getMessage());
		}
		
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select * from Passage";
        
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
	        selectPreparedStatement.executeQuery();
	        selectPreparedStatement.close();

		} catch (SQLException e) {
			
            System.out.println("--- [WARNING] Warning table doesn't exist, creating it");
            
    		String req = "CREATE TABLE Passage (idCollab varchar(50), timestamp varchar(255), autorise boolean, idPorte int, idZone int, primary key(idCollab,timestamp));";
    		try
    		{
    			PreparedStatement ps = dh.getConn().prepareStatement(req);
                ps.executeUpdate();
                ps.close();
                System.out.println("--- Table PASSAGE created");
    		}
    		catch(Exception ex)
    		{
    			System.out.println("--- [ERROR] Error creating table Passage: " + ex.getMessage());
    		}	        
		}
	
	}
    
    
	@Override
	public void sauvegarderPassage(Passage p) {
		System.out.println("Saving passage : " + p.toString());
		PreparedStatement insertPreparedStatement = null;
        String insertQuery = "insert into Passage values(?,?,?,?,?);";
        
		try {
			insertPreparedStatement = dh.getConn().prepareStatement(insertQuery);
			
	        insertPreparedStatement.setString(1, p.col.id);
	        insertPreparedStatement.setString(2, p.timestamp);
	        insertPreparedStatement.setBoolean(3, p.autorise);
	        insertPreparedStatement.setShort(4, p.p.id);
	        insertPreparedStatement.setShort(5, p.p.z.id);

	        insertPreparedStatement.executeUpdate();

	        insertPreparedStatement.close();	
			System.out.println("--- Passage inserted");
			dh.getConn().commit();

		} catch (SQLException ex) {
			System.out.println("--- [ERROR] Error adding Passage : " + ex.getMessage());
		}
	}

	@Override
	public Passage[] getPassages() {
		System.out.println("Getting all passages");
		PreparedStatement selectPreparedStatement = null;

		ArrayList<Passage> lesPassages = new ArrayList<Passage>();
		try {

			String selectQuery = "select * from Passage";

			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);

			ResultSet rs = selectPreparedStatement.executeQuery();
			System.out.println("--- Get Passages successful");
			Collab c = null;
			Porte p = null;
			
			while (rs.next()) {
				c = new Collab(rs.getString("idCollab"),"","","");
				p = new Porte((short)rs.getInt("idPorte"),"",new Zone(rs.getShort("idZone"),""));
				
				lesPassages.add(new Passage(c, rs.getString("timestamp"), p, rs.getBoolean("autorise")));
			}

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error, getting Passages: " + e.getMessage());

		}

		System.out.println("--- Getting all Passages OK");
		Object[] passageTab = lesPassages.toArray();
		System.out.println("Passages : " + lesPassages.toString());

		Passage[] passageArray = Arrays.copyOf(passageTab, passageTab.length, Passage[].class);
		return passageArray;
	}

	@Override
	public Passage[] getPassagesParZones(short idZone) {
		System.out.println("Getting all passages for zone :" + idZone);
		PreparedStatement selectPreparedStatement = null;

		ArrayList<Passage> lesPassages = new ArrayList<Passage>();
		try {

			String selectQuery = "select * from Passage where idZone = ?";

			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setShort(1, idZone);
			
			ResultSet rs = selectPreparedStatement.executeQuery();
			System.out.println("--- Get Passages successful");
			Collab c = null;
			Porte p = null;
			
			while (rs.next()) {
				c = new Collab(rs.getString("idCollab"),"","","");
				p = new Porte((short)rs.getInt("idPorte"),"",new Zone(rs.getShort("idZone"),""));
				
				lesPassages.add(new Passage(c, rs.getString("timestamp"), p, rs.getBoolean("autorise")));
			}

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error, getting Passages: " + e.getMessage());

		}

		System.out.println("--- Getting all Passages OK");
		Object[] passageTab = lesPassages.toArray();
		System.out.println("Passages : " + lesPassages.toString());

		Passage[] passageArray = Arrays.copyOf(passageTab, passageTab.length, Passage[].class);
		return passageArray;
	}

}
