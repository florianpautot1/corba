package accessControl.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.omg.CORBA.ORB;

import accessControl.AutorisationPerm;
import accessControl.AutorisationTemp;
import accessControl.Collab;
import accessControl.Existe;
import accessControl.NonExiste;
import accessControl.ServiceAutorisationPOA;
import accessControl.Zone;
import accessControl.database.DatabaseHandler;
import accessControl.metier.CollabTemp;

public class ServiceAutorisationImpl extends ServiceAutorisationPOA {
	private ORB orb;
	DatabaseHandler dh = new DatabaseHandler();

	public void setORB(ORB orb_val) {
		orb = orb_val;
	}

	public void initDB() {
		System.out.println("Initialzing AUTORISATION DB");
		try {
			System.out.println("--- Connecting");
			dh.connectDatabase("Autorisation");
			System.out.println("--- Connected");
		} catch (Exception e) {
			System.out.println("--- [ERROR] Error creating db Autorisation : " + e.getMessage());
		}

		try {
			String selectQuery = "select id from AutorisationPerm union select id from AutorisationTemp";
			PreparedStatement selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);

		} catch (SQLException e) {

			System.out.println("--- [WARNING] Warning tables doesn't exist, creating them : " + e.getMessage());

			String reqPerm = "CREATE TABLE AutorisationPerm (id varchar(50) primary key not null , idCollab varchar(50),  hdeb varchar(255), hfin varchar(255), idZone int);";
			try {
				PreparedStatement ps = dh.getConn().prepareStatement(reqPerm);
				ps.executeUpdate();
				ps.close();
				System.out.println("--- Table AUTORISATIONPERM created");
			} catch (Exception ex) {
				System.out.println("--- [ERROR] Error creating table Autorisation perm: " + ex.getMessage());
			}

			String reqTemp = "CREATE TABLE AutorisationTemp (id varchar(50) primary key not null , idCollab varchar(50),  hdeb varchar(255), hfin varchar(255), idZone int, jourDeb varchar(255), jourFin varchar(255));";
			try {
				PreparedStatement ps = dh.getConn().prepareStatement(reqTemp);
				ps.executeUpdate();
				ps.close();
				System.out.println("--- Table AUTORISATIONTEMP created");

			} catch (Exception ex) {
				System.out.println("--- [ERROR] Error creating table Autorisation temp: " + ex.getMessage());
			}

			try {
				String selectQuery = "select * from RESPONSABLES";
				PreparedStatement selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);

			} catch (SQLException ex2) {

				System.out.println(
						"--- [WARNING] Warning table RESPONSABLES doesn't exist, creating it : " + ex2.getMessage());

				String reqResp = "CREATE TABLE RESPONSABLES (idZone int primary key not null, idCollab int);";
				try {
					PreparedStatement ps = dh.getConn().prepareStatement(reqResp);
					ps.executeUpdate();
					ps.close();
					System.out.println("--- Table RESPONSABLES created");
				} catch (Exception ex) {
					System.out.println("--- [ERROR] Error creating table RESPONSABLES perm: " + ex.getMessage());
				}
			}

			try {
				String selectQuery = "select * from ZONES";
				PreparedStatement selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);

			} catch (SQLException ex3) {

				System.out
						.println("--- [WARNING] Warning table ZONES doesn't exist, creating it : " + ex3.getMessage());

				String reqResp = "CREATE TABLE ZONES (idZone int primary key not null, nomZone varchar(50));";
				try {
					PreparedStatement ps = dh.getConn().prepareStatement(reqResp);
					ps.executeUpdate();
					ps.close();
					System.out.println("--- Table ZONES created");
				} catch (Exception ex) {
					System.out.println("--- [ERROR] Error creating table ZONES perm: " + ex.getMessage());
				}
			}

			ajouterZone((short) 1, "Zone A : Aerospatial et securite");
			ajouterZone((short) 2, "Zone B : Recherche et developpement");
			ajouterZone((short) 3, "Zone C : Finances et comptabilite");

		}
	}

	@Override
	public void supprimerAutorisation(String idAut) throws NonExiste {
		if (idAut.startsWith("P")) {
			System.out.println("Deleting autorisation perm :" + idAut);
			PreparedStatement deletePS = null;
			String deleteQuery = "delete from AutorisationPerm where id = ? ;";
			try {
				deletePS = dh.getConn().prepareStatement(deleteQuery);

				deletePS.setString(1, idAut);

				deletePS.executeUpdate();
				deletePS.close();
				System.out.println("--- Autorisation perm deleted");
				dh.getConn().commit();

			} catch (SQLException e) {
				System.out.println("Error deleting autorisation: " + e.getMessage());
				throw new NonExiste("Error deleting autorisation");
			}

		} else if (idAut.startsWith("T")) {
			System.out.println("Deleting autorisation temp :" + idAut);

			PreparedStatement deletePS = null;
			String deleteQuery = "delete from AutorisationTemp where id = ? ;";
			try {
				deletePS = dh.getConn().prepareStatement(deleteQuery);

				deletePS.setString(1, idAut);

				deletePS.executeUpdate();

				deletePS.close();
				System.out.println("--- Autorisation temp deleted");
				dh.getConn().commit();

			} catch (SQLException e) {
				System.out.println("--- [ERROR] Error deleting autorisation: " + e.getMessage());
				throw new NonExiste("--- [ERROR] Error deleting autorisation");
			}
		} else
			System.out.println("Erreur inconnue");

	}

	@Override
	public boolean verifierAutorisation(Collab e, Zone z) {
		System.out.println("Checking autorisation for collab : " + e.toString() + " in zone :" + z.toString());
		Date timestamp = new Date();

		String selectQuery = "select * from AutorisationPerm where idCollab = ? and idZone = ?";
		PreparedStatement selectPreparedStatement = null;
		boolean res = false;
		SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.FRENCH);
		SimpleDateFormat formatDay = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);

		try {

			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setString(1, e.id);
			selectPreparedStatement.setShort(2, z.id);

			ResultSet rs = selectPreparedStatement.executeQuery();
			if (rs.next()) {

				Date hDeb = null;
				Date hFin = null;
				Date todayMinutes = null;
				try {
					hDeb = format.parse(rs.getString("hdeb"));
					hFin = format.parse(rs.getString("hfin"));
					Date date = new Date();
				    Calendar calendar = Calendar.getInstance();
					
					todayMinutes = format.parse(calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE));

				} catch (ParseException e1) {
					System.out.println("--- [ERROR] Error parsing dates");
				}
				Date today = new Date();

				if (todayMinutes.after(hDeb) && todayMinutes.before(hFin)) {
					System.out.println("--- Autorisation perm : OK");
					res = true;
				}

			} else {
				System.out.println("--- Autorisation perm : NOK, testing temp");
				res = false;
				selectQuery = "select * from AutorisationTemp where idCollab = ? and idZone = ?";

				selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
				selectPreparedStatement.setString(1, e.id);
				selectPreparedStatement.setShort(2, z.id);
				rs = selectPreparedStatement.executeQuery();

				if (rs.next()) {

					Date hDeb = null;
					Date hFin = null;
					Date jourFin = null;
					Date jourDeb = null;
					Date todayMinutes = null;
					try {
						Date date = new Date();   // given date
					    Calendar calendar = Calendar.getInstance();
					    
						hDeb = format.parse(rs.getString("hdeb"));
						hFin = format.parse(rs.getString("hfin"));
						jourDeb = formatDay.parse(rs.getString("jourDeb"));
						jourFin = formatDay.parse(rs.getString("jourFin"));
						todayMinutes = format.parse(calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE));

					} catch (ParseException e1) {
						System.out.println("--- [ERROR] Error parsing dates");
					}
					
					
					Date today = new Date();
					
					if ((todayMinutes.after(hDeb) && todayMinutes.before(hFin)) && (today.after(jourDeb) && today.before(jourFin))) {
						System.out.println("--- Autorisation temp : OK");
						res = true;
					}
					else
						System.out.println("--- Autorisation temp : NOK");

				}
			}

			selectPreparedStatement.close();
		} catch (SQLException ex) {
			System.out.println("--- [ERROR] Error checking autorisation : " + ex.getMessage());
		}

		return res;
	}

	@Override
	public void ajouterAutorisationTemp(AutorisationTemp a) throws Existe {
		PreparedStatement insertPreparedStatement = null;

		AutorisationTemp at = (AutorisationTemp) a;
		System.out.println("Adding AutorisationTemp :" + at.toString());

		String insertQuery = "insert into AutorisationTemp values(?,?,?,?,?,?,?);";
		try {
			insertPreparedStatement = dh.getConn().prepareStatement(insertQuery);

			insertPreparedStatement.setString(1, "T" + "-" + at.idCollab + "-" + at.idZone);
			insertPreparedStatement.setString(2, at.idCollab);
			insertPreparedStatement.setString(3, at.hDeb);
			insertPreparedStatement.setString(4, at.hFin);
			insertPreparedStatement.setShort(5, at.idZone);
			insertPreparedStatement.setString(6, at.jourDeb);
			insertPreparedStatement.setString(7, at.jourFin);

			insertPreparedStatement.executeUpdate();
			insertPreparedStatement.close();
			System.out.println("--- Autorisation temp inserted");
			dh.getConn().commit();

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error adding autorisation temp : " + e.getMessage());
			throw new Existe("--- [ERROR] Error adding autorisation temp ");
		}

	}

	@Override
	public void ajouterAutorisationPerm(AutorisationPerm a) throws Existe {
		PreparedStatement insertPreparedStatement = null;

		AutorisationPerm ap = (AutorisationPerm) a;
		System.out.println("Adding AutorisationPerm :" + ap.toString());

		String insertQuery = "insert into AutorisationPerm values(?,?,?,?, ?);";
		try {
			insertPreparedStatement = dh.getConn().prepareStatement(insertQuery);

			insertPreparedStatement.setString(1, "P" + "-" + a.idCollab + "-" + a.idZone);
			insertPreparedStatement.setString(2, a.idCollab);
			insertPreparedStatement.setString(3, a.hDeb);
			insertPreparedStatement.setString(4, a.hFin);
			insertPreparedStatement.setShort(5, a.idZone);

			insertPreparedStatement.executeUpdate();
			insertPreparedStatement.close();
			System.out.println("--- Autorisation perm inserted :" + a.toString());
			dh.getConn().commit();

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error adding autorisation : " + e.getMessage());
			throw new Existe("--- [ERROR] Error adding autorisation ");
		}

	}

	@Override
	public void modifierAutorisationPerm(AutorisationPerm a) throws NonExiste {
		PreparedStatement updatePS = null;
		System.out.println("Updating autorisation perm : " + a.toString());
		String updateQuery = "update AutorisationPerm set idCollab = ?, hdeb = ?, hfin = ?, idZone = ? where id = ? ;";
		try {
			updatePS = dh.getConn().prepareStatement(updateQuery);

			updatePS.setString(1, a.idCollab);
			updatePS.setString(2, a.hDeb);
			updatePS.setString(3, a.hFin);
			updatePS.setShort(4, a.idZone);
			updatePS.setString(5, a.id);

			updatePS.executeUpdate();
			updatePS.close();
			System.out.println("--- AutorisationPerm updated");
			dh.getConn().commit();

		} catch (SQLException ex) {
			System.out.println("--- [ERROR] Error updating autorisationPerm : " + ex.getMessage());
			throw new NonExiste("--- [ERROR] Error updating autorisationPerm ");
		}

	}

	@Override
	public void modifierAutorisationTemp(AutorisationTemp a) throws NonExiste {
		PreparedStatement updatePS = null;

		System.out.println("Updating autorisation temp : " + a.toString());

		String updateQuery = "update AutorisationTemp set idCollab = ?, hdeb = ?, hfin = ?, idZone = ?, jourdeb = ?, jourfin = ? where id = ? ;";
		try {
			updatePS = dh.getConn().prepareStatement(updateQuery);

			updatePS.setString(1, a.idCollab);
			updatePS.setString(2, a.hDeb);
			updatePS.setString(3, a.hFin);
			updatePS.setShort(4, a.idZone);
			updatePS.setString(5, a.jourDeb);
			updatePS.setString(6, a.jourFin);
			updatePS.setString(7, a.id);

			updatePS.executeUpdate();

			updatePS.close();
			System.out.println("--- AutorisationTemp updated");
			dh.getConn().commit();

		} catch (SQLException ex) {
			System.out.println("--- [ERROR] Error updating autorisation temp: " + ex.getMessage());
			throw new NonExiste("--- [ERROR] Error updating autorisation temp");
		}

	}

	@Override
	public AutorisationTemp getAutorisationTemp(String idAut) throws NonExiste {
		System.out.println("Getting autorisation temp : " + idAut);
		PreparedStatement selectPreparedStatement = null;
		String selectQuery = "select * from AutorisationTemp where id = ?";
		AutorisationTemp a = null;
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setString(1, idAut);

			ResultSet rs = selectPreparedStatement.executeQuery();
			if (rs.next()) {
				a = new AutorisationTemp(rs.getString("id"), rs.getString("idCollab"), rs.getString("hdeb"),
						rs.getString("hfin"), rs.getShort("idZone"), rs.getString("jourdeb"), rs.getString("jourfin"));
			}

			selectPreparedStatement.close();

			System.out.println("--- Get Autorisation temp successful");

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error, Autorisation doesn't exist : " + e.getMessage());
			throw new NonExiste("--- [ERROR] Error, Autorisation doesn't exist");
		}

		return a;
	}

	@Override
	public AutorisationPerm getAutorisationPerm(String idAut) throws NonExiste {
		System.out.println("Getting autorisation perm: " + idAut);
		PreparedStatement selectPreparedStatement = null;
		AutorisationPerm a = null;

		String selectQuery = "select * from AutorisationPerm where id = ?";

		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setString(1, idAut);

			ResultSet rs = selectPreparedStatement.executeQuery();
			if (rs.next()) {
				a = new AutorisationPerm(rs.getString("id"), rs.getString("idCollab"), rs.getString("hdeb"),
						rs.getString("hfin"), rs.getShort("idZone"));
			}
			System.out.println("--- Get Autorisation perm successful");

			selectPreparedStatement.close();
		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error, Autorisation doesn't exist : " + e.getMessage());
			throw new NonExiste("--- [ERROR] Error, Autorisation doesn't exist ");
		}

		return a;
	}

	@Override
	public AutorisationPerm[] getAutorisationsPerms(Collab col) throws NonExiste {
		System.out.println("Getting all autorisations");
		PreparedStatement selectPreparedStatement = null;

		ArrayList<AutorisationPerm> lesAutorisations = new ArrayList<AutorisationPerm>();
		try {

			String selectQuery = "select * from AutorisationPerm where idCollab = ?";

			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setString(1, col.id);

			ResultSet rs = selectPreparedStatement.executeQuery();
			System.out.println("--- Get Autorisations perms successful");
			while (rs.next()) {
				lesAutorisations.add(new AutorisationPerm(rs.getString("id"), rs.getString("idCollab"),
						rs.getString("hdeb"), rs.getString("hfin"), rs.getShort("idZone")));
			}

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error, getting Autorisation perm: " + e.getMessage());

		}

		System.out.println("--- Getting all autorisations perms OK");
		Object[] autTab = lesAutorisations.toArray();
		System.out.println("Autorisations perms : " + lesAutorisations.toString());

		AutorisationPerm[] autorisationArray = Arrays.copyOf(autTab, autTab.length, AutorisationPerm[].class);
		return autorisationArray;
	}

	@Override
	public AutorisationTemp[] getAutorisationsTemps(Collab col) throws NonExiste {
		System.out.println("Getting all autorisations");
		PreparedStatement selectPreparedStatement = null;

		ArrayList<AutorisationTemp> lesAutorisations = new ArrayList<AutorisationTemp>();
		try {

			String selectQuery = "select * from AutorisationTemp where idCollab = ?";

			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setString(1, col.id);

			ResultSet rs = selectPreparedStatement.executeQuery();
			System.out.println("--- Get Autorisations temps successful");

			while (rs.next()) {
				lesAutorisations.add(new AutorisationTemp(rs.getString("id"), rs.getString("idCollab"),
						rs.getString("hdeb"), rs.getString("hfin"), rs.getShort("idZone"), rs.getString("jourdeb"),
						rs.getString("jourfin")));
			}

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error, getting Autorisation temps : " + e.getMessage());

		}

		System.out.println("--- Getting all autorisations temps OK");
		Object[] autTab = lesAutorisations.toArray();
		System.out.println("Autorisations temps : " + lesAutorisations.toString());

		AutorisationTemp[] autorisationArray = Arrays.copyOf(autTab, autTab.length, AutorisationTemp[].class);
		return autorisationArray;
	}

	@Override
	public AutorisationPerm[] getAutorisationsPermsForZone(short idZone) {
		System.out.println("Getting all autorisations perms for zone :" + idZone);
		PreparedStatement selectPreparedStatement = null;

		ArrayList<AutorisationPerm> lesAutorisations = new ArrayList<AutorisationPerm>();
		try {

			String selectQuery = "select * from AutorisationPerm where idZone = ?";

			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setShort(1, idZone);

			ResultSet rs = selectPreparedStatement.executeQuery();
			System.out.println("--- Get Autorisations perms successful");

			while (rs.next()) {
				lesAutorisations.add(new AutorisationPerm(rs.getString("id"), rs.getString("idCollab"),
						rs.getString("hdeb"), rs.getString("hfin"), rs.getShort("idZone")));
			}

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error, getting Autorisation perms : " + e.getMessage());

		}

		System.out.println("--- Getting all autorisations perms OK");
		Object[] autTab = lesAutorisations.toArray();
		System.out.println("Autorisations perms : " + lesAutorisations.toString());

		AutorisationPerm[] autorisationArray = Arrays.copyOf(autTab, autTab.length, AutorisationPerm[].class);
		return autorisationArray;
	}

	@Override
	public AutorisationTemp[] getAutorisationsTempsForZone(short idZone) {
		System.out.println("Getting all autorisations temps for zone :" + idZone);
		PreparedStatement selectPreparedStatement = null;

		ArrayList<AutorisationTemp> lesAutorisations = new ArrayList<AutorisationTemp>();
		try {

			String selectQuery = "select * from AutorisationTemp where idZone = ?";

			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setShort(1, idZone);

			ResultSet rs = selectPreparedStatement.executeQuery();
			System.out.println("--- Get Autorisations temps successful");

			while (rs.next()) {
				lesAutorisations.add(new AutorisationTemp(rs.getString("id"), rs.getString("idCollab"),
						rs.getString("hdeb"), rs.getString("hfin"), rs.getShort("idZone"), rs.getString("jourdeb"),
						rs.getString("jourfin")));
			}

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error, getting Autorisation temps : " + e.getMessage());

		}

		System.out.println("--- Getting all autorisations temps OK");
		Object[] autTab = lesAutorisations.toArray();
		System.out.println("Autorisations temps : " + lesAutorisations.toString());

		AutorisationTemp[] autorisationArray = Arrays.copyOf(autTab, autTab.length, AutorisationTemp[].class);
		return autorisationArray;
	}

	@Override
	public Zone[] getZones() {
		System.out.println("Getting zones  ");
		PreparedStatement selectPreparedStatement = null;
		String selectQuery = "select * from Zones";
		ArrayList<Zone> res = new ArrayList<Zone>();
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);

			ResultSet rs = selectPreparedStatement.executeQuery();
			while (rs.next()) {
				res.add(new Zone(rs.getShort("idZone"), rs.getString("nomZone")));
			}
			selectPreparedStatement.close();
		} catch (SQLException ex) {
			System.out.println("--- [ERROR] Error getting zone : " + ex.getMessage());
		}

		Object[] zoneTab = res.toArray();
		System.out.println("Zones : " + res.toString());

		Zone[] zoneArray = Arrays.copyOf(zoneTab, zoneTab.length, Zone[].class);
		return zoneArray;
	}

	public void ajouterZone(short idZone, String nomZone) {
		PreparedStatement insertPreparedStatement = null;
		System.out.println("Adding Zone :" + nomZone);

		String insertQuery = "insert into Zones values(?,?);";
		try {
			insertPreparedStatement = dh.getConn().prepareStatement(insertQuery);

			insertPreparedStatement.setShort(1, idZone);
			insertPreparedStatement.setString(2, nomZone);

			insertPreparedStatement.executeUpdate();

			insertPreparedStatement.close();
			System.out.println("--- Zone inserted ");
			dh.getConn().commit();

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error adding zone : " + e.getMessage());
		}
	}

	@Override
	public void donnerRespPourZone(short idCollab, short idZone) throws Existe {
		PreparedStatement insertPreparedStatement = null;
		System.out.println("Adding Resp to :" + idCollab + " fo Zone : " + idZone);

		String insertQuery = "insert into Responsables values(?,?);";
		try {
			insertPreparedStatement = dh.getConn().prepareStatement(insertQuery);

			insertPreparedStatement.setShort(1, idZone);
			insertPreparedStatement.setShort(2, idCollab);

			insertPreparedStatement.executeUpdate();

			insertPreparedStatement.close();
			System.out.println("--- Resp inserted ");
			dh.getConn().commit();

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error adding resp : " + e.getMessage());
			throw new Existe("--- [ERROR] Error adding resp ");
		}
	}

	@Override
	public void supprimerResp(short idCollab, short idZone) throws NonExiste {
		System.out.println("Deleting resp  :" + idCollab);
		PreparedStatement deletePS = null;
		String deleteQuery = "delete from Responsables where idCollab = ? and idZone = ?;";
		try {
			deletePS = dh.getConn().prepareStatement(deleteQuery);

			deletePS.setShort(1, idCollab);
			deletePS.setShort(2, idZone);

			deletePS.executeUpdate();

			deletePS.close();
			System.out.println("--- Resp  deleted");
			dh.getConn().commit();

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error deleting resp: " + e.getMessage());
			throw new NonExiste("--- [ERROR] Error deleting resp");
		}
	}

	@Override
	public short getResp(short idCollab) {
		System.out.println("Checking resp for collab : " + idCollab);
		PreparedStatement selectPreparedStatement = null;
		String selectQuery = "select idZone from Responsables where idCollab = ?";
		short res = 0;
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setShort(1, idCollab);

			ResultSet rs = selectPreparedStatement.executeQuery();
			if (rs.next()) {
				res = (short) rs.getInt("idZone");
			}
			selectPreparedStatement.close();
		} catch (SQLException ex) {
			System.out.println("--- [ERROR] Error getting resp : " + ex.getMessage());
		}

		return res;
	}

}
