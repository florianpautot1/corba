package accessControl.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.omg.CORBA.ORB;

import accessControl.Authentification;
import accessControl.Collab;
import accessControl.Existe;
import accessControl.NonExiste;
import accessControl.ServiceAuthentificationOperations;
import accessControl.ServiceAuthentificationPOA;
import accessControl.database.DatabaseHandler;

public class ServiceAuthentificationImpl extends ServiceAuthentificationPOA{
	private ORB orb;
	DatabaseHandler dh = new DatabaseHandler();
	
    public void setORB(ORB orb_val) {
		orb = orb_val; 
		}
    
    public void initDB()
	{
    	System.out.println("Initialzing AUTHENTIFICATION DB");
		try
		{
			System.out.println("--- Connecting Database");
			dh.connectDatabase("Authentification");
			System.out.println("--- Connected");
		}
		catch(Exception e)
		{
			System.out.println("--- [ERROR] Error creating db Authentification : " + e.getMessage());
		}
		
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select * from Authentification";
        
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
	        selectPreparedStatement.executeQuery();
	        selectPreparedStatement.close();

		} catch (SQLException e) {
			
            System.out.println("--- [WARNING] table doesn't exist, creating it ");
            
    		String req = "CREATE TABLE Authentification (id varchar(50), empreinte varchar(255), mdp varchar(255), primary key (id,empreinte));";
    		try
    		{
    			PreparedStatement ps = dh.getConn().prepareStatement(req);
                ps.executeUpdate();
                ps.close();
                System.out.println("--- Table AUTHENTIFICATION created");
    		}
    		catch(Exception ex)
    		{
    			System.out.println("--- [ERROR] Error creating table Authentification: " + ex.getMessage());
    		}	        
		}
	
	}

	@Override
	public String verificationEmpreinte(String empreinte) {
		System.out.println("Checking print " + empreinte);
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select * from Authentification where empreinte = ?";
        String res = "";
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setString(1, empreinte);

	        ResultSet rs = selectPreparedStatement.executeQuery();
	        if(rs.next())
	        {
	        	res = rs.getString("id");
		        System.out.println("--- Print OK");

	        }
	        else
	        {
	        	System.out.println("--- Print NOK");
	        }
	        selectPreparedStatement.close();
		} catch (SQLException e) {
            System.out.println("--- [ERROR] Error checking print : " + e.getMessage());
		}
		
        return res;
	}

	@Override
	public void definirAuthentification(Collab e, String empreinte, String mdp)
			throws NonExiste, Existe {
		System.out.println("Defining authentification for collab :" + e.toString());
		PreparedStatement insertPreparedStatement = null;
        String insertQuery = "insert into Authentification values(?,?,?);";
        
		try {
			insertPreparedStatement = dh.getConn().prepareStatement(insertQuery);
			
	        insertPreparedStatement.setString(1, e.id);

	        insertPreparedStatement.setString(2, empreinte);
	        insertPreparedStatement.setString(3, mdp);

	        insertPreparedStatement.executeUpdate();
			dh.getConn().commit();
	        insertPreparedStatement.close();	
			System.out.println("--- Authentification inserted");

		} catch (SQLException ex) {
			System.out.println("--- [ERROR] Error adding authentification : " + ex.getMessage());
			throw new Existe("--- [ERROR] Error adding authentification ");
		}			
	}

	@Override
	public void modifierEmpreinte(Collab e, String empreinte) throws NonExiste {
		System.out.println("Modifying print for collab : " + e.toString());
		PreparedStatement updatePS = null;
        String updateQuery = "update authentification set empreinte = ? where id = ? ;";
		try {
			updatePS = dh.getConn().prepareStatement(updateQuery);
			
	        updatePS.setString(1, empreinte);
	        updatePS.setString(2, e.id);


	        updatePS.executeUpdate();
			dh.getConn().commit();

	        updatePS.close();	
			System.out.println("--- Print updated");

		} catch (SQLException ex) {
			System.out.println("--- [ERROR] Error updating empreinte : " + ex.getMessage());
			throw new NonExiste("--- [ERROR] Error updating empreinte ");
		}				
	}

	@Override
	public boolean verificationMdp(Collab e, String mdp) {
		System.out.println("Checking password for collab : "+ e.toString());
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select mdp from Authentification where id = ?";
        boolean res = false;
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setString(1, e.id);

	        ResultSet rs = selectPreparedStatement.executeQuery();
	        if(rs.next())
	        {
	        	if(rs.getString("mdp").equals(mdp))
	        	{
	    	        System.out.println("--- Password OK");
	        		res = true;

	        	}
	        	else
	        		System.out.println("--- Password NOK");
	        }
	        selectPreparedStatement.close();
		} catch (SQLException ex) {
            System.out.println("--- [ERROR] Error checking empreinte : " + ex.getMessage());
		}
		
        return res;
	}

	@Override
	public void supprimerEmpreinte(Collab e) throws NonExiste {
		System.out.println("Deleting identification : "+e.id);
		PreparedStatement deletePS = null;
        String deleteQuery = "delete from Authentification where id = ? ;";
		try {
			deletePS = dh.getConn().prepareStatement(deleteQuery);
			
	        deletePS.setString(1, e.id);

	        deletePS.executeUpdate();
			dh.getConn().commit();

	        deletePS.close();	
			System.out.println("--- Authentification deleted");

		} catch (SQLException ex) {
			System.out.println("--- [ERROR] Error deleting authentification : " + ex.getMessage());
			throw new NonExiste("--- [ERROR] Error deleting authentification ");
		}				
	}

	@Override
	public Authentification getAuthentification(Collab e) throws NonExiste {
		System.out.println("Checking password for collab : "+ e.toString());
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select * from Authentification where id = ?";
        Authentification a = new Authentification("","","");
        boolean res = false;
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setString(1, e.id);

	        ResultSet rs = selectPreparedStatement.executeQuery();
	        if(rs.next())
	        {
	        	a.empreinte = rs.getString("empreinte");
	        	a.id = rs.getString("id");
	        	a.mdp = rs.getString("mdp");
	        }
	        selectPreparedStatement.close();
		} catch (SQLException ex) {
            System.out.println("--- [ERROR] Error checking empreinte : " + ex.getMessage());
            throw new NonExiste("--- [ERROR] Error checking empreinte ");
		}
		
        return a;
	}

	@Override
	public void modifierMdp(Collab e, String mdp) throws NonExiste {
		System.out.println("Modifying mdp for collab : " + e.toString());
		PreparedStatement updatePS = null;
        String updateQuery = "update authentification set mdp = ? where id = ? ;";
		try {
			updatePS = dh.getConn().prepareStatement(updateQuery);
			
	        updatePS.setString(1, mdp);
	        updatePS.setString(2, e.id);


	        updatePS.executeUpdate();

	        updatePS.close();	
			System.out.println("--- Mdp updated");
			dh.getConn().commit();


		} catch (SQLException ex) {
			System.out.println("--- [ERROR] Error updating mdp : " + ex.getMessage());
			throw new NonExiste("--- [ERROR] Error updating mdp ");
		}				
	}


}
