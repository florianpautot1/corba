package accessControl.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import org.omg.CORBA.ORB;

import accessControl.Collab;
import accessControl.Existe;
import accessControl.NonExiste;
import accessControl.ServiceAcceuilPOA;
import accessControl.ServiceAuthentification;
import accessControl.ServiceAutorisation;
import accessControl.clients.Client;
import accessControl.database.DatabaseHandler;
import accessControl.metier.CollabTemp;

public class ServiceAccueilImpl extends ServiceAcceuilPOA{
	private ORB orb;
	DatabaseHandler dh = new DatabaseHandler();
	private static int lastInsertedId = 0;
	
    public void setORB(ORB orb_val) {
		orb = orb_val; 
		}
    
    public static long getLastInsertedId() {
		return lastInsertedId;
	}

	public void initDB()
	{
    	System.out.println("Initializing DB Accueil");
		try
		{
			System.out.println("--- Connecting DB");
			dh.connectDatabase("Accueil");
			System.out.println("--- Connected");

		}
		catch(Exception e)
		{
			System.out.println("--- [ERROR] Error creating db Accueil : " + e.getMessage());
		}
		
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select * from Accueil";
        
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
	        ResultSet rs = selectPreparedStatement.executeQuery();
	        selectPreparedStatement.close();

		} catch (SQLException e) {
			
            System.out.println("--- [WARNING] table doesn't exist, creating it");
            
    		String req = "CREATE TABLE Accueil (id int auto_increment, nom varchar(255), prenom varchar(255), photo varchar(255), primary key(id));";
    		try
    		{
    			PreparedStatement ps = dh.getConn().prepareStatement(req);
                ps.executeUpdate();
                ps.close();
                System.out.println("--- Table ACCUEIL created");
    		}
    		catch(Exception ex)
    		{
    			System.out.println("Error creating table Accueil: " + ex.getMessage());
    		}	        
		}
	}
    
	@Override
	public void ajouterCollabTemp(Collab col) throws Existe {
		System.out.println("Adding Collab temp : " + col.toString());
		PreparedStatement insertPreparedStatement = null;
        String insertQuery = "insert into Accueil(nom, prenom, photo) values(?,?,?);";
		try {
			insertPreparedStatement = dh.getConn().prepareStatement(insertQuery);
			
	        insertPreparedStatement.setString(1, col.nom);
	        insertPreparedStatement.setString(2, col.prenom);
	        insertPreparedStatement.setString(3, col.photo);

	        insertPreparedStatement.executeUpdate();

	        ResultSet generatedKeys = insertPreparedStatement.getGeneratedKeys();
			if (generatedKeys.next()) {
			    int id = generatedKeys.getInt(1);
			    lastInsertedId = id;
		        insertPreparedStatement.close();	
		        col.id = "T-"+id;
		        Client.getServiceAnnuaire().ajouterCollab(col);
		        Client.setLastInsertedCollabTempId(id);
			} else {
				System.out.println("--- [ERROR] Error getting last id");
			}
			
			System.out.println("--- Collab temp inserted");
			dh.getConn().commit();

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error adding collab : " + e.getMessage());
			throw new Existe("--- [ERROR] Error adding collab ");
		}		
	}

	@Override
	public void supprimerCollabTemp(short idCol) throws NonExiste {
		System.out.println("Deleting collab temp : "+ idCol);
		PreparedStatement deletePS = null;
        String deleteQuery = "delete from Accueil where id = ? ;";
		try {
			deletePS = dh.getConn().prepareStatement(deleteQuery);
			
	        deletePS.setShort(1, idCol);

	        deletePS.executeUpdate();

	        deletePS.close();	
			System.out.println("--- Collab temp deleted");
			dh.getConn().commit();
			
			Client.getServiceAnnuaire().supprimerCollab("T-"+idCol);
		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error deleting collab temp : " + e.getMessage());
			throw new NonExiste("--- [ERROR] Error deleting collab temp ");
		}			
	}

	@Override
	public Collab getCollabTemp(short idCol) throws NonExiste {
		System.out.println("Getting collab temp : " +idCol);
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select * from Accueil where id = ?";
        CollabTemp c = null;
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setShort(1, idCol);

	        ResultSet rs = selectPreparedStatement.executeQuery();
	        System.out.println("--- Get Collab temp successful");
	        while (rs.next()) {
	        	Short id = rs.getShort("id");
	            c = new CollabTemp("T-"+id.toString(), rs.getString("nom"), rs.getString("prenom"), rs.getString("photo"));
	        }
	        selectPreparedStatement.close();
		} catch (SQLException e) {
            System.out.println("--- [ERROR] Error getting collab temp : " + e.getMessage());
			throw new NonExiste("--- [ERROR] Error getting collab temp ");

		}
		
        return c;
	}

	@Override
	public CollabTemp[] getCollabTemps() {
		System.out.println("Getting all collabs temps");
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select * from Accueil";
        ArrayList<CollabTemp> lesCollabs = new ArrayList<CollabTemp>();
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);

	        ResultSet rs = selectPreparedStatement.executeQuery();
	        System.out.println("--- Get Collabs successful");
	        while (rs.next()) {
	        	Short id = rs.getShort("id");
	            lesCollabs.add(new CollabTemp("T-"+id.toString(), rs.getString("nom"), rs.getString("prenom"), rs.getString("photo")));
	        }
	        selectPreparedStatement.close();
		} catch (SQLException e) {
            System.out.println("--- [ERROR] Error getting collabs temps : " + e.getMessage());

		}
		
		Object[] colTablab = lesCollabs.toArray();
		System.out.println("Collabs : " + lesCollabs.toString());

		CollabTemp[] collabArray = Arrays.copyOf(colTablab, colTablab.length, CollabTemp[].class);
		return collabArray;
		
	}

}
