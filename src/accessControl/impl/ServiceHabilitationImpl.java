package accessControl.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.omg.CORBA.ORB;

import accessControl.Collab;
import accessControl.Existe;
import accessControl.Habilitation;
import accessControl.NonExiste;
import accessControl.ServiceHabilitationPOA;
import accessControl.database.DatabaseHandler;

public class ServiceHabilitationImpl extends ServiceHabilitationPOA{
	private ORB orb;
	DatabaseHandler dh = new DatabaseHandler();
	
    public void initDB()
	{
    	System.out.println("Initializing HABILITATION DB");
		try
		{
			System.out.println("--- Connecting ...");
			dh.connectDatabase("Habilitation");
			System.out.println("--- Connected");
		}
		catch(Exception e)
		{
			System.out.println("--- [ERROR] Error creating db Habilitation : " + e.getMessage());
		}
		
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select * from Habilitation";
        
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
	        selectPreparedStatement.executeQuery();
	        selectPreparedStatement.close();

		} catch (SQLException e) {
			
            System.out.println("--- [WARNING] Warning table doesn't exist, creating it");
            
    		String req = "CREATE TABLE Habilitation (idCollab varchar(50), habilitation int, primary key(idCollab,habilitation));";
    		try
    		{
    			PreparedStatement ps = dh.getConn().prepareStatement(req);
                ps.executeUpdate();
                ps.close();
                System.out.println("--- Table HABILITATION created");
    		}
    		catch(Exception ex)
    		{
    			System.out.println("--- [ERROR] Error creating table Habilitation: " + ex.getMessage());
    		}	        
		}
	
	}
    
    public void setORB(ORB orb_val) {
		orb = orb_val; 
		}

	@Override
	public void ajouterHabilitation(Collab e, Habilitation h) throws Existe {
		PreparedStatement insertPreparedStatement = null;
        String insertQuery = "insert into Habilitation values(?,?);";
        System.out.println("Adding habilitation "+h.toString()+" for : "+e.toString());
		try {
			insertPreparedStatement = dh.getConn().prepareStatement(insertQuery);
			
	        insertPreparedStatement.setString(1, e.id);
	        insertPreparedStatement.setInt(2, h.value());

	        insertPreparedStatement.executeUpdate();

	        insertPreparedStatement.close();	
			System.out.println("--- Habilitation inserted");
			dh.getConn().commit();

		} catch (SQLException ex) {
			System.out.println("--- [ERROR] Error adding Habilitation : " + ex.getMessage());
			throw new Existe("--- [ERROR] Error adding Habilitation  ");
		}					
	}


	@Override
	public void supprimerHabilitation(Collab e, Habilitation h)
			throws NonExiste {
		PreparedStatement deletePS = null;
        String deleteQuery = "delete from Habilitation where idCollab = ? and habilitation = ?;";
        System.out.println("Deleting habilitation "+h.toString()+" for "+e.toString());
		try {
			deletePS = dh.getConn().prepareStatement(deleteQuery);
			
	        deletePS.setString(1, e.id);
	        deletePS.setInt(2, h.value());

	        deletePS.executeUpdate();

	        deletePS.close();	
			System.out.println("--- Habilitation  deleted");
			dh.getConn().commit();

		} catch (SQLException ex) {
			System.out.println("--- [ERROR] Error deleting Habilitation : " + ex.getMessage());
			throw new NonExiste("--- [ERROR] Error deleting Habilitation");
		}			
	}

	@Override
	public short[] getHabilitations(Collab e) throws NonExiste {
		System.out.println("Getting all habilitations for collab :" + e.toString());
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select habilitation from Habilitation where idCollab = ?";
        short[] lesHabilitations = new short[5];
        lesHabilitations[0] = 99;
        short[] lesHabRetour = null;
        int i = 0;
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setString(1, e.id);
	        ResultSet rs = selectPreparedStatement.executeQuery();
	        while (rs.next()) {
	            lesHabilitations[i] = rs.getShort("habilitation");
	            i++;
	        }
	        if (i == 0) {
	        	lesHabRetour = new short[1];
	        	lesHabRetour[0] = 99;	
			}else {
				lesHabRetour = new short[i];
				for (int j = 0; j < i; j++) {
					lesHabRetour[j] = lesHabilitations[j];
				}
			}
	        selectPreparedStatement.close();
	        System.out.println("--- Success");
		} catch (SQLException ex) {
            System.out.println("--- [ERROR] Error : " + ex.getMessage());
            throw new NonExiste("--- [ERROR] Error getting habilitations");
		}
		if(lesHabilitations == null)
			System.out.println("--- Habilitations : empty");
		else
			System.out.println("--- Habilitations : " + lesHabilitations.toString());

		return lesHabRetour;	
	}

	@Override
	public boolean verifierHabilitation(Collab e, Habilitation h) {
		
		System.out.println("Checking habilitation "+h.toString()+" for collab "+e.toString());
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select habilitation from Habilitation where idCollab = ? and habilitation = ?";
        boolean res = false;
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setString(1, e.id);
			selectPreparedStatement.setInt(2, h.value());

	        ResultSet rs = selectPreparedStatement.executeQuery();
	        if (rs.next()) {
		        System.out.println("--- Habilitation OK");

	        	res = true;
	        }
	        else
	        {
		        System.out.println("--- Habilitation NOK");

	        	res = false;
	        }
	        selectPreparedStatement.close();
		} catch (SQLException ex) {
            System.out.println("--- [ERROR] Error checking habilitation: " + ex.getMessage());

		}
		
		return res;
	}


}
