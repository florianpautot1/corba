package accessControl.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import org.omg.CORBA.ORB;

import accessControl.Collab;
import accessControl.Existe;
import accessControl.NonExiste;
import accessControl.ServiceRHOperations;
import accessControl.ServiceRHPOA;
import accessControl.clients.Client;
import accessControl.database.DatabaseHandler;
import accessControl.metier.CollabPerm;

public class ServiceRHImpl extends ServiceRHPOA{
	private ORB orb;
	DatabaseHandler dh = new DatabaseHandler();

    public void setORB(ORB orb_val) {
		orb = orb_val; 
		}
    
    public void initDB()
	{
    	System.out.println("Initializing RH DB ");
		try
		{
			System.out.println("--- Connecting");
			dh.connectDatabase("RH");
			System.out.println("--- Connected");
		}
		catch(Exception e)
		{
			System.out.println("Error creating db RH : " + e.getMessage());
		}
		
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select * from RH";
        
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
	        ResultSet rs = selectPreparedStatement.executeQuery();
	        selectPreparedStatement.close();

		} catch (SQLException e) {
			
            System.out.println("--- [Warning] Warning table doesn't exist, creating it ");
            
    		String req = "CREATE TABLE RH (id int auto_increment, nom varchar(255), prenom varchar(255), photo varchar(255), primary key(id));";
    		try
    		{
    			PreparedStatement ps = dh.getConn().prepareStatement(req);
                ps.executeUpdate();
                ps.close();
                System.out.println("--- Table RH created");
    		}
    		catch(Exception ex)
    		{
    			System.out.println("--- [ERROR] Error creating table Accueil: " + ex.getMessage());
    		}	        
		}
	}
    
    
	@Override
	public void ajouterCollabPerm(Collab col) throws Existe {
		System.out.println("Adding collab perm :"+ col.toString());
		PreparedStatement insertPreparedStatement = null;
        String insertQuery = "insert into RH(nom,prenom,photo) values(?,?,?);";
		try {
			insertPreparedStatement = dh.getConn().prepareStatement(insertQuery);
			
	        insertPreparedStatement.setString(1, col.nom);
	        insertPreparedStatement.setString(2, col.prenom);
	        insertPreparedStatement.setString(3, col.photo);

	        insertPreparedStatement.executeUpdate();

	        ResultSet generatedKeys = insertPreparedStatement.getGeneratedKeys();
			if (generatedKeys.next()) {
			    int id = generatedKeys.getInt(1);
			    col.id = "P-"+id;
			    Client.getServiceAnnuaire().ajouterCollab(col);
			    Client.setLastInsertedCollabPermId(id);
		        insertPreparedStatement.close();	
			} else {
				System.out.println("--- [ERROR] Error getting last id");
			}
			
			System.out.println("--- Collab perm inserted");
			dh.getConn().commit();

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error adding collab perm : " + e.getMessage());
			throw new Existe("--- [ERROR] Error adding collab perm");
		}				
	}

	@Override
	public void supprimerCollabPerm(short idCol) throws NonExiste {
		System.out.println("Deleting collab perm : "+idCol);
		PreparedStatement deletePS = null;
        String deleteQuery = "delete from RH where id = ? ;";
		try {
			deletePS = dh.getConn().prepareStatement(deleteQuery);
			
	        deletePS.setShort(1, idCol);

	        deletePS.executeUpdate();

	        deletePS.close();	
			System.out.println("--- Collab perm deleted");
			dh.getConn().commit();
		    Client.getServiceAnnuaire().supprimerCollab("P-"+idCol);

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error deleting collab perm : " + e.getMessage());
			throw new NonExiste("--- [ERROR] Error deleting collab perm ");
		}			
	}

	@Override
	public void modifierCollabPerm(Collab col) throws NonExiste {
		System.out.println("Updating collab perm : "+col.toString());
		PreparedStatement updatePS = null;
        String updateQuery = "update RH set nom = ?, prenom = ?, photo = ?  where id = ? ;";
		try {
			updatePS = dh.getConn().prepareStatement(updateQuery);
			
	        updatePS.setString(1, col.nom);
	        updatePS.setString(2, col.prenom);
	        updatePS.setString(3, col.photo);
	        updatePS.setString(4, col.id);

	        updatePS.executeUpdate();

	        updatePS.close();	
			System.out.println("--- Collab perm updated");
			dh.getConn().commit();
			col.id = "P-"+col.id;
			Client.getServiceAnnuaire().modifierCollab(col);
		} catch (SQLException ex) {
			System.out.println("--- [ERROR] Error updating collab perm  : " + ex.getMessage());
			throw new NonExiste("--- [ERROR] Error updating collab perm  ");
		}			
	}

	@Override
	public Collab getCollabPerm(short idCol) throws NonExiste {
		System.out.println("Getting collab perm : "+idCol);
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select * from RH where id = ?";
        CollabPerm c = new CollabPerm();
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setShort(1, idCol);

	        ResultSet rs = selectPreparedStatement.executeQuery();
	        if (rs.next()) {
	        	Short id = rs.getShort("id");
	            c = new CollabPerm("P-"+id.toString(), rs.getString("nom"), rs.getString("prenom"), rs.getString("photo"));
		        selectPreparedStatement.close();
		        System.out.println("--- Get Collab perm successful");
	        }
	        else
	        {
	            System.out.println("--- [ERROR] Collab doesn't exist  ");
				throw new NonExiste();
	        }

		} catch (SQLException e) {
            System.out.println("--- [ERROR] Error getting collab perm: " + e.getMessage());
            throw new NonExiste("--- [ERROR] Error getting collab perm");
		}
		
        return c;
	}
	@Override
	public CollabPerm[] getCollabPerms() {
		System.out.println("Getting all collabs perms ");
		PreparedStatement selectPreparedStatement = null;
        String selectQuery = "select * from RH";
        ArrayList<CollabPerm> lesCollabs = new ArrayList<CollabPerm>();
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);

	        ResultSet rs = selectPreparedStatement.executeQuery();
	        while (rs.next()) {
	        	Short id = rs.getShort("id");
	            lesCollabs.add(new CollabPerm("P-"+id.toString(), rs.getString("nom"), rs.getString("prenom"), rs.getString("photo")));
	        }
	        selectPreparedStatement.close();
	        
	        System.out.println("--- Get Collabs perms successful");

		} catch (SQLException e) {
            System.out.println("--- [ERROR] Error, Collab doesn't exist : " + e.getMessage());

		}
		
		Object[] colTablab = lesCollabs.toArray();
		System.out.println("Collabs : " + lesCollabs.toString());

		CollabPerm[] collabArray = Arrays.copyOf(colTablab, colTablab.length, CollabPerm[].class);
		return collabArray;
	}

}
