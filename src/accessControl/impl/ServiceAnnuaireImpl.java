package accessControl.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.omg.CORBA.ORB;

import accessControl.AnnuaireOperations;
import accessControl.AnnuairePOA;
import accessControl.Collab;
import accessControl.Existe;
import accessControl.NonExiste;
import accessControl.database.DatabaseHandler;

public class ServiceAnnuaireImpl extends AnnuairePOA {

	private ORB orb;
	DatabaseHandler dh = new DatabaseHandler();

	public ServiceAnnuaireImpl() throws ClassNotFoundException, SQLException {
		super();
		dh.connectDatabase("Annuaire");
	}

	public void setORB(ORB orb_val) {
		orb = orb_val;
	}

	@Override
	public void ajouterCollab(Collab col) throws Existe {
		System.out.println("Adding collab : " + col.toString());
		PreparedStatement insertPreparedStatement = null;
		String insertQuery = "insert into Annuaire values(?,?,?);";
		try {
			insertPreparedStatement = dh.getConn().prepareStatement(insertQuery);

			insertPreparedStatement.setString(1, col.id);
			insertPreparedStatement.setString(2, col.nom);
			insertPreparedStatement.setString(3, col.photo);

			insertPreparedStatement.executeUpdate();
			insertPreparedStatement.close();
			dh.getConn().commit();
			System.out.println("--- Collab inserted");

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error adding collab : " + e.getMessage());
			throw new Existe("--- [ERROR] Error adding collab : " + e.getMessage());
		}

	}

	@Override
	public void supprimerCollab(String idCol) throws NonExiste {
		System.out.println("Deleting collab : " + idCol);
		PreparedStatement deletePS = null;
		String deleteQuery = "delete from Annuaire where idCollab = ? ;";
		try {
			deletePS = dh.getConn().prepareStatement(deleteQuery);

			deletePS.setString(1, idCol);

			deletePS.executeUpdate();
			deletePS.close();
			System.out.println("--- Collab deleted");
			dh.getConn().commit();

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error deleting collab : " + e.getMessage());
			throw new NonExiste("--- [ERROR] Error deleting collab ");
		}
	}

	@Override
	public void modifierCollab(Collab col) throws NonExiste {
		System.out.println("Updating collab :" + col.toString());
		PreparedStatement insertPreparedStatement = null;
		String updateQuery = "update annuaire set name = ?, photo = ? where idCollab = ? ;";
		try {
			insertPreparedStatement = dh.getConn().prepareStatement(updateQuery);

			insertPreparedStatement.setString(1, col.nom);
			insertPreparedStatement.setString(2, col.photo);
			insertPreparedStatement.setString(3, col.id);

			insertPreparedStatement.executeUpdate();
			insertPreparedStatement.close();
			System.out.println("--- Collab updated");
			dh.getConn().commit();

		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error updating collab, user doesn't exist : " + e.getMessage());
			throw new NonExiste("--- [ERROR] Error updating collab, user doesn't exist ");
		}
	}

	@Override
	public Collab getCollab(String idCol) throws NonExiste {
		System.out.println("Getting collab : " + idCol);
		PreparedStatement selectPreparedStatement = null;
		String selectQuery = "select * from Annuaire where idCollab = ?";
		Collab c = null;
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setString(1, idCol);

			ResultSet rs = selectPreparedStatement.executeQuery();
			System.out.println("--- Get Collab successful");
			while (rs.next()) {
				c = new Collab(rs.getString("id"), rs.getString("name"), "", rs.getString("photo"));
			}
			selectPreparedStatement.close();
		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error, Collab doesn't exist : " + e.getMessage());
			throw new NonExiste("--- [ERROR] Error, Collab doesn't exist ");

		}

		return c;
	}

	public void initDB() {
		System.out.println("Initializing Annuaire DB ...");
		try {
			System.out.println("--- Connecting");
			dh.connectDatabase("Annuaire");
		} catch (Exception e) {
			System.out.println("Error creating db Annuaire : " + e.getMessage());
		}
		System.out.println("--- Conected");

		PreparedStatement selectPreparedStatement = null;
		String selectQuery = "select * from Annuaire";
		Collab c = null;
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			ResultSet rs = selectPreparedStatement.executeQuery();

			selectPreparedStatement.close();
		} catch (SQLException e) {
			System.out.println("---[WARNING] Error table doesn't exist, creating it");

			try {
				String req = "CREATE TABLE ANNUAIRE (idCollab varchar(50), name varchar(255), photo varchar(255), primary key(idCollab));";

				PreparedStatement ps = dh.getConn().prepareStatement(req);
				ps.executeUpdate();
				ps.close();
				System.out.println("--- Table Annuaire Created");
			} catch (Exception ex) {
				System.out.println("Error creating table Annuaire : " + ex.getMessage());
			}
		}

	}

	@Override
	public Collab[] getCollabs() {
		System.out.println("Getting all collabs");
		PreparedStatement selectPreparedStatement = null;
		String selectQuery = "select * from Annuaire";
		ArrayList<Collab> lesCollabs = new ArrayList<Collab>();
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);

			ResultSet rs = selectPreparedStatement.executeQuery();
			System.out.println("--- Get Collabs successful");
			while (rs.next()) {
				lesCollabs.add(new Collab(rs.getString("id"), rs.getString("name"), "", rs.getString("photo")));
			}
			selectPreparedStatement.close();
		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error, Collab doesn't exist : " + e.getMessage());

		}

		return (Collab[]) lesCollabs.toArray();
	}

	@Override
	public boolean verifierPhoto(String idCollab, String photo) {
		System.out.println("Checking photo");
		PreparedStatement selectPreparedStatement = null;
		String selectQuery = "select * from Annuaire where photo = ? and idCollab = ?";
		boolean res = false;
		try {
			selectPreparedStatement = dh.getConn().prepareStatement(selectQuery);
			selectPreparedStatement.setString(1, photo);
			selectPreparedStatement.setString(2, idCollab);

			ResultSet rs = selectPreparedStatement.executeQuery();
			if (rs.next()) {
				System.out.println("--- Check photo successful");
				res = true;
			}

			selectPreparedStatement.close();
		} catch (SQLException e) {
			System.out.println("--- [ERROR] Error, checking photo : " + e.getMessage());

		}

		return res;
	}

}
