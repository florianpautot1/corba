package accessControl;

/**
 * Interface definition : ServiceHabilitation
 * 
 * @author OpenORB Compiler
 */
public interface ServiceHabilitationOperations
{
    /**
     * Operation ajouterHabilitation
     */
    public void ajouterHabilitation(accessControl.Collab e, accessControl.Habilitation h)
        throws accessControl.Existe;

    /**
     * Operation supprimerHabilitation
     */
    public void supprimerHabilitation(accessControl.Collab e, accessControl.Habilitation h)
        throws accessControl.NonExiste;

    /**
     * Operation getHabilitations
     */
    public short[] getHabilitations(accessControl.Collab e)
        throws accessControl.NonExiste;

    /**
     * Operation verifierHabilitation
     */
    public boolean verifierHabilitation(accessControl.Collab e, accessControl.Habilitation h);

}
