package accessControl;

/** 
 * Helper class for : ServiceHabilitation
 *  
 * @author OpenORB Compiler
 */ 
public class ServiceHabilitationHelper
{
    /**
     * Insert ServiceHabilitation into an any
     * @param a an any
     * @param t ServiceHabilitation value
     */
    public static void insert(org.omg.CORBA.Any a, accessControl.ServiceHabilitation t)
    {
        a.insert_Object(t , type());
    }

    /**
     * Extract ServiceHabilitation from an any
     * @param a an any
     * @return the extracted ServiceHabilitation value
     */
    public static accessControl.ServiceHabilitation extract(org.omg.CORBA.Any a)
    {
        if (!a.type().equal(type()))
            throw new org.omg.CORBA.MARSHAL();
        try {
            return accessControl.ServiceHabilitationHelper.narrow(a.extract_Object());
        } catch (final org.omg.CORBA.BAD_PARAM e) {
            throw new org.omg.CORBA.MARSHAL(e.getMessage());
        }
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the ServiceHabilitation TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            _tc = orb.create_interface_tc(id(),"ServiceHabilitation");
        }
        return _tc;
    }

    /**
     * Return the ServiceHabilitation IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:accessControl/ServiceHabilitation:1.0";

    /**
     * Read ServiceHabilitation from a marshalled stream
     * @param istream the input stream
     * @return the readed ServiceHabilitation value
     */
    public static accessControl.ServiceHabilitation read(org.omg.CORBA.portable.InputStream istream)
    {
        return(accessControl.ServiceHabilitation)istream.read_Object(accessControl._ServiceHabilitationStub.class);
    }

    /**
     * Write ServiceHabilitation into a marshalled stream
     * @param ostream the output stream
     * @param value ServiceHabilitation value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, accessControl.ServiceHabilitation value)
    {
        ostream.write_Object((org.omg.CORBA.portable.ObjectImpl)value);
    }

    /**
     * Narrow CORBA::Object to ServiceHabilitation
     * @param obj the CORBA Object
     * @return ServiceHabilitation Object
     */
    public static ServiceHabilitation narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof ServiceHabilitation)
            return (ServiceHabilitation)obj;

        if (obj._is_a(id()))
        {
            _ServiceHabilitationStub stub = new _ServiceHabilitationStub();
            stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
            return stub;
        }

        throw new org.omg.CORBA.BAD_PARAM();
    }

    /**
     * Unchecked Narrow CORBA::Object to ServiceHabilitation
     * @param obj the CORBA Object
     * @return ServiceHabilitation Object
     */
    public static ServiceHabilitation unchecked_narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof ServiceHabilitation)
            return (ServiceHabilitation)obj;

        _ServiceHabilitationStub stub = new _ServiceHabilitationStub();
        stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
        return stub;

    }

}
