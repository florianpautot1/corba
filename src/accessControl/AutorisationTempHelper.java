package accessControl;

/** 
 * Helper class for : AutorisationTemp
 *  
 * @author OpenORB Compiler
 */ 
public class AutorisationTempHelper
{
    private static final boolean HAS_OPENORB;
    static {
        boolean hasOpenORB = false;
        try {
            Thread.currentThread().getContextClassLoader().loadClass("org.openorb.CORBA.Any");
            hasOpenORB = true;
        }
        catch(ClassNotFoundException ex) {
        }
        HAS_OPENORB = hasOpenORB;
    }
    /**
     * Insert AutorisationTemp into an any
     * @param a an any
     * @param t AutorisationTemp value
     */
    public static void insert(org.omg.CORBA.Any a, accessControl.AutorisationTemp t)
    {
        a.insert_Streamable(new accessControl.AutorisationTempHolder(t));
    }

    /**
     * Extract AutorisationTemp from an any
     * @param a an any
     * @return the extracted AutorisationTemp value
     */
    public static accessControl.AutorisationTemp extract(org.omg.CORBA.Any a)
    {
        if (!a.type().equal(type()))
            throw new org.omg.CORBA.MARSHAL();
        if (HAS_OPENORB && a instanceof org.openorb.CORBA.Any) {
            // streamable extraction. The jdk stubs incorrectly define the Any stub
            org.openorb.CORBA.Any any = (org.openorb.CORBA.Any)a;
            try {
                org.omg.CORBA.portable.Streamable s = any.extract_Streamable();
                if(s instanceof accessControl.AutorisationTempHolder)
                    return ((accessControl.AutorisationTempHolder)s).value;
            } catch (org.omg.CORBA.BAD_INV_ORDER ex) {
            }
            accessControl.AutorisationTempHolder h = new accessControl.AutorisationTempHolder(read(a.create_input_stream()));
            a.insert_Streamable(h);
            return h.value;
        }
        return read(a.create_input_stream());
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;
    private static boolean _working = false;

    /**
     * Return the AutorisationTemp TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            synchronized(org.omg.CORBA.TypeCode.class) {
                if (_tc != null)
                    return _tc;
                if (_working)
                    return org.omg.CORBA.ORB.init().create_recursive_tc(id());
                _working = true;
                org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
                org.omg.CORBA.StructMember []_members = new org.omg.CORBA.StructMember[7];

                _members[0] = new org.omg.CORBA.StructMember();
                _members[0].name = "id";
                _members[0].type = orb.get_primitive_tc(org.omg.CORBA.TCKind.tk_string);
                _members[1] = new org.omg.CORBA.StructMember();
                _members[1].name = "idCollab";
                _members[1].type = orb.get_primitive_tc(org.omg.CORBA.TCKind.tk_string);
                _members[2] = new org.omg.CORBA.StructMember();
                _members[2].name = "hDeb";
                _members[2].type = orb.get_primitive_tc(org.omg.CORBA.TCKind.tk_string);
                _members[3] = new org.omg.CORBA.StructMember();
                _members[3].name = "hFin";
                _members[3].type = orb.get_primitive_tc(org.omg.CORBA.TCKind.tk_string);
                _members[4] = new org.omg.CORBA.StructMember();
                _members[4].name = "idZone";
                _members[4].type = orb.get_primitive_tc(org.omg.CORBA.TCKind.tk_short);
                _members[5] = new org.omg.CORBA.StructMember();
                _members[5].name = "jourDeb";
                _members[5].type = orb.get_primitive_tc(org.omg.CORBA.TCKind.tk_string);
                _members[6] = new org.omg.CORBA.StructMember();
                _members[6].name = "jourFin";
                _members[6].type = orb.get_primitive_tc(org.omg.CORBA.TCKind.tk_string);
                _tc = orb.create_struct_tc(id(),"AutorisationTemp",_members);
                _working = false;
            }
        }
        return _tc;
    }

    /**
     * Return the AutorisationTemp IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:accessControl/AutorisationTemp:1.0";

    /**
     * Read AutorisationTemp from a marshalled stream
     * @param istream the input stream
     * @return the readed AutorisationTemp value
     */
    public static accessControl.AutorisationTemp read(org.omg.CORBA.portable.InputStream istream)
    {
        accessControl.AutorisationTemp new_one = new accessControl.AutorisationTemp();

        new_one.id = istream.read_string();
        new_one.idCollab = istream.read_string();
        new_one.hDeb = istream.read_string();
        new_one.hFin = istream.read_string();
        new_one.idZone = istream.read_short();
        new_one.jourDeb = istream.read_string();
        new_one.jourFin = istream.read_string();

        return new_one;
    }

    /**
     * Write AutorisationTemp into a marshalled stream
     * @param ostream the output stream
     * @param value AutorisationTemp value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, accessControl.AutorisationTemp value)
    {
        ostream.write_string(value.id);
        ostream.write_string(value.idCollab);
        ostream.write_string(value.hDeb);
        ostream.write_string(value.hFin);
        ostream.write_short(value.idZone);
        ostream.write_string(value.jourDeb);
        ostream.write_string(value.jourFin);
    }

}
