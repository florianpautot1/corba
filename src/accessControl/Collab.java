package accessControl;

/**
 * Struct definition : Collab
 * 
 * @author OpenORB Compiler
*/
public class Collab implements org.omg.CORBA.portable.IDLEntity
{
    @Override
	public String toString() {
		return "Collab [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", photo=" + photo + "]";
	}

	/**
     * Struct member id
     */
    public String id;

    /**
     * Struct member nom
     */
    public String nom;

    /**
     * Struct member prenom
     */
    public String prenom;

    /**
     * Struct member photo
     */
    public String photo;

    /**
     * Default constructor
     */
    public Collab()
    { }

    /**
     * Constructor with fields initialization
     * @param id id struct member
     * @param nom nom struct member
     * @param prenom prenom struct member
     * @param photo photo struct member
     */
    public Collab(String id, String nom, String prenom, String photo)
    {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.photo = photo;
    }

}
