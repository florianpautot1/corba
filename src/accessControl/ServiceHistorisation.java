package accessControl;

/**
 * Interface definition : ServiceHistorisation
 * 
 * @author OpenORB Compiler
 */
public interface ServiceHistorisation extends ServiceHistorisationOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
