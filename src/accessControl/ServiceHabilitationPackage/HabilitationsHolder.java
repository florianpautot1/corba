package accessControl.ServiceHabilitationPackage;

/**
 * Holder class for : Habilitations
 * 
 * @author OpenORB Compiler
 */
final public class HabilitationsHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal Habilitations value
     */
    public short[] value;

    /**
     * Default constructor
     */
    public HabilitationsHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public HabilitationsHolder(short[] initial)
    {
        value = initial;
    }

    /**
     * Read Habilitations from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = HabilitationsHelper.read(istream);
    }

    /**
     * Write Habilitations into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        HabilitationsHelper.write(ostream,value);
    }

    /**
     * Return the Habilitations TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return HabilitationsHelper.type();
    }

}
