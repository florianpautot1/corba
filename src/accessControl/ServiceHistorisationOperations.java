package accessControl;

/**
 * Interface definition : ServiceHistorisation
 * 
 * @author OpenORB Compiler
 */
public interface ServiceHistorisationOperations
{
    /**
     * Operation sauvegarderPassage
     */
    public void sauvegarderPassage(accessControl.Passage p);

    /**
     * Operation getPassages
     */
    public accessControl.Passage[] getPassages();

    /**
     * Operation getPassagesParZones
     */
    public accessControl.Passage[] getPassagesParZones(short idZone);

}
