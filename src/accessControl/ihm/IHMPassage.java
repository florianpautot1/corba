package accessControl.ihm;

import java.awt.Dimension;

import javax.swing.WindowConstants;

import accessControl.Passage;
import accessControl.ServiceHistorisation;
import accessControl.clients.Client;
import accessControl.ihm.Ihm.WListener;
import accessControl.model.ModeleDynamiquePassage;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IHMPassage extends Ihm{
	private ServiceHistorisation SH;
	private JTable lesPassages;
    private ModeleDynamiquePassage modele = new ModeleDynamiquePassage();
	private JLabel lblHistoriqueDesPassages = new JLabel("Historique des passages");
	private JScrollPane tableSc;
	private JButton btnFermer = new JButton("Fermer");

	public IHMPassage(short idZone) {

		init();
		initComponents();
		addComponentsHandlers();

		this.setVisible(true);
		

	}

	private void init()
	{
		WListener wlist = new WListener(this);
		this.addWindowListener(wlist);
		getContentPane().setLayout(null);

		this.setTitle("Service Passage");
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setPreferredSize(new Dimension(800, 600));
		this.pack();
		this.setLocationRelativeTo(null);
	}
	
	private void initComponents()
	{
		lblHistoriqueDesPassages.setBounds(10, 11, 348, 14);
		getContentPane().add(lblHistoriqueDesPassages);
		
		//Remplissage de la table
		lesPassages = new JTable(modele);
		tableSc = new JScrollPane(lesPassages);
		tableSc.setColumnHeaderView(lesPassages.getTableHeader());
		lesPassages.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lesPassages.setShowVerticalLines(false);
		tableSc.setBounds(10, 30, 750, 470);
		getContentPane().add(tableSc);
		
		
		btnFermer.setBounds(351, 527, 107, 23);
		getContentPane().add(btnFermer);
	}
	
	private void addComponentsHandlers()
	{
		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
	}
}
