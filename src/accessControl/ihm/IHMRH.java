package accessControl.ihm;

import javax.swing.JLabel;

import java.awt.Dimension;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;

import accessControl.AutorisationPerm;
import accessControl.AutorisationTemp;
import accessControl.Collab;
import accessControl.Existe;
import accessControl.NonExiste;
import accessControl.ServiceAuthentification;
import accessControl.ServiceAutorisation;
import accessControl.ServiceRH;
import accessControl.clients.Client;
import accessControl.ihm.Ihm.WListener;
import accessControl.metier.CollabPerm;
import accessControl.model.ModeleDynamiqueCollaborateurPerm;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class IHMRH extends Ihm{

	private ServiceAuthentification SA;
	
	private JTable lesCollaborateursPerm;
    
	private ModeleDynamiqueCollaborateurPerm modele = new ModeleDynamiqueCollaborateurPerm();
	
    private JTextField inputNomAjouter;
	private JTextField inputPrenomAjouter;
	private JTextField inputPhotoAjouter;
	private JTextField inputNomModifier;
	private JTextField inputPrenomModifier;
	private JTextField inputPhotoModifier;
	private JTextField inputMdpAjouter;
	private JTextField inputMdpModifier;
	
	private JScrollPane tableSc;
	
	private ServiceAutorisation SAuto;
	
	private JButton btnAjouterCollabPerm = new JButton("Ajouter");
	private JButton btnSupprimerCollabPerm = new JButton("Supprimer");
	private JButton bntModifierCollaborateurPerm = new JButton("Modifier");
	private JButton btnAppliquer = new JButton("Appliquer");
	
	private Collab cASuppr;
	private CollabPerm cModif = new CollabPerm();

	private JLabel lblNomAjouter = new JLabel("Nom");
	private JLabel lblPrnom = new JLabel("Pr\u00E9nom");
	private JLabel lblPhoto = new JLabel("Photo");
	private JLabel lblAjouterUnCollaborateur = new JLabel("Ajouter un collaborateur permanent");
	private JLabel lblModifierUnCollaborateur = new JLabel("Modifier un collaborateur permanent");
	private JLabel label = new JLabel("Nom");
	private JLabel label_1 = new JLabel("Pr\u00E9nom");
	private JLabel label_2 = new JLabel("Photo");
	private JLabel lblMdp = new JLabel("Mdp.");
	private JLabel label_3 = new JLabel("Mdp.");
	
	public static void main(String args[]){ 
		IHMRH frame = new IHMRH();
	}
	
	
	public IHMRH() {

		init();
		initComponents();
		addComponentsHandler();
		
		
		this.setVisible(true);
	}
	
	private void init()
	{
		this.setTitle("Client RH");
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setPreferredSize(new Dimension(600, 600));
		this.pack();
		this.setLocationRelativeTo(null);
		
		WListener wlist = new WListener(this);
		this.addWindowListener(wlist);
		SA = Client.getServiceAuthentification();
		SAuto = Client.getServiceAutorisation();
		getContentPane().setLayout(null);
	}
	
	private void initComponents()
	{
		 //Remplissage de la table
		lesCollaborateursPerm = new JTable(modele);
		tableSc = new JScrollPane(lesCollaborateursPerm);
		tableSc.setColumnHeaderView(lesCollaborateursPerm.getTableHeader());
		lesCollaborateursPerm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lesCollaborateursPerm.setShowVerticalLines(false);
		tableSc.setBounds(10, 10, 560, 316);
		getContentPane().add(tableSc);
		
		lblNomAjouter.setBounds(20, 444, 46, 14);
		getContentPane().add(lblNomAjouter);
		
		inputNomAjouter = new JTextField();
		inputNomAjouter.setBounds(76, 441, 120, 20);
		getContentPane().add(inputNomAjouter);
		inputNomAjouter.setColumns(10);
		
		inputPrenomAjouter = new JTextField();
		inputPrenomAjouter.setColumns(10);
		inputPrenomAjouter.setBounds(76, 410, 120, 20);
		getContentPane().add(inputPrenomAjouter);
		
		inputPhotoAjouter = new JTextField();
		inputPhotoAjouter.setColumns(10);
		inputPhotoAjouter.setBounds(76, 472, 120, 20);
		getContentPane().add(inputPhotoAjouter);

		
		btnAjouterCollabPerm.setBounds(76, 527, 120, 23);
		getContentPane().add(btnAjouterCollabPerm);

		
		btnSupprimerCollabPerm.setBounds(470, 337, 100, 23);
		getContentPane().add(btnSupprimerCollabPerm);
		

		lblPrnom.setBounds(20, 413, 46, 14);
		getContentPane().add(lblPrnom);

		lblPhoto.setBounds(20, 475, 46, 14);
		getContentPane().add(lblPhoto);

		lblAjouterUnCollaborateur.setBounds(10, 388, 225, 14);
		getContentPane().add(lblAjouterUnCollaborateur);
		

		
		bntModifierCollaborateurPerm.setBounds(360, 337, 100, 23);
		getContentPane().add(bntModifierCollaborateurPerm);
		
		inputNomModifier = new JTextField();
		inputNomModifier.setBounds(385, 441, 120, 20);
		getContentPane().add(inputNomModifier);
		inputNomModifier.setColumns(10);
		
		inputPrenomModifier = new JTextField();
		inputPrenomModifier.setColumns(10);
		inputPrenomModifier.setBounds(385, 410, 120, 20);
		getContentPane().add(inputPrenomModifier);
		
		inputPhotoModifier = new JTextField();
		inputPhotoModifier.setBounds(385, 472, 120, 20);
		getContentPane().add(inputPhotoModifier);
		inputPhotoModifier.setColumns(10);
		

		
		btnAppliquer.setBounds(385, 527, 120, 23);
		getContentPane().add(btnAppliquer);
		

		lblModifierUnCollaborateur.setBounds(311, 388, 225, 14);
		getContentPane().add(lblModifierUnCollaborateur);
		

		label.setBounds(329, 444, 46, 14);
		getContentPane().add(label);
		

		label_1.setBounds(329, 413, 46, 14);
		getContentPane().add(label_1);
		

		label_2.setBounds(329, 475, 46, 14);
		getContentPane().add(label_2);
		
		inputMdpAjouter = new JTextField();
		inputMdpAjouter.setColumns(10);
		inputMdpAjouter.setBounds(76, 503, 120, 20);
		getContentPane().add(inputMdpAjouter);
		
		inputMdpModifier = new JTextField();
		inputMdpModifier.setColumns(10);
		inputMdpModifier.setBounds(385, 503, 120, 20);
		getContentPane().add(inputMdpModifier);
		

		lblMdp.setBounds(329, 506, 46, 14);
		getContentPane().add(lblMdp);
		

		label_3.setBounds(20, 509, 46, 14);
		getContentPane().add(label_3);
	}
	
	private void addComponentsHandler()
	{
		btnAjouterCollabPerm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Creation collaborateur
				Collab nouveauCollabPerm = new Collab();
				nouveauCollabPerm.id = "P-"+Client.getLastInsertedCollabPermId();
				nouveauCollabPerm.nom = inputNomAjouter.getText();
				nouveauCollabPerm.prenom = inputPrenomAjouter.getText();
				nouveauCollabPerm.photo = inputPhotoAjouter.getText();
				System.out.println("Nouveau collab saisi : "+nouveauCollabPerm.toString());
				modele.addCollabPerm(nouveauCollabPerm);
				try {
					SA.modifierMdp(nouveauCollabPerm, inputMdpAjouter.getText());
				} catch (NonExiste e1) {
					System.out.println("--- [ERROR] Error updating mdp : " + e1.raison);

				}
				inputNomAjouter.setText("");
				inputPrenomAjouter.setText("");
				inputPhotoAjouter.setText("");		
				inputMdpAjouter.setText("");
			}
		});
		
		btnSupprimerCollabPerm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selection = lesCollaborateursPerm.getSelectedRow();
				cASuppr = modele.getCollabPerm(selection);
				modele.removeCollabPerm(selection);
				// Suppr. en cascade
				try {
					SA.supprimerEmpreinte(cASuppr);
				} catch (NonExiste e1) {
					System.out.println("--- [ERROR] Error deleting empreinte : " + e1.raison);

				}
				//autorisations
				try {
					AutorisationPerm[] lesAuto = SAuto.getAutorisationsPerms(cASuppr);
					for (int i = 0; i < lesAuto.length; i++) {
						SAuto.supprimerAutorisation(lesAuto[i].id);
					}
				} catch (NonExiste e1) {
					System.out.println("--- [ERROR] Error deleting autorisations : " + e1.raison);

				}
			}
		});
		
		bntModifierCollaborateurPerm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// recuperation des anciennes informations
				cModif.id = (String) modele.getValueAt(lesCollaborateursPerm.getSelectedRow(), 0);
				cModif.prenom = (String) modele.getValueAt(lesCollaborateursPerm.getSelectedRow(), 1);
				cModif.nom = (String) modele.getValueAt(lesCollaborateursPerm.getSelectedRow(), 2);
				cModif.photo = (String) modele.getValueAt(lesCollaborateursPerm.getSelectedRow(), 3);
				inputNomModifier.setText(cModif.nom);
				inputPrenomModifier.setText(cModif.prenom);
				inputPhotoModifier.setText(cModif.photo);
				inputMdpModifier.setText("");
			}
		});
		
		btnAppliquer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// recuperation des modifications
				cModif.prenom = inputPrenomModifier.getText();
				cModif.nom = inputNomModifier.getText();
				cModif.photo = inputPhotoModifier.getText();
				modele.modifyCollabPerm(lesCollaborateursPerm.getSelectedRow(), cModif);
				try {
					SA.modifierMdp(cModif, inputMdpModifier.getText());
				} catch (NonExiste e1) {
					System.out.println("--- [ERROR] Error udpating mdp : " + e1.raison);
				}
				inputNomModifier.setText("");
				inputPrenomModifier.setText("");
				inputPhotoModifier.setText("");
				inputMdpModifier.setText("");
			}
		});
	}
}
