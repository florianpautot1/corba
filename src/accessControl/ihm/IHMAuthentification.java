package accessControl.ihm;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.JButton;
import javax.swing.JComboBox;

import accessControl.Authentification;
import accessControl.Collab;
import accessControl.Existe;
import accessControl.NonExiste;
import accessControl.ServiceAcceuil;
import accessControl.ServiceAuthentification;
import accessControl.ServiceAutorisation;
import accessControl.ServiceHabilitation;
import accessControl.ServiceRH;
import accessControl.clients.Client;
import accessControl.ihm.Ihm.WListener;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IHMAuthentification extends Ihm {

	private JTextField inputEmpreinteVerifier;
	private JTextField inputEmpreinteDefinir;
	private JTextField inputMotDePasseDefinir;
	private JTextField inputEmpreinteModifier;
	private JTextField inputMdpModifier;

	private JButton btnModifierMdp = new JButton("Modifier le mot de passe");
	private JButton btnAfficher = new JButton("Afficher");
	
	private JPanel definirAuthentification = new JPanel();
	private JPanel modifierEmpreinte = new JPanel();
	private JPanel verifierEmpreinte = new JPanel();
	
	private JComboBox listCollaborateurDefinirAuth;

	// Client qui va taper dans l'annuaire

	private ServiceAuthentification SAuth;
	private ServiceRH SRh;
	private ServiceAcceuil SAccu;
	private ServiceHabilitation SH;
	
	// Liste necessaires
	private Collab allCollab[];
	private Collab allCollabTemp[];
	private Collab allCollabPerm[];

	// Informations sur le collaborateur
	private String idCollaborateur;
	private String collaborateur;

	private boolean estPermanent;
	private boolean estAccueil;
	private boolean estRH;

	// Autres variables
	private JLabel lblServiceAuthentification = new JLabel("Service AUTHENTIFICATION");
	private JLabel lblNouveauMdp = new JLabel("Nouveau mdp");
	private JLabel EmpreinteModifier = new JLabel("Nouvelle empreinte");


	private JButton btnVerifierEmpreinte = new JButton("Verifier");
	private JButton btnModifierEmpreinte = new JButton("Modifier l'empreinte");

	private JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);

	public IHMAuthentification(Collab collab) {

		idCollaborateur = collab.id;
		
		init();
		initComponents();
		addComponentsHandlers();
		this.setVisible(true);
	}

	/* V�rifie si un collaborateur est temporaire ou non */
	public boolean estPermanent(String idCollab) {

		// Split de l'ID
		String chaineListBox = idCollab;
		String[] parts = chaineListBox.split("-");
		String part1 = parts[0];
		String part2 = parts[1];

		// Traitement
		boolean estPermanent = false;
		if (getClient().getCollab().id.contains("P"))
			estPermanent = true;
		else
			estPermanent = false;
		return estPermanent;
	}

	/* V�rifie si un collaborateur est accueil ou non */
	public boolean estAccueil(String idCollab) throws NonExiste {

		Collab collab;
		short tabHabilitations[];
		boolean estPermanent = estPermanent(idCollab);
		boolean estAccueil = false;

		// Split de l'ID
		String chaineListBox = idCollab;
		String[] parts = chaineListBox.split("-");
		String part1 = parts[0];
		String part2 = parts[1];

		// Test si collaborateur temporaire ou non
		if (estPermanent == false)
			collab = SAccu.getCollabTemp(Short.parseShort(part2));
		else
			collab = SRh.getCollabPerm(Short.parseShort(part2));

		// Recuperation de ses habilitations
		tabHabilitations = SH.getHabilitations(collab);
		for (int i = 0; i < tabHabilitations.length; i++) {
			if (tabHabilitations[i] == 2)
				estAccueil = true;
		}
		return estAccueil;
	}

	/* V�rifie si un collaborateur est rh ou non */
	public boolean estRH(String idCollab) throws NonExiste {

		ServiceRH SRh = getClient().getServiceRH();
		ServiceAcceuil SAccu = getClient().getServiceAccueil();
		ServiceAuthentification SAuth = getClient().getServiceAuthentification();
		ServiceHabilitation SH = getClient().getServiceHabilitation();

		Collab collab;
		short tabHabilitations[];
		boolean estPermanent = estPermanent(idCollab);
		boolean estRH = false;

		// Split de l'ID
		String chaineListBox = idCollab;
		String[] parts = chaineListBox.split("-");
		String part1 = parts[0];
		String part2 = parts[1];

		// Test si collaborateur temporaire ou non
		if (estPermanent == false)
			collab = SAccu.getCollabTemp(Short.parseShort(part2));
		else
			collab = SRh.getCollabPerm(Short.parseShort(part2));

		// Recuperation de ses habilitations
		tabHabilitations = SH.getHabilitations(collab);
		for (int i = 0; i < tabHabilitations.length; i++) {
			if (tabHabilitations[i] == 0)
				estRH = true;
		}
		return estRH;
	}

	/* R�cup�re l'id d'un collaborateur a partir de la listbox */
	public short getIdFromListBox(JComboBox listCollab) {

		String chaineListBox = listCollab.getSelectedItem().toString();
		String[] parts = chaineListBox.split("-");
		String part1 = parts[0];
		String part2 = parts[1];
		part1.trim();
		return Short.parseShort(part2);
	}
	
	/* R�cup�re l'id d'un collaborateur a partir de la listbox */
	public boolean estPermSelected(JComboBox listCollab) {
		
		boolean estPerm;
		String chaineListBox = listCollab.getSelectedItem().toString();
		String[] parts = chaineListBox.split("-");
		String part1 = parts[0]; 
		String part2 = parts[1];
		part1.trim();
		if (part1.contains("P")) return true;
		else return false;
	}
	
	private void addComponentsHandlers()
	{
		btnVerifierEmpreinte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// Verification de l'empreinte
				String idEmpreinte;
				idEmpreinte = SAuth.verificationEmpreinte(inputEmpreinteVerifier.getText().toString());

				// Empreinte reconnue
				if (!idEmpreinte.isEmpty())
					JOptionPane.showMessageDialog(null, "L'empreinte existe",
							"InfoBox: " + "ERREUR - Verification d'une empreinte", JOptionPane.INFORMATION_MESSAGE);

				// Empreinte inconnue
				else
					JOptionPane.showMessageDialog(null, "Empreinte inconnue",
							"InfoBox: " + "ERREUR - Verification d'une empreinte", JOptionPane.INFORMATION_MESSAGE);

				// On vide l'input
				inputEmpreinteVerifier.setText("");
			}
		});

		btnModifierEmpreinte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// On retrouve l'objet Collaborateur
				Collab collab = new Collab();
				if (estPermanent == false) {
					try {
						collab = SAccu.getCollabTemp(Short.parseShort(getClient().getIdCollab()));
						// Enregistrement des donnees
						try {
							SAuth.modifierEmpreinte(collab, inputEmpreinteModifier.getText().toString());
							JOptionPane.showMessageDialog(null,
									"L'empreinte de " + collab.prenom + " " + collab.nom + " " + "a �t� modifi�e",
									"InfoBox: " + "ERREUR - Collaborateur", JOptionPane.INFORMATION_MESSAGE);
							inputEmpreinteModifier.setText("");
						} catch (NonExiste e1) {
							JOptionPane.showMessageDialog(null,
									"L'empreinte de " + collab.prenom + " " + collab.nom + " " + "n'a pas �t� modifi�e",
									"InfoBox: " + "ERREUR - Authentification", JOptionPane.INFORMATION_MESSAGE);
							inputEmpreinteModifier.setText("");
							System.out.println("--- [ERROR] Error modifiying empreinte : " + e1.raison);

						}
					} catch (NonExiste e1) {
						System.out.println("--- [ERROR] Error : " + e1.raison);

					}
				} else {
					try {
						collab = SRh.getCollabPerm(Short.parseShort(getClient().getIdCollab()));
						// Enregistrement des donnees
						try {
							SAuth.modifierEmpreinte(collab, inputEmpreinteModifier.getText().toString());
							JOptionPane.showMessageDialog(null,
									"L'empreinte de " + collab.prenom + " " + collab.nom + " " + "a �t� modifi�e",
									"InfoBox: " + "VALIDATION - Collaborateur", JOptionPane.INFORMATION_MESSAGE);
							inputEmpreinteModifier.setText("");
						} catch (NonExiste e2) {
							JOptionPane.showMessageDialog(null,
									"L'empreinte de " + collab.prenom + " " + collab.nom + " " + "n'a pas �t� modifi�e",
									"InfoBox: " + "ERREUR - Authentification", JOptionPane.INFORMATION_MESSAGE);
							inputEmpreinteModifier.setText("");
							System.out.println("--- [ERROR] Error modifying empreinte : " + e2.raison);

						}
					} catch (NonExiste e3) {
						JOptionPane.showMessageDialog(null, "Le collaborateur n'existe pas",
								"InfoBox: " + "ERREUR - Authentification", JOptionPane.INFORMATION_MESSAGE);
						System.out.println("--- [ERROR] Error collab doesn't exist : " + e3.raison);

					}
				}
			}
		});
		
		btnModifierMdp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// On retrouve l'objet Collaborateur
				Collab collab = new Collab();
				if (estPermanent == false) {
					try {
						collab = SAccu.getCollabTemp(Short.parseShort(getClient().getIdCollab()));
						// Enregistrement des donnees
						try {
							SAuth.modifierMdp(collab, inputMdpModifier.getText().toString());
							JOptionPane.showMessageDialog(null,
									"Le mdp de " + collab.prenom + " " + collab.nom + " " + "a �t� modifi�",
									"InfoBox: " + "ERREUR - Collaborateur", JOptionPane.INFORMATION_MESSAGE);
							inputMdpModifier.setText("");
						} catch (NonExiste e1) {
							JOptionPane.showMessageDialog(null,
									"Le mdp de " + collab.prenom + " " + collab.nom + " " + "n'a pas �t� modifi�",
									"InfoBox: " + "ERREUR - Authentification", JOptionPane.INFORMATION_MESSAGE);
							inputMdpModifier.setText("");
							System.out.println("--- [ERROR] Error modifying password : " + e1.raison);

						}
					} catch (NonExiste e1) {
						System.out.println("--- [ERROR] Error : " + e1.raison);

					}
				} else {
					try {
						collab = SRh.getCollabPerm(Short.parseShort(getClient().getIdCollab()));
						// Enregistrement des donnees
						try {
							SAuth.modifierMdp(collab, inputMdpModifier.getText().toString());
							JOptionPane.showMessageDialog(null,
									"Le mdp de " + collab.prenom + " " + collab.nom + " " + "a �t� modifi�",
									"InfoBox: " + "VALIDATION - Collaborateur", JOptionPane.INFORMATION_MESSAGE);
							inputMdpModifier.setText("");
						} catch (NonExiste e2) {
							JOptionPane.showMessageDialog(null,
									"Le mdp de " + collab.prenom + " " + collab.nom + " " + "n'a pas �t� modifi�",
									"InfoBox: " + "ERREUR - Authentification", JOptionPane.INFORMATION_MESSAGE);
							inputMdpModifier.setText("");
							System.out.println("--- [ERROR] Error adding collab temp : " + e2.raison);

						}
					} catch (NonExiste e3) {
						JOptionPane.showMessageDialog(null, "Le collaborateur n'existe pas",
								"InfoBox: " + "ERREUR - Authentification", JOptionPane.INFORMATION_MESSAGE);
						System.out.println("--- [ERROR] Error collab doesn't exist : " + e3.raison);

					}
				}
			}
		});
	}

	private void initComponents()
	{
		lblServiceAuthentification.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblServiceAuthentification.setBounds(10, 11, 258, 25);
		getContentPane().add(lblServiceAuthentification);

		tabbedPane.setBounds(10, 47, 513, 181);
		getContentPane().add(tabbedPane);

		tabbedPane.addTab("Verifier une empreinte", null, verifierEmpreinte, null);
		verifierEmpreinte.setLayout(null);

		inputEmpreinteVerifier = new JTextField();
		inputEmpreinteVerifier.setBounds(10, 11, 483, 20);
		verifierEmpreinte.add(inputEmpreinteVerifier);
		inputEmpreinteVerifier.setColumns(10);

		btnVerifierEmpreinte.setBounds(10, 42, 483, 23);
		verifierEmpreinte.add(btnVerifierEmpreinte);

		tabbedPane.addTab("Modifier Identifiants", null, modifierEmpreinte, null);
		modifierEmpreinte.setLayout(null);

		btnModifierEmpreinte.setBounds(10, 36, 483, 23);
		modifierEmpreinte.add(btnModifierEmpreinte);

		EmpreinteModifier.setBounds(15, 11, 143, 14);
		modifierEmpreinte.add(EmpreinteModifier);

		inputEmpreinteModifier = new JTextField();
		inputEmpreinteModifier.setColumns(10);
		inputEmpreinteModifier.setBounds(173, 8, 320, 20);
		modifierEmpreinte.add(inputEmpreinteModifier);

		lblNouveauMdp.setBounds(15, 83, 143, 14);
		modifierEmpreinte.add(lblNouveauMdp);

		inputMdpModifier = new JTextField();
		inputMdpModifier.setColumns(10);
		inputMdpModifier.setBounds(173, 80, 320, 20);
		modifierEmpreinte.add(inputMdpModifier);
		if (getClient().getEstPermanent() == false)
			inputMdpModifier.setEnabled(false);
		
		/*
		 * Seuls les collaborateurs permanents peuvent modifier les empreintes
		 * et mdp
		 */
		if (this.estPermanent == true) {

			tabbedPane.addTab("Definir authentification", null, definirAuthentification, null);
			definirAuthentification.setLayout(null);

			listCollaborateurDefinirAuth = new JComboBox();
			listCollaborateurDefinirAuth.setBounds(10, 11, 321, 20);
			// Remplissage de la liste avec collaborateurs temporaires PUIS
			// permanents
			allCollabTemp = SAccu.getCollabTemps();
			if (this.estAccueil == true) {
				for (int i = 0; i < allCollabTemp.length; ++i) {
					collaborateur = "";
					collaborateur = allCollabTemp[i].id + "- " + " TEMP - " + allCollabTemp[i].prenom + " "
							+ allCollabTemp[i].nom;
					listCollaborateurDefinirAuth.addItem(collaborateur);
				}
			}

			if (this.estRH == true) {
				allCollabPerm = SRh.getCollabPerms();
				for (int i = 0; i < allCollabPerm.length; ++i) {
					collaborateur = "";
					collaborateur = allCollabPerm[i].id + "- " + "PERM - " + allCollabPerm[i].prenom + " "
							+ allCollabPerm[i].nom;
					listCollaborateurDefinirAuth.addItem(collaborateur);
				}
			}
			definirAuthentification.add(listCollaborateurDefinirAuth);

			inputEmpreinteDefinir = new JTextField();
			inputEmpreinteDefinir.setBounds(114, 39, 379, 20);
			definirAuthentification.add(inputEmpreinteDefinir);
			inputEmpreinteDefinir.setColumns(10);
			if (estAccueil == true)
				inputEmpreinteDefinir.setEditable(false);

			JLabel Empreinte = new JLabel("Empreinte");
			Empreinte.setBounds(33, 42, 81, 14);
			definirAuthentification.add(Empreinte);

			JLabel MotDePasseDefinir = new JLabel("Mot de passe");
			MotDePasseDefinir.setBounds(12, 67, 92, 14);
			definirAuthentification.add(MotDePasseDefinir);

			inputMotDePasseDefinir = new JTextField();
			inputMotDePasseDefinir.setColumns(10);
			inputMotDePasseDefinir.setBounds(114, 64, 379, 20);
			definirAuthentification.add(inputMotDePasseDefinir);
			if (estAccueil == true) inputMotDePasseDefinir.setEditable(false);
			
			// Gestion des inputs de l'onglet DEFINIR
			inputEmpreinteDefinir.setEnabled(false);
			inputMotDePasseDefinir.setEnabled(false);

			JButton btnDfinirAuthentification = new JButton("Definir");
			btnDfinirAuthentification.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					short idToDelete = getIdFromListBox(listCollaborateurDefinirAuth);
					// On retrouve l'objet Collaborateur
					Collab collab = new Collab();
					try {
						collab = SAccu.getCollabTemp(idToDelete);
					} catch (NonExiste e1) {
						try {
							collab = SRh.getCollabPerm(idToDelete);
						} catch (NonExiste e2) {
							JOptionPane.showMessageDialog(null, "Le collaborateur n'existe pas",
									"InfoBox: " + "ERREUR - Collaborateur", JOptionPane.INFORMATION_MESSAGE);
							System.out.println("--- [ERROR] Error collab doesn't exist : " + e2.raison);

						}
					}

					// Enregistrement des donnees
					try {
						SAuth.definirAuthentification(collab, inputEmpreinteDefinir.getText().toString(),
								inputMotDePasseDefinir.getText().toString());
						JOptionPane.showMessageDialog(null,
								"L'empreinte et le mot de passe de " + collab.prenom + " " + collab.nom + " "
										+ "ont bien �t� d�finis",
								"InfoBox: " + "VALIDATION - Authentification", JOptionPane.INFORMATION_MESSAGE);
					} catch (NonExiste | Existe e1) {
						System.out.println("--- [ERROR] Error defining auth : " + e1.getMessage());
					}
				}
			});
			btnDfinirAuthentification.setBounds(10, 92, 200, 23);
			definirAuthentification.add(btnDfinirAuthentification);
			btnDfinirAuthentification.setEnabled(false);

			JButton btnSupprimerEmp = new JButton("Supprimer empreinte");
			btnSupprimerEmp.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					short idToDelete = getIdFromListBox(listCollaborateurDefinirAuth);
					try {
						Collab collabEmpToDelete = SAccu.getCollabTemp(idToDelete);
						SAuth.supprimerEmpreinte(collabEmpToDelete);
						JOptionPane.showMessageDialog(null,
								"L'empreinte de " + collabEmpToDelete.prenom + " " + collabEmpToDelete.nom + " "
										+ "a ete supprimee",
								"InfoBox: " + "VALIDATION - Suppression d'empreinte", JOptionPane.INFORMATION_MESSAGE);
					} catch (NonExiste e1) {
						System.out.println("--- [ERROR] Error deleting empreinte : " + e1.getMessage());
					}
				}
			});
			btnSupprimerEmp.setBounds(291, 92, 200, 23);
			definirAuthentification.add(btnSupprimerEmp);
			if( estRH == true ) btnSupprimerEmp.setEnabled(false);
			
			btnAfficher.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					short idToShow = getIdFromListBox(listCollaborateurDefinirAuth);
					if(estPermSelected(listCollaborateurDefinirAuth) == true) {
						try { 
							Collab collabEmpToShow = SRh.getCollabPerm(idToShow);
							Authentification auth;
							auth = SAuth.getAuthentification(collabEmpToShow);
							if (auth.empreinte != "" && auth.mdp != "") {
								inputEmpreinteDefinir.setText(auth.empreinte);
								inputMotDePasseDefinir.setText(auth.mdp);
								btnDfinirAuthentification.setEnabled(false);
							} else {
								if (estAccueil == true)
									btnDfinirAuthentification.setEnabled(false);
								else
									btnDfinirAuthentification.setEnabled(true);
							}
						} catch (NonExiste e1) {
							System.out.println("--- [ERROR] Error getting auth : " + e1.getMessage());
						}
					} else {
						try {
							Collab collabEmpToShow = SAccu.getCollabTemp(idToShow);
							Authentification auth;
							auth = SAuth.getAuthentification(collabEmpToShow);
							inputEmpreinteDefinir.setText(auth.empreinte);
							inputMotDePasseDefinir.setText(auth.mdp);
							/* Activation des inputs */
							inputEmpreinteDefinir.setEnabled(true);
							inputMotDePasseDefinir.setEnabled(true);
							inputEmpreinteDefinir.setEditable(true);
							inputMotDePasseDefinir.setEditable(true);
							/* Gestion si l'empreinte est vide ou non */
							if (auth.empreinte.isEmpty()) {
								inputEmpreinteDefinir.setEnabled(false);
								inputEmpreinteDefinir.setEditable(false);
								btnSupprimerEmp.setEnabled(false);
							} else {
								btnSupprimerEmp.setEnabled(true);
							}
							if (auth.mdp.isEmpty()) {
								inputMotDePasseDefinir.setEnabled(false);
								inputMotDePasseDefinir.setEditable(false);
							}
						} catch (NonExiste e1) {
							System.out.println("--- [ERROR] Error getting auth : " + e1.getMessage());
						}
					}
				}
			});
			btnAfficher.setBounds(346, 10, 147, 23);
			definirAuthentification.add(btnAfficher);
			
			btnModifierMdp.setBounds(10, 108, 483, 23);
			modifierEmpreinte.add(btnModifierMdp);
			if (getClient().getEstPermanent() == false)
				btnModifierMdp.setEnabled(false);

		}
	}

	private void init()
	{
		this.setTitle("Service Authentification");
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setPreferredSize(new Dimension(560, 300));
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);

		// Initialisation a l'ouverture de la fenetre et recuperation du service
		// associe
		SAuth = getClient().getServiceAuthentification();
		SAccu = getClient().getServiceAccueil();
		SRh = getClient().getServiceRH();
		SH = getClient().getServiceHabilitation();
		
		// Recuperation de l'ID du collaborateur
		
		try {
			// V�rification de si Collaborateur permanent ou non (afin de griser le
			// dernier onglet)
			this.estPermanent = estPermanent(idCollaborateur);
			// V�rification si collaborateur est Accueil
			this.estAccueil = estAccueil(idCollaborateur);
			this.estRH = estRH(idCollaborateur);

		} catch (NonExiste e4) {
			System.out.println("--- [ERROR] Error not exists : " + e4.raison);
		}


		WListener wlist = new WListener(this);
		this.addWindowListener(wlist);

		getContentPane().setLayout(null);
	
	}
}
