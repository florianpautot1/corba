package accessControl.ihm;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.WindowConstants;

import accessControl.Collab;
import accessControl.NonExiste;
import accessControl.ServiceAcceuil;
import accessControl.ServiceAutorisation;
import accessControl.ServiceHabilitation;
import accessControl.ServiceRH;
import accessControl.clients.Client;
import accessControl.ihm.Ihm.WListener;

public class IHMMenu extends Ihm {
	
	private ServiceHabilitation SH;
	private ServiceRH SRh;
	private ServiceAcceuil SAccu;
	private ServiceAutorisation SAuto;
	
	private JButton btnGererAutorisations = new JButton("G\u00E9rer autorisations");
	private JButton btnGererCollabTemp = new JButton("G\u00E9rer collaborateurs temp.");
	private JButton btnGererHabilitations = new JButton("G\u00E9rer habilitations");
	private JButton btnGererCollabPerm = new JButton("G\u00E9rer collaborateurs perm.");
	private JButton btnGererAuthentifications = new JButton("G\u00E9rer authentifications");
	private JButton btnVoirLesPassages = new JButton("Voir les passages");

	private JLabel lblBonjour = new JLabel("Bonjour,");
	private JLabel lblPrenomCollab = new JLabel("Pr\u00E9nomCollab");
	private JLabel lblNomCollab = new JLabel("NomCollab");

	// Gestion du collaborateur
	private Collab collab = new Collab();
	private short tabHabilitations[];
	private short idCol;
	private short idZoneResp;

	private static IHMMenu frame;

	public static void main(String args[]){ 
		frame.setTitle("Menu principal");
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.setPreferredSize(new Dimension(650, 400));
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	public IHMMenu(short idCollab) {

		idCol = idCollab;

		
		init();
		initComponents();
		addComponentsHandlers();
		idZoneResp = SAuto.getResp(idCol);

		this.setVisible(true);
	}
	
	private void init()
	{
		this.setTitle("Menu principal");
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.setPreferredSize(new Dimension(450, 220));
		this.pack();
		this.setLocationRelativeTo(null);
		
		// Initialisation � l'ouverture de la fen�tre et r�cup�ration du service associ�
		SH = getClient().getServiceHabilitation();
		SRh = getClient().getServiceRH();
		SAccu = getClient().getServiceAccueil();
		SAuto = getClient().getServiceAutorisation();
		
		WListener wlist = new WListener(this);
		this.addWindowListener(wlist);
		getContentPane().setLayout(null);
	}
	
	private void initComponents()
	{
		btnGererAutorisations.setBounds(10, 70, 200, 23);
		getContentPane().add(btnGererAutorisations);
		
		
		btnGererCollabTemp.setBounds(220, 70, 200, 23);
		getContentPane().add(btnGererCollabTemp);
		
		
		btnGererHabilitations.setBounds(10, 100, 200, 23);
		getContentPane().add(btnGererHabilitations);		
		
		
		btnGererCollabPerm.setBounds(220, 100, 200, 23);
		getContentPane().add(btnGererCollabPerm);


		btnGererAuthentifications.setBounds(10, 130, 200, 23);
		getContentPane().add(btnGererAuthentifications);
		
		/*
		JButton btnPasserPorte = new JButton("Passer une porte");
		btnPasserPorte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IHMservicePorte frame = new IHMservicePorte();
				frame.setVisible(true);
			}
		});
		btnPasserPorte.setBounds(220, 130, 200, 23);
		getContentPane().add(btnPasserPorte);
		*/
		
		
		lblBonjour.setBounds(10, 10, 50, 14);
		getContentPane().add(lblBonjour);
		
		lblPrenomCollab.setBounds(62, 10, 60, 14);
		getContentPane().add(lblPrenomCollab);
		
		lblNomCollab.setBounds(119, 10, 60, 14);
		getContentPane().add(lblNomCollab);


		
		// Affichage du collaborateur
		lblNomCollab.setText(Client.getCollab().nom);
		lblPrenomCollab.setText(Client.getCollab().prenom);
		
		// Grisage / degrisage des boutons en fonction
		btnGererCollabPerm.setEnabled(false);
		btnGererCollabTemp.setEnabled(false);
		btnGererAutorisations.setEnabled(false);
		btnGererHabilitations.setEnabled(false);
		btnGererAuthentifications.setEnabled(true);
		
		
		btnVoirLesPassages.setBounds(220, 130, 200, 23);
		getContentPane().add(btnVoirLesPassages);
		btnVoirLesPassages.setEnabled(false);
		/* btnPasserPorte.setEnabled(true); */
				
		if (Client.getEstPermanent() == true) {
			try {
				collab = SRh.getCollabPerm(idCol);
				
			} 
			catch (NonExiste e1) { 
				System.out.println("--- [ERROR] Error getting collab perm : " + e1.raison);

			}
		} else {
			try {
				collab = SAccu.getCollabTemp(idCol);
				
			} 
			catch (NonExiste e1) { 
				System.out.println("--- [ERROR] Error getting collab temp : " + e1.raison);

			}
		}
			
		// Recuperation de ses habilitations
		try {
			tabHabilitations = SH.getHabilitations(collab);
			for(int i = 0; i < tabHabilitations.length; i++){
				if (tabHabilitations[i] == 0) btnGererCollabPerm.setEnabled(true);
				if (tabHabilitations[i] == 1) {
					btnGererAutorisations.setEnabled(true);
					btnGererHabilitations.setEnabled(true);
					btnVoirLesPassages.setEnabled(true);
				}
				if (tabHabilitations[i] == 2) btnGererCollabTemp.setEnabled(true);
				if (tabHabilitations[i] == 3) btnGererAuthentifications.setEnabled(true);
			}	
		} catch (NonExiste e1) { 
			System.out.println("--- [ERROR] Error getting hab : " + e1.raison);

		}
	}
	
	private void addComponentsHandlers()
	{
		btnGererAutorisations.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IHMAutorisation frame = new IHMAutorisation(idZoneResp);
				frame.setVisible(true);
			}
		});
		
		btnGererCollabTemp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IHMAccueil frame = new IHMAccueil();
				//frame.setVisible(true);
			}
		});
		
		btnGererHabilitations.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IHMHabilitation frame = new IHMHabilitation();
				frame.setVisible(true);
			}
		});
		
		btnGererCollabPerm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IHMRH frame = new IHMRH();
				frame.setVisible(true);
			}
		});
		
		btnGererAuthentifications.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IHMAuthentification frame = new IHMAuthentification(getClient().getCollab());
				frame.setVisible(true);
			}
		});
		
		btnVoirLesPassages.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IHMPassage frame = new IHMPassage(idZoneResp);
				frame.setVisible(true);
			}
		});
	}
}
