package accessControl.ihm;

import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import accessControl.clients.Client;
import accessControl.ihm.Ihm.WListener;

public class Ihm extends JFrame{
	
	private static Client client = new Client();
	
	
	public static Client getClient() {
		return client;
	}
	
	class WListener extends JFrame implements WindowListener, WindowFocusListener, WindowStateListener {

		private Ihm ihm;
		
		public WListener(Ihm i) {
			ihm = i;
		}
		
		public void windowOpened(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		public void windowClosing(WindowEvent e) {
			Ihm i = (Ihm) e.getSource();
			if(i instanceof IHMMenu || i instanceof IHMConnexion || i instanceof IHMLecteurEmpreinte)
			{
				client.disconnect();
				System.exit(0);
			}

		}

		public void windowClosed(WindowEvent e) {
		}

		public void windowIconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		public void windowDeiconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		public void windowActivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		public void windowDeactivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		public void windowStateChanged(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		public void windowGainedFocus(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		public void windowLostFocus(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}
	}
}
