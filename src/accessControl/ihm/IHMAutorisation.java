package accessControl.ihm;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Frame;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import accessControl.AutorisationPerm;
import accessControl.AutorisationTemp;
import accessControl.Collab;
import accessControl.Existe;
import accessControl.Habilitation;
import accessControl.NonExiste;
import accessControl.ServiceAcceuil;
import accessControl.ServiceAutorisation;
import accessControl.ServiceHabilitation;
import accessControl.ServiceRH;
import accessControl.Zone;
import accessControl.clients.Client;
import accessControl.ihm.Ihm.WListener;
import accessControl.metier.CollabPerm;
import accessControl.metier.CollabTemp;
import accessControl.model.ModeleDynamiqueAutorisationPerm;
import accessControl.model.ModeleDynamiqueAutorisationTemp;

import javax.swing.JTable;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;

import javax.print.attribute.standard.JobOriginatingUserName;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import com.sun.xml.internal.ws.util.StringUtils;

import javax.swing.event.ChangeEvent;
import javax.swing.border.EtchedBorder;
import java.awt.Color;
import java.awt.Component;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

public class IHMAutorisation extends Ihm {

	private short idZone = 0;

	private JFrame modalFrame;

	private JTable TempTable;
	private JTable PermTable;

	private JLabel lblJourDeb = new JLabel("Jour debut");
	private JLabel lblJourFin = new JLabel("Jour fin");
	private JLabel lblHeureDbut = new JLabel("Heure début");
	private JLabel labelHfin = new JLabel("Heure fin");
	private JLabel lblCollaborateur = new JLabel("Collaborateur");
	private JLabel lblZone = new JLabel("Zone");
	private JLabel label = new JLabel("Collaborateur");
	private JLabel label_1 = new JLabel("Zone");
	private JLabel labelModifHDeb = new JLabel("Heure début");
	private JLabel labelModifHFin = new JLabel("Heure fin");
	private JLabel labelModifJFin = new JLabel("Jour fin");
	private JLabel labelModifJDeb = new JLabel("Jour debut");
	private JLabel labelIDAutorisation = new JLabel("");
	private JLabel lblAutorisation = new JLabel("Autorisation :");
	
	private JPanel contentPane;
	private JPanel autoPermPanel = new JPanel();
	private JPanel PermPanel = new JPanel();
	private JPanel autoTempPanel = new JPanel();
	private JPanel TempPanel = new JPanel();
	private JPanel ajouterAutPanel = new JPanel();
	private JPanel collabPanel = new JPanel();
	private JPanel panelComboTemp = new JPanel();
	private JPanel panelComboPerm = new JPanel();
	private JPanel jTempPanel = new JPanel();
	private JPanel panelChoixAut = new JPanel();
	private JPanel horairePanel = new JPanel();
	private JPanel donnerRespPanel = new JPanel();
	private JPanel supprRespPanel = new JPanel();
	private JPanel modifPanel = new JPanel();
	private JPanel modifHorairesPanel = new JPanel();
	private JPanel modifJourPanel = new JPanel();

	private JButton btnUpdateAutPerm = new JButton("Modifier");
	private JButton btnSupprimerPerm = new JButton("Supprimer");
	private JButton btnSupprimerTemp = new JButton("Supprimer");
	private JButton btnUpdateAutTemp = new JButton("Modifier");
	private JButton btnAjouterAut = new JButton("Ajouter");
	private JButton btnAjouterResp = new JButton("Ajouter");
	private JButton btnSupprimerResp = new JButton("Supprimer");
	private JButton btnValider = new JButton("Valider");

	private JComboBox comboCollabTemp;
	private JComboBox comboCollabPerm;
	private JComboBox hFinCombo;
	private JComboBox hDebConbo;
	private JComboBox comboCollabSupprResp;
	private JComboBox comboCollabResp;
	private JComboBox comboZonesResp;
	private JComboBox comboZoneSupprResp;
	private JComboBox comboModifHFin;
	private JComboBox comboModifHdeb;

	private JRadioButton rdbtnCollabTemp = new JRadioButton("Collaborateur Temp");
	private JRadioButton rdbtnCollabPerm = new JRadioButton("Collaborateur Perm");
	private JRadioButton rdbtnTemporaire = new JRadioButton("Autorisation temporaire");
	private JRadioButton rdbtnPermanente = new JRadioButton("Autorisation permanente");

	private ServiceAutorisation SAuto;
	private ServiceAcceuil SAccu;
	private ServiceRH SRh;
	private ServiceHabilitation Sh;

	private ModeleDynamiqueAutorisationPerm modelePerm = new ModeleDynamiqueAutorisationPerm();
	private ModeleDynamiqueAutorisationTemp modeleTemp = new ModeleDynamiqueAutorisationTemp();

	private AutorisationTemp[] allAutorisationsPerm[];
	private AutorisationPerm[] allAutorisationsTemp[];
	private AutorisationPerm autPerm;
	private AutorisationTemp autTemp;
	private Collab[] lesCollabsPerms;
	private Collab[] lesCollabsTemps;
	private Zone[] lesZones;

	private UtilDateModel modelDeb = new UtilDateModel();
	private UtilDateModel modelFin = new UtilDateModel();
	private UtilDateModel modelModifDeb = new UtilDateModel();
	private UtilDateModel modelModifFin = new UtilDateModel();

	private Properties prop = new Properties();
	
	private JDatePanelImpl datePanelDeb = new JDatePanelImpl(modelDeb, prop);
	private JDatePanelImpl datePanelFin = new JDatePanelImpl(modelFin, prop);
	private JDatePanelImpl datePanelModifDeb = new JDatePanelImpl(modelModifDeb, prop);
	private JDatePanelImpl datePanelModifFin = new JDatePanelImpl(modelModifFin, prop);

	private JDatePickerImpl datePickerDeb = new JDatePickerImpl(datePanelDeb, new DateLabelFormatter());
	private JDatePickerImpl datePickerFin = new JDatePickerImpl(datePanelFin, new DateLabelFormatter());
	private JDatePickerImpl datePickerModifDeb = new JDatePickerImpl(datePanelModifDeb, new DateLabelFormatter());
	private JDatePickerImpl datePickerModifFin = new JDatePickerImpl(datePanelModifFin, new DateLabelFormatter());

	private Calendar calendarDeb = Calendar.getInstance();
	private Calendar endDeb = Calendar.getInstance();
	private Calendar calendarFin = Calendar.getInstance();
	private Calendar endFin = Calendar.getInstance();
	private Calendar calendarModifDeb = Calendar.getInstance();
	private Calendar endModifDeb = Calendar.getInstance();
	private Calendar calendarModifFin = Calendar.getInstance();
	private Calendar endModifFin = Calendar.getInstance();

	private DefaultComboBoxModel<Date> hourModelModifFin = new DefaultComboBoxModel<>();
	private DefaultComboBoxModel<Date> hourModelDeb = new DefaultComboBoxModel<>();
	private DefaultComboBoxModel<Date> hourModelFin = new DefaultComboBoxModel<>();
	private DefaultComboBoxModel<Date> hourModelFinDeb = new DefaultComboBoxModel<>();

	private JScrollPane tableSCTemp;
	private JScrollPane tableSCPerm;

	/**
	 * Create the frame.
	 */
	public IHMAutorisation(short idZone) {
		this.idZone = idZone;
		init();
		initComponents();
		addComponentsHandlers();
		fillTables();
		jTempPanel.setVisible(false);

	}

	private void supprimerAutorisationTemp(ActionEvent e) {
		modeleTemp.removeAutorisationTemp(TempTable.getSelectedRow());
	}

	private void supprimerAutorisationPerm(ActionEvent e) {
		modelePerm.removeAutorisationPerm(PermTable.getSelectedRow());
	}

	private boolean checkDates(Date dDeb, Date dFin) {
		boolean res = false;
		if (dDeb.before(dFin))
			res = true;
		return res;
	}

	private void init() {
		setTitle("Service Autorisation");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1053, 751);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		// Initialisation a l'ouverture de la fenetre et recuperation du service
		// associe
		SAuto = getClient().getServiceAutorisation();
		SAccu = getClient().getServiceAccueil();
		SRh = getClient().getServiceRH();
		Sh = getClient().getServiceHabilitation();

		lesCollabsPerms = SRh.getCollabPerms();
		lesCollabsTemps = SAccu.getCollabTemps();
		lesZones = SAuto.getZones();

		WListener wlist = new WListener(this);
		this.addWindowListener(wlist);

	}

	private void initComponents() {
		autoPermPanel.setBorder(new TitledBorder(null, "Autorisations permanentes", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		autoPermPanel.setBounds(6, 6, 478, 219);
		contentPane.add(autoPermPanel);
		autoPermPanel.setLayout(null);

		PermPanel.setBounds(6, 21, 466, 139);
		autoPermPanel.add(PermPanel);

		btnUpdateAutPerm.setBounds(114, 184, 260, 29);
		autoPermPanel.add(btnUpdateAutPerm);

		btnSupprimerPerm.setBounds(114, 161, 260, 29);
		autoPermPanel.add(btnSupprimerPerm);
		btnSupprimerPerm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				supprimerAutorisationPerm(e);
			}
		});

		autoTempPanel.setBorder(
				new TitledBorder(null, "Autorisation temporaires", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		autoTempPanel.setBounds(511, 6, 536, 219);
		contentPane.add(autoTempPanel);
		autoTempPanel.setLayout(null);

		TempPanel.setBounds(6, 16, 524, 145);
		autoTempPanel.add(TempPanel);

		btnSupprimerTemp.setBounds(143, 162, 266, 29);
		autoTempPanel.add(btnSupprimerTemp);

		btnUpdateAutTemp.setBounds(143, 184, 266, 29);
		autoTempPanel.add(btnUpdateAutTemp);

		btnSupprimerTemp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				supprimerAutorisationTemp(e);
			}
		});

		ajouterAutPanel.setBorder(
				new TitledBorder(null, "Ajouter autorisation", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		ajouterAutPanel.setBounds(6, 227, 609, 302);
		contentPane.add(ajouterAutPanel);
		ajouterAutPanel.setLayout(null);

		collabPanel
				.setBorder(new TitledBorder(null, "Choix collab", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		collabPanel.setBounds(6, 74, 393, 100);
		ajouterAutPanel.add(collabPanel);
		collabPanel.setLayout(null);

		panelComboTemp.setBounds(9, 19, 343, 27);
		collabPanel.add(panelComboTemp);
		panelComboTemp.setLayout(null);

		comboCollabTemp = new JComboBox(lesCollabsTemps);
		comboCollabTemp.setBounds(157, 0, 186, 27);
		panelComboTemp.add(comboCollabTemp);

		rdbtnCollabTemp.setBounds(0, 0, 186, 23);
		panelComboTemp.add(rdbtnCollabTemp);

		comboCollabTemp.setVisible(false);

		panelComboPerm.setBounds(9, 57, 343, 28);
		collabPanel.add(panelComboPerm);
		panelComboPerm.setLayout(null);

		rdbtnCollabPerm.setBounds(0, 0, 152, 23);
		panelComboPerm.add(rdbtnCollabPerm);

		rdbtnCollabPerm.setSelected(true);

		comboCollabPerm = new JComboBox(lesCollabsPerms);
		comboCollabPerm.setBounds(157, 1, 186, 27);
		panelComboPerm.add(comboCollabPerm);

		jTempPanel
				.setBorder(new TitledBorder(null, "Dates limites", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		jTempPanel.setBounds(6, 178, 393, 77);
		ajouterAutPanel.add(jTempPanel);
		jTempPanel.setLayout(null);

		prop.put("text.today", "Ajourd'hui");
		prop.put("text.month", "Mois");
		prop.put("text.year", "Annee");

		datePickerDeb = new JDatePickerImpl(datePanelDeb, new DateLabelFormatter());
		datePickerDeb.setBounds(110, 13, 158, 28);
		jTempPanel.add(datePickerDeb);

		datePickerFin.setBounds(110, 43, 158, 28);
		jTempPanel.add(datePickerFin);

		lblJourDeb.setBounds(17, 19, 81, 16);
		jTempPanel.add(lblJourDeb);

		lblJourFin.setBounds(17, 49, 61, 16);
		jTempPanel.add(lblJourFin);

		panelChoixAut.setBorder(
				new TitledBorder(null, "Type autorisation", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelChoixAut.setBounds(10, 29, 393, 45);
		ajouterAutPanel.add(panelChoixAut);
		panelChoixAut.setLayout(null);

		rdbtnTemporaire.setBounds(6, 16, 190, 23);
		panelChoixAut.add(rdbtnTemporaire);
		
		rdbtnPermanente.setSelected(true);
		rdbtnPermanente.setBounds(197, 16, 190, 23);
		panelChoixAut.add(rdbtnPermanente);
		
		btnAjouterAut.setBounds(6, 259, 589, 37);
		ajouterAutPanel.add(btnAjouterAut);

		horairePanel.setBorder(new TitledBorder(null, "Horaires", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		horairePanel.setBounds(415, 22, 180, 236);
		ajouterAutPanel.add(horairePanel);
		horairePanel.setLayout(null);

		lblHeureDbut.setBounds(8, 37, 77, 16);
		horairePanel.add(lblHeureDbut);

		calendarDeb.set(Calendar.HOUR_OF_DAY, 0);
		calendarDeb.set(Calendar.MINUTE, 0);
		calendarDeb.set(Calendar.SECOND, 0);

		endDeb.set(Calendar.HOUR_OF_DAY, 23);
		endDeb.set(Calendar.MINUTE, 59);
		endDeb.set(Calendar.SECOND, 0);

		calendarFin.set(Calendar.HOUR_OF_DAY, 0);
		calendarFin.set(Calendar.MINUTE, 0);
		calendarFin.set(Calendar.SECOND, 0);

		endFin.set(Calendar.HOUR_OF_DAY, 23);
		endFin.set(Calendar.MINUTE, 59);
		endFin.set(Calendar.SECOND, 0);
		
		do {
			hourModelDeb.addElement(calendarDeb.getTime());
			calendarDeb.add(Calendar.MINUTE, 15);
		} while (calendarDeb.getTime().before(endDeb.getTime()));

		do {
			hourModelFin.addElement(calendarFin.getTime());
			calendarFin.add(Calendar.MINUTE, 15);
		} while (calendarFin.getTime().before(endFin.getTime()));

		hFinCombo = new JComboBox(hourModelFin);
		hFinCombo.setBounds(16, 142, 84, 27);
		horairePanel.add(hFinCombo);
		hFinCombo.setRenderer(new DateFormattedListCellRenderer(new SimpleDateFormat("HH:mm")));

		hDebConbo = new JComboBox(hourModelDeb);
		hDebConbo.setBounds(18, 56, 84, 27);
		horairePanel.add(hDebConbo);
		hDebConbo.setRenderer(new DateFormattedListCellRenderer(new SimpleDateFormat("HH:mm")));

		labelHfin.setBounds(8, 119, 77, 16);
		horairePanel.add(labelHfin);

		donnerRespPanel.setBorder(new TitledBorder(null, "Donner responsabilit\u00E9", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		donnerRespPanel.setBounds(627, 227, 188, 270);
		contentPane.add(donnerRespPanel);
		donnerRespPanel.setLayout(null);

		comboCollabResp = new JComboBox(SRh.getCollabPerms());
		comboCollabResp.setBounds(18, 93, 152, 27);
		donnerRespPanel.add(comboCollabResp);

		comboZonesResp = new JComboBox(lesZones);
		comboZonesResp.setBounds(18, 169, 152, 27);
		donnerRespPanel.add(comboZonesResp);

		
		btnAjouterResp.setBounds(30, 208, 132, 41);
		donnerRespPanel.add(btnAjouterResp);

		lblCollaborateur.setBounds(18, 72, 107, 16);
		donnerRespPanel.add(lblCollaborateur);

		lblZone.setBounds(18, 148, 61, 16);
		donnerRespPanel.add(lblZone);

		supprRespPanel.setLayout(null);
		supprRespPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null),
				"Supprimer Responsabilit\u00E9", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		supprRespPanel.setBounds(827, 227, 188, 270);
		contentPane.add(supprRespPanel);

		comboCollabSupprResp = new JComboBox(lesCollabsPerms);
		comboCollabSupprResp.setBounds(18, 91, 144, 27);
		supprRespPanel.add(comboCollabSupprResp);

		comboZoneSupprResp = new JComboBox(lesZones);
		comboZoneSupprResp.setBounds(18, 165, 144, 27);
		supprRespPanel.add(comboZoneSupprResp);
		
		btnSupprimerResp.setBounds(18, 205, 144, 41);
		supprRespPanel.add(btnSupprimerResp);

		label.setBounds(18, 70, 107, 16);
		supprRespPanel.add(label);

		label_1.setBounds(17, 144, 61, 16);
		supprRespPanel.add(label_1);
		
		modifPanel.setBorder(
				new TitledBorder(null, "Modifier autorisation", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		modifPanel.setBounds(6, 544, 609, 179);
		contentPane.add(modifPanel);
		modifPanel.setLayout(null);

		modifHorairesPanel.setLayout(null);
		modifHorairesPanel
				.setBorder(new TitledBorder(null, "Horaires", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		modifHorairesPanel.setBounds(6, 19, 207, 71);
		modifPanel.add(modifHorairesPanel);

		labelModifHDeb.setBounds(6, 20, 77, 16);
		modifHorairesPanel.add(labelModifHDeb);

		calendarModifDeb.set(Calendar.HOUR_OF_DAY, 0);
		calendarModifDeb.set(Calendar.MINUTE, 0);
		calendarModifDeb.set(Calendar.SECOND, 0);

		endModifDeb.set(Calendar.HOUR_OF_DAY, 23);
		endModifDeb.set(Calendar.MINUTE, 59);
		endModifDeb.set(Calendar.SECOND, 0);

		calendarModifFin.set(Calendar.HOUR_OF_DAY, 0);
		calendarModifFin.set(Calendar.MINUTE, 0);
		calendarModifFin.set(Calendar.SECOND, 0);

		endModifFin.set(Calendar.HOUR_OF_DAY, 23);
		endModifFin.set(Calendar.MINUTE, 59);
		endModifFin.set(Calendar.SECOND, 0);

		do {
			hourModelModifFin.addElement(calendarModifFin.getTime());
			calendarModifFin.add(Calendar.MINUTE, 15);
		} while (calendarModifFin.getTime().before(endModifFin.getTime()));

		do {
			hourModelFinDeb.addElement(calendarModifDeb.getTime());
			calendarModifDeb.add(Calendar.MINUTE, 15);
		} while (calendarModifDeb.getTime().before(endModifDeb.getTime()));

		comboModifHFin = new JComboBox(hourModelModifFin);
		comboModifHFin.setBounds(100, 39, 89, 27);
		modifHorairesPanel.add(comboModifHFin);
		comboModifHFin.setRenderer(new DateFormattedListCellRenderer(new SimpleDateFormat("HH:mm")));

		comboModifHdeb = new JComboBox(hourModelFinDeb);
		comboModifHdeb.setBounds(6, 39, 89, 27);
		modifHorairesPanel.add(comboModifHdeb);
		comboModifHdeb.setRenderer(new DateFormattedListCellRenderer(new SimpleDateFormat("HH:mm")));

		labelModifHFin.setBounds(106, 20, 77, 16);
		modifHorairesPanel.add(labelModifHFin);

		modifJourPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Jour",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		modifJourPanel.setLayout(null);
		modifJourPanel.setBounds(225, 19, 331, 71);
		modifPanel.add(modifJourPanel);

		datePickerModifDeb.setBounds(144, 13, 158, 28);
		modifJourPanel.add(datePickerModifDeb);

		datePickerModifFin.setBounds(144, 37, 158, 28);
		modifJourPanel.add(datePickerModifFin);

		labelModifJDeb.setBounds(51, 19, 81, 16);
		modifJourPanel.add(labelModifJDeb);

		labelModifJFin.setBounds(51, 43, 61, 16);
		modifJourPanel.add(labelModifJFin);

		modifJourPanel.setVisible(false);

		
		btnValider.setBounds(132, 102, 165, 64);
		modifPanel.add(btnValider);

		lblAutorisation.setBounds(16, 102, 104, 16);
		modifPanel.add(lblAutorisation);

		labelIDAutorisation.setBounds(26, 125, 94, 16);
		modifPanel.add(labelIDAutorisation);
	}

	private void addComponentsHandlers() {

		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(modifJourPanel.isVisible() && TempTable.getSelectedRow() != -1)
				{
					SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.FRENCH);
					SimpleDateFormat formatDay = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);

					Date hDeb = (Date) comboModifHdeb.getSelectedItem();
					Date hFin = (Date) comboModifHFin.getSelectedItem();
					

					
					autTemp.hDeb = format.format(hDeb);
					autTemp.hFin = format.format(hFin);
					autTemp.jourDeb = datePickerModifDeb.getJFormattedTextField().getText();
					autTemp.jourFin = datePickerModifFin.getJFormattedTextField().getText();
					
					Date jDeb = null;
					Date jFin = null;
					try {
						jDeb = formatDay.parse(autTemp.jourDeb);
						jFin = formatDay.parse(autTemp.jourFin);

					} catch (ParseException e1) {
						System.out.println("--- [ERROR] Error parsing dates");
					}
					
					if(!checkDates(hDeb, hFin) && !checkDates(jDeb, jFin))
					{
						System.out.println("--- [ERROR] Dates incorrect");
						return;
					}
					
					modeleTemp.modifyAutorisationTemp(TempTable.getSelectedRow(), autTemp);
				}
				else if(PermTable.getSelectedRow() != -1)
				{
					SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.FRENCH);

					Date hDeb = (Date) comboModifHdeb.getSelectedItem();
					Date hFin = (Date) comboModifHFin.getSelectedItem();
					
					autPerm.hDeb = format.format(hDeb);
					autPerm.hFin = format.format(hFin);
					modelePerm.modifyAutorisationPerm(PermTable.getSelectedRow(), autPerm);
				}
			}
		});

		rdbtnCollabPerm.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (rdbtnCollabPerm.isSelected()) {
					rdbtnCollabTemp.setSelected(false);
					comboCollabTemp.setVisible(false);
					comboCollabPerm.setVisible(true);
				} else if (!rdbtnCollabPerm.isSelected()) {
					rdbtnCollabTemp.setSelected(false);
					comboCollabTemp.setVisible(false);
					comboCollabPerm.setVisible(false);
				}
			}
		});

		rdbtnCollabPerm.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (rdbtnCollabPerm.isSelected()) {
					rdbtnCollabTemp.setSelected(false);
					comboCollabTemp.setVisible(false);
					comboCollabPerm.setVisible(true);

				} else if (!rdbtnCollabPerm.isSelected()) {
					rdbtnCollabTemp.setSelected(false);
					comboCollabTemp.setVisible(false);
					comboCollabPerm.setVisible(false);
				}
			}
		});

		rdbtnPermanente.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (rdbtnPermanente.isSelected()) {
					rdbtnTemporaire.setSelected(false);
					jTempPanel.setVisible(false);
					if (rdbtnCollabTemp.isSelected()) {
						rdbtnCollabTemp.setSelected(false);
						rdbtnCollabPerm.setSelected(true);
					}
				} else if (!rdbtnPermanente.isSelected() && !rdbtnTemporaire.isSelected()) {
					rdbtnPermanente.setSelected(false);
					jTempPanel.setVisible(false);

				}
			}
		});

		rdbtnTemporaire.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (rdbtnTemporaire.isSelected()) {
					rdbtnPermanente.setSelected(false);
					jTempPanel.setVisible(true);
				} else if (!rdbtnPermanente.isSelected() && !rdbtnTemporaire.isSelected()) {
					rdbtnTemporaire.setSelected(false);
					jTempPanel.setVisible(false);

				}
			}
		});
		
		rdbtnCollabTemp.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (rdbtnCollabTemp.isSelected()) {
					rdbtnCollabPerm.setSelected(false);
					comboCollabPerm.setVisible(false);
					comboCollabTemp.setVisible(true);
					jTempPanel.setVisible(true);
					rdbtnTemporaire.setSelected(true);
				}
				if (!rdbtnCollabTemp.isSelected()) {
					rdbtnCollabPerm.setSelected(false);
					comboCollabPerm.setVisible(false);
					comboCollabTemp.setVisible(false);
				}
			}
		});

		rdbtnCollabTemp.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (rdbtnCollabTemp.isSelected()) {
					rdbtnCollabPerm.setSelected(false);
					comboCollabPerm.setVisible(false);
					comboCollabTemp.setVisible(true);
					jTempPanel.setVisible(true);

				}
				if (!rdbtnCollabTemp.isSelected()) {
					rdbtnCollabPerm.setSelected(false);
					comboCollabPerm.setVisible(false);
					comboCollabTemp.setVisible(false);
				}
			}
		});
		
		btnAjouterResp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Collab c = (Collab) comboCollabResp.getSelectedItem();
				Zone z = (Zone) comboZonesResp.getSelectedItem();
				try {
					String fullID = c.id;
					String[] parts = fullID.split("-");
					short realID = Short.parseShort(parts[1]);
					SAuto.donnerRespPourZone(realID, z.id);
					Sh.ajouterHabilitation(c, Habilitation.RESP);
				} catch (Existe e1) {
					System.out.println("--- [ERREUR] Erreur de l'ajout de la responsabilité" + e1.raison);
				}
			}
		});

		btnSupprimerResp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Collab c = (Collab) comboCollabSupprResp.getSelectedItem();
				Zone z = (Zone) comboZoneSupprResp.getSelectedItem();
				try {
					String fullID = c.id;
					String[] parts = fullID.split("-");
					short realID = Short.parseShort(parts[1]);
					SAuto.supprimerResp(realID, z.id);
					Sh.supprimerHabilitation(c, Habilitation.RESP);
				} catch (NonExiste e1) {
					System.out.println("--- [ERREUR] Erreur de la suppression de la responsabilité : " + e1.raison);
				}
			}
		});
		
		btnAjouterAut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Date heureDateDeb = (Date) hDebConbo.getSelectedItem();
				Date heureDateFin = (Date) hFinCombo.getSelectedItem();
				SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.FRENCH);
				SimpleDateFormat formatDay = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);

				String hDeb = format.format(heureDateDeb);
				String hFin = format.format(heureDateFin);

				if (!checkDates(heureDateDeb, heureDateFin)) {
					System.out.println("--- [ERROR] Hours incorrect");
					return;
				}

				if (rdbtnCollabPerm.isSelected()) {
					Collab c = (Collab) comboCollabPerm.getSelectedItem();
					if (rdbtnPermanente.isSelected()) {
						AutorisationPerm ap = new AutorisationPerm("P-" + c.id + "-" + idZone, c.id, hDeb, hFin,
								idZone);
						modelePerm.addAutorisationPerm(ap);

					} else if (rdbtnTemporaire.isSelected()) {
						String jourDeb = datePickerDeb.getJFormattedTextField().getText();
						String jourFin = datePickerFin.getJFormattedTextField().getText();

						Date jDeb = null;
						Date jFin = null;
						try {
							jDeb = formatDay.parse(jourDeb);
							jFin = formatDay.parse(jourFin);
						} catch (ParseException e1) {
							System.out.println("--- [ERROR] Error parsing dates");
						}
						if (!checkDates(jDeb, jFin)) {
							System.out.println("--- [ERROR] Dates incorrect");
							return;
						}

						AutorisationTemp at = new AutorisationTemp("T-" + c.id + "-" + idZone, c.id, hDeb, hFin, idZone,
								jourDeb, jourFin);

						modeleTemp.addAutorisationTemp(at);

					} else {
						AutorisationPerm ap = new AutorisationPerm("P-" + c.id + "-" + idZone, c.id, hDeb, hFin,
								idZone);
						modelePerm.addAutorisationPerm(ap);

					}
				} else if (rdbtnCollabTemp.isSelected()) {
					Collab c = (Collab) comboCollabTemp.getSelectedItem();

					if (rdbtnTemporaire.isSelected()) {
						String jourDeb = datePickerDeb.getJFormattedTextField().getText();
						String jourFin = datePickerFin.getJFormattedTextField().getText();
						Date jDeb = null;
						Date jFin = null;
						try {
							jDeb = formatDay.parse(jourDeb);
							jFin = formatDay.parse(jourFin);
						} catch (ParseException e1) {
							System.out.println("--- [ERROR] Error parsing dates");
						}
						if (!checkDates(jDeb, jFin)) {
							System.out.println("--- [ERROR] Dates incorrect");
							return;
						}

						AutorisationTemp at = new AutorisationTemp("T-" + c.id + "-" + idZone, c.id, hDeb, hFin, idZone,
								jourDeb, jourFin);
						modeleTemp.addAutorisationTemp(at);

					}
				}
			}
		});
	
		btnUpdateAutPerm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (PermTable.getSelectedRow() != -1) {
					autPerm = new AutorisationPerm();
					autPerm.id = (String) modelePerm.getValueAt(PermTable.getSelectedRow(), 0);
					labelIDAutorisation.setText(autPerm.id);

					autPerm.idCollab = (String) modelePerm.getValueAt(PermTable.getSelectedRow(), 1);
					autPerm.hDeb = (String) modelePerm.getValueAt(PermTable.getSelectedRow(), 2);
					autPerm.hFin = (String) modelePerm.getValueAt(PermTable.getSelectedRow(), 3);
					autPerm.idZone = (short) modelePerm.getValueAt(PermTable.getSelectedRow(), 4);

					modifJourPanel.setVisible(false);
				}

			}
		});

		btnUpdateAutTemp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (TempTable.getSelectedRow() != -1) {
					autTemp = new AutorisationTemp();
					autTemp.id = (String) modeleTemp.getValueAt(TempTable.getSelectedRow(), 0);
					labelIDAutorisation.setText(autTemp.id);
					autTemp.idCollab = (String) modeleTemp.getValueAt(TempTable.getSelectedRow(), 1);
					autTemp.hDeb = (String) modeleTemp.getValueAt(TempTable.getSelectedRow(), 2);
					autTemp.hFin = (String) modeleTemp.getValueAt(TempTable.getSelectedRow(), 3);
					autTemp.idZone = (short) modeleTemp.getValueAt(TempTable.getSelectedRow(), 4);
					autTemp.jourDeb = (String) modeleTemp.getValueAt(TempTable.getSelectedRow(), 5);
					autTemp.jourFin = (String) modeleTemp.getValueAt(TempTable.getSelectedRow(), 6);

					modifJourPanel.setVisible(true);

					datePickerModifDeb.getJFormattedTextField().setText(autTemp.jourDeb);
					datePickerModifFin.getJFormattedTextField().setText(autTemp.jourFin);
				}

			}
		});
	}

	private void fillTables()
	{
		// Remplissage de la table
		TempTable = new JTable(modeleTemp);
		tableSCTemp = new JScrollPane(TempTable);
		tableSCTemp.setColumnHeaderView(TempTable.getTableHeader());
		TempTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		TempTable.setShowVerticalLines(false);
		TempPanel.add(tableSCTemp);

		modeleTemp.setAllAutorisations(SAuto.getAutorisationsTempsForZone(idZone));
		modelePerm.setAllAutorisations(SAuto.getAutorisationsPermsForZone(idZone));

		// Remplissage de la table
		PermTable = new JTable(modelePerm);
		tableSCPerm = new JScrollPane(PermTable);
		tableSCPerm.setColumnHeaderView(TempTable.getTableHeader());
		PermTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		PermTable.setShowVerticalLines(false);
		PermPanel.add(tableSCPerm);
	}
}

class DateFormattedListCellRenderer extends DefaultListCellRenderer {

	private String datePattern = "HH:mm";
	private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

	public DateFormattedListCellRenderer(SimpleDateFormat format) {
		this.dateFormatter = format;
	}

	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		if (value instanceof Date) {
			value = dateFormatter.format((Date) value);
		}
		return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}

}

class DateLabelFormatter extends AbstractFormatter {

	private String datePattern = "dd/MM/yyyy";
	private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

	@Override
	public Object stringToValue(String text) throws ParseException {
		return dateFormatter.parseObject(text);
	}

	@Override
	public String valueToString(Object value) throws ParseException {
		if (value != null) {
			Calendar cal = (Calendar) value;
			return dateFormatter.format(cal.getTime());
		}

		return "";
	}

}