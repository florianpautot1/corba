package accessControl.ihm;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.JButton;

import accessControl.Collab;
import accessControl.NonExiste;
import accessControl.ServiceAcceuil;
import accessControl.ServiceAuthentification;
import accessControl.ServiceRH;
import accessControl.clients.Client;
import accessControl.ihm.Ihm.WListener;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JComboBox;

public class IHMConnexion extends Ihm {
	private JLabel lblLogin = new JLabel("Collaborateur");
	private JLabel lblMdp = new JLabel("Mdp");
	
	private JTextField inputMdp;

	private JComboBox listAllCollab = new JComboBox();
	
	private JButton btnValider = new JButton("Connexion");

	private static IHMConnexion frame;
	
	public static void main(String[] args) {
		frame = new IHMConnexion();
		frame.setTitle("Connexion");
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.setPreferredSize(new Dimension(390, 185));
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	public IHMConnexion() {
		init();
		initComponents();
		addComponentsHandlers();
	}
	
	public void fillAllCollabConnect(JComboBox list) {
		
		Client cli = getClient();
		ServiceRH SRh = cli.getServiceRH();
		ServiceAcceuil SAccu = cli.getServiceAccueil();
		
		Collab allCollabTemp[];
		Collab allCollabPerm[];
		String collaborateur;
		
		allCollabTemp = SAccu.getCollabTemps();
		allCollabPerm = SRh.getCollabPerms();
		
		for (int i = 0; i < allCollabPerm.length; ++i){
	    	collaborateur = "";
	    	String id = getIdFromFullID(allCollabPerm[i].id);
	    	collaborateur = id + "- " + "PERM - " + allCollabPerm[i].prenom + " " +allCollabPerm[i].nom;
	    	list.addItem(collaborateur);
	    }
		
	    for (int i = 0; i < allCollabTemp.length; ++i){
	    	collaborateur = "";
	    	String id = getIdFromFullID(allCollabTemp[i].id);
	    	collaborateur = id + "- " + " TEMP - " + allCollabTemp[i].prenom + " " +allCollabTemp[i].nom;
	    	list.addItem(collaborateur);
	    }
	}
	
	public String getIdFromFullID(String id) {
	
		String[] parts = id.split("-");
		String part1 = parts[0]; 
		String part2 = parts[1];
		part1.trim();	
		return part2;
	}
	
	public String getIdFromListBox(JComboBox listCollab) {
		
		String chaineListBox = listCollab.getSelectedItem().toString();
		String[] parts = chaineListBox.split("-");
		String part1 = parts[0]; 
		String part2 = parts[1];
		String part3 = parts[2];
		part1.trim();	
		return part1;
	}
	
	public boolean estPermFromListBox(JComboBox listCollab) {
		boolean estPermanent = false;
		String chaineListBox = listCollab.getSelectedItem().toString();
		String[] parts = chaineListBox.split("-");
		String part1 = parts[0]; 
		String part2 = parts[1];
		String part3 = parts[2];
		part2.trim();
		
		if (part2.contains("PERM")) estPermanent = true;
		else estPermanent = false;
		
		return estPermanent;
	}

	public void init()
	{
		getContentPane().setLayout(null);
		
		WListener wlist = new WListener(this);
		this.addWindowListener(wlist);
	}
	
	public void initComponents()
	{
		lblLogin.setBounds(27, 43, 81, 14);
		getContentPane().add(lblLogin);
		
		listAllCollab.setBounds(118, 40, 220, 20);
		this.fillAllCollabConnect(listAllCollab);
		getContentPane().add(listAllCollab);
		
		lblMdp.setBounds(77, 71, 31, 14);
		getContentPane().add(lblMdp);
		
		inputMdp = new JTextField();
		inputMdp.setBounds(118, 68, 220, 20);
		getContentPane().add(inputMdp);
		inputMdp.setColumns(10);
		
		btnValider.setBounds(220, 97, 122, 23);
		getContentPane().add(btnValider);
	}
	
	public void addComponentsHandlers()
	{
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// Creation client et service
				ServiceAuthentification SAuth = Client.getServiceAuthentification();
				ServiceRH SRh = Client.getServiceRH();
				ServiceAcceuil SAccu = Client.getServiceAccueil();
				
				// Valeur de l'authentification
				boolean authOk = false;
				
				// Construction d'un collaborateur
				Collab collab = new Collab();
				collab.id = getIdFromListBox(listAllCollab);
				
				// Enregistrement dans client
				Client.setIdCollab(getIdFromListBox(listAllCollab));
				Client.setEstPermanent(estPermFromListBox(listAllCollab));
				
				// Verification et traitement
				if (Client.getEstPermanent() == true) {
					try { 
						Collab collabPerm = new Collab();
						collabPerm = SRh.getCollabPerm(Short.parseShort(collab.id));
						Client.setCollab(collabPerm);
						authOk = SAuth.verificationMdp(collabPerm, inputMdp.getText().toString());
					} 
					catch (NonExiste e1) { 
						System.out.println("--- [ERROR] Error checking mdp : " + e1.raison);
					}	
				} else { 
					try { 
						Collab collabTemp = new Collab();
						collabTemp = SAccu.getCollabTemp(Short.parseShort(collab.id));
						Client.setCollab(collabTemp);
						authOk = SAuth.verificationMdp(collabTemp, inputMdp.getText().toString());
					} 
					catch (NonExiste e1) { 
						System.out.println("--- [ERROR] Error checking mdp : " + e1.raison);

					} 
				}
				
				if (authOk == true) {
					frame.setVisible(false);				
					IHMMenu frameM = new IHMMenu(Short.parseShort(collab.id));
				}
				else { inputMdp.setText(""); }
			}
		});
	}
}
