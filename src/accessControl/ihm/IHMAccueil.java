package accessControl.ihm;

import javax.swing.JLabel;

import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;

import accessControl.AutorisationTemp;
import accessControl.Collab;
import accessControl.NonExiste;
import accessControl.ServiceAuthentification;
import accessControl.ServiceAutorisation;
import accessControl.clients.Client;
import accessControl.model.ModeleDynamiqueCollaborateurTemp;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IHMAccueil extends Ihm {
	private JTable lesCollaborateursTemp;
   
	private ModeleDynamiqueCollaborateurTemp modele = new ModeleDynamiqueCollaborateurTemp();
	
    private JTextField inputNomAjouter;
	private JTextField inputPrenomAjouter;
	private JTextField inputPhotoAjouter;
	private JTextField inputMdpAjouter;
	
	private ServiceAuthentification SA;
	private ServiceAutorisation SAuto;
	private Collab cASuppr;
	
	private JScrollPane tableSc = null;
	
	private JButton btnAjouterCollabTemp = new JButton("Ajouter");
	private JButton btnSupprimerCollabTemp = new JButton("Supprimer");
	
	private JLabel lblPrnom = new JLabel("Pr\u00E9nom");
	private JLabel lblPhoto = new JLabel("Photo");
	private JLabel lblAjouterUnCollaborateur = new JLabel("Ajouter un collaborateur temporaire");
	private JLabel lblNomAjouter = new JLabel("Nom");
	private JLabel lblMdp = new JLabel("Mdp.");

	public static void main(String args[]){ 
		IHMAccueil frame = new IHMAccueil();
		
	}
	
	public IHMAccueil() {

		init();
		initComponents();
		initTable();
		addComponentsHandlers();
	}
	
	private void initComponents()
	{
		lblNomAjouter.setBounds(20, 330, 46, 14);
		getContentPane().add(lblNomAjouter);
		
		inputNomAjouter = new JTextField();
		inputNomAjouter.setBounds(76, 327, 100, 20);
		getContentPane().add(inputNomAjouter);
		inputNomAjouter.setColumns(10);
		
		inputPrenomAjouter = new JTextField();
		inputPrenomAjouter.setColumns(10);
		inputPrenomAjouter.setBounds(76, 358, 100, 20);
		getContentPane().add(inputPrenomAjouter);
		
		inputPhotoAjouter = new JTextField();
		inputPhotoAjouter.setColumns(10);
		inputPhotoAjouter.setBounds(76, 389, 100, 20);
		getContentPane().add(inputPhotoAjouter);
		
		btnAjouterCollabTemp.setBounds(76, 451, 100, 23);
		getContentPane().add(btnAjouterCollabTemp);
		
		btnSupprimerCollabTemp.setBounds(435, 265, 133, 23);
		getContentPane().add(btnSupprimerCollabTemp);
		
		lblPrnom.setBounds(20, 361, 46, 14);
		getContentPane().add(lblPrnom);
		
		lblPhoto.setBounds(20, 392, 46, 14);
		getContentPane().add(lblPhoto);
		
		lblAjouterUnCollaborateur.setBounds(10, 300, 225, 14);
		getContentPane().add(lblAjouterUnCollaborateur);
		
		inputMdpAjouter = new JTextField();
		inputMdpAjouter.setColumns(10);
		inputMdpAjouter.setBounds(76, 420, 100, 20);
		getContentPane().add(inputMdpAjouter);
		
		lblMdp.setBounds(20, 423, 46, 14);
		getContentPane().add(lblMdp);
		
		this.setVisible(true);
	}
	
	private void addComponentsHandlers()
	{
		btnAjouterCollabTemp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Cr�ation collaborateur
				Collab nouveauCollabTemp;
				nouveauCollabTemp = new Collab();
				nouveauCollabTemp.id = "T-"+getClient().getLastInsertedCollabTempId();
				nouveauCollabTemp.nom = inputNomAjouter.getText();
				nouveauCollabTemp.prenom = inputPrenomAjouter.getText();
				nouveauCollabTemp.photo = inputPhotoAjouter.getText();
				try {
					SA.modifierMdp(nouveauCollabTemp, inputMdpAjouter.getText());
				} catch (NonExiste e1) {
					System.out.println("--- [ERROR] Error adding collab temp : " + e1.raison);
				}
				inputPhotoAjouter.setText("");
				inputPrenomAjouter.setText("");
				inputNomAjouter.setText("");
				inputMdpAjouter.setText("");
				modele.addCollabTemp(nouveauCollabTemp);			
			}
		});

		
		btnSupprimerCollabTemp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selection = lesCollaborateursTemp.getSelectedRow();
				cASuppr = modele.getCollabTemp(selection);
				modele.removeCollabTemp(selection);
				// Suppr. en cascade
				try {
					SA.supprimerEmpreinte(cASuppr);
				} catch (NonExiste e1) {
					System.out.println("--- [ERROR] Error deleting empreinte : " + e1.raison);
				}
				//autorisations
				try {
					AutorisationTemp[] lesAuto = SAuto.getAutorisationsTemps(cASuppr);
					for (int i = 0; i < lesAuto.length; i++) {
						SAuto.supprimerAutorisation(lesAuto[i].id);
					}
				} catch (NonExiste e1) {
					System.out.println("--- [ERROR] Error deleting autorisations : " + e1.raison);

				}
			}
		});
	}
	
	private void initTable()
	{
		//Remplissage de la table
		lesCollaborateursTemp = new JTable(modele);
		tableSc = new JScrollPane(lesCollaborateursTemp);
		tableSc.setColumnHeaderView(lesCollaborateursTemp.getTableHeader());
		lesCollaborateursTemp.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lesCollaborateursTemp.setShowVerticalLines(false);
		tableSc.setBounds(10, 10, 560, 250);
		getContentPane().add(tableSc);
	}
	
	private void init()
	{
		WListener wlist = new WListener(this);
		this.addWindowListener(wlist);
		getContentPane().setLayout(null);
		
		this.setTitle("Service ACCUEIL");
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setPreferredSize(new Dimension(600, 600));
		this.pack();
		this.setLocationRelativeTo(null);
	
		SA = getClient().getServiceAuthentification();
		SAuto = getClient().getServiceAutorisation();
	}
}