package accessControl.ihm;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import com.sun.jmx.snmp.Timestamp;
import com.sun.xml.internal.ws.util.StringUtils;

import javax.swing.JButton;

import accessControl.Annuaire;
import accessControl.Collab;
import accessControl.NonExiste;
import accessControl.Passage;
import accessControl.Porte;
import accessControl.ServiceAcceuil;
import accessControl.ServiceAuthentification;
import accessControl.ServiceAutorisation;
import accessControl.ServiceHistorisation;
import accessControl.ServiceRH;
import accessControl.Zone;
import accessControl.clients.Client;
import accessControl.ihm.Ihm.WListener;

import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

public class IHMLecteurEmpreinte extends Ihm {
	
	private JTextField inputEmpreinte;
	private JTextField inputPhoto;
	
	private JComboBox listPortes;
	
	private ServiceAutorisation SAut;
	private ServiceAuthentification SAuth;
	private ServiceRH SRh;
	private ServiceAcceuil SAccu;
	private ServiceHistorisation SHisto;
	private Annuaire Annu;
	
	private Porte[] allPortes;

	private JButton btnValider = new JButton("Acceder");
	private JLabel listPorte = new JLabel("Empreinte");
	private JLabel lblPhoto = new JLabel("photo");
	private JLabel Porte = new JLabel("Porte");

	public static void main(String[] args) {
		IHMLecteurEmpreinte frame = new IHMLecteurEmpreinte();
		frame.setTitle("Passer une porte");
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.setPreferredSize(new Dimension(400,400));
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	public IHMLecteurEmpreinte() {
		
		init();
		initComponents();
		addComponentsHandlers();
		
		this.setVisible(true);
	}
	
	/* Recuperer la zone associee a la porte selectionnee */
	public Zone getZoneFromListPortes(JComboBox allPortes) {
		String chaineListBox = listPortes.getSelectedItem().toString();
		String[] parts = chaineListBox.split("-");
		String part1 = parts[0]; 
		String part2 = parts[1];
		part1.trim();
		Zone zone = Client.getZone(Short.parseShort(part1));
		return zone;
	}
	
	/* Enregistre le passage d'une porte */
	public Passage creerPassage(boolean autorise) {
		
		// R�cup�ration de la porte
		Porte porte;
		String chaineListBox = listPortes.getSelectedItem().toString();
		String[] parts = chaineListBox.split("-");
		String part1 = parts[0]; 
		String part2 = parts[1];
		part1.trim();
		porte = Client.getPorte(Short.parseShort(part1));
		
		// Enregistrement du passage avec le timestamp
		String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new java.util.Date());
		Passage p = new Passage(Client.getCollab(), timestamp, porte, autorise);
		return p;
	}
	
	private void init()
	{
		this.setTitle("Passer une porte");
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.setPreferredSize(new Dimension(385, 285));
		this.pack();
		this.setVisible(true);
		
		WListener wlist = new WListener(this);
		this.addWindowListener(wlist);
		getContentPane().setLayout(null);
	}
	
	private void initComponents()
	{
		listPortes = new JComboBox();
		listPortes.setBounds(98, 46, 160, 20);
		getContentPane().add(listPortes);
		/* Remplissage de la listbox */
		allPortes = Client.getPortes();
		for(int i = 0; i < allPortes.length; i++) {
			
			String chainePorte = Short.toString(allPortes[i].id) + "-" + allPortes[i].name.toString();
			listPortes.addItem(chainePorte);
		}	
		
		Porte.setBounds(40, 49, 38, 14);
		getContentPane().add(Porte);

		listPorte.setBounds(15, 84, 67, 14);
		getContentPane().add(listPorte);
		
		this.inputEmpreinte = new JTextField();
		this.inputEmpreinte.setBounds(98, 81, 160, 20);
		getContentPane().add(inputEmpreinte);
		this.inputEmpreinte.setColumns(10);
		
		this.inputPhoto = new JTextField();
		this.inputPhoto.setBounds(98, 117, 160, 20);
		getContentPane().add(this.inputPhoto);
		this.inputPhoto.setColumns(10);
		
		btnValider.setBounds(169, 153, 89, 23);
		getContentPane().add(btnValider);

		lblPhoto.setBounds(25, 114, 47, 20);
		getContentPane().add(lblPhoto);
	}
	
	private void addComponentsHandlers()
	{
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String idAuthOK;
				boolean idAuthOK2 = false;
				boolean autOK;
				SAut = Client.getServiceAutorisation();
				SAuth = Client.getServiceAuthentification();
				SAccu = Client.getServiceAccueil();
				SRh = Client.getServiceRH();
				SHisto = Client.getServiceHistorisation();
				Annu = Client.getServiceAnnuaire();
				
				// Recuperation de la zone associee a partir de la porte
				Zone zone = getZoneFromListPortes(listPortes);
				
				try {
					
					/* idAuthOK contient l'id du collaborateur */
					idAuthOK = SAuth.verificationEmpreinte(inputEmpreinte.getText());
					/* idAuthOK2 contient un boolean pour savoir si la photo existe */
					if(!idAuthOK.isEmpty()) idAuthOK2 = Annu.verifierPhoto(idAuthOK, inputPhoto.getText());

					if(!idAuthOK.isEmpty() && idAuthOK2 == true)
					{
						String[] idParts = idAuthOK.split("-");
						Collab collab;
						if (idParts[0].contains("P")){
							collab = SRh.getCollabPerm(Short.parseShort(idParts[1]));
							Client.setCollab(collab);
						} else {
							collab = SAccu.getCollabTemp(Short.parseShort(idParts[1]));
							Client.setCollab(collab);
						}
						
						autOK = SAut.verifierAutorisation(collab, zone);
						if (autOK == true) {
							javax.swing.JOptionPane.showMessageDialog(null,"Bienvenue, " 
						                                                   + collab.prenom
						                                                   + " "
						                                                   + collab.nom);
							
							Passage p = creerPassage(true);
							SHisto.sauvegarderPassage(p);
						} else {
							javax.swing.JOptionPane.showMessageDialog(null,"ACCES INTERDIT");
							Passage p = creerPassage(false);
							SHisto.sauvegarderPassage(p);
						}	
					} else {
						javax.swing.JOptionPane.showMessageDialog(null,"ACCES INTERDIT.");
						Passage p = creerPassage(false);
						SHisto.sauvegarderPassage(p);
					}
				} catch (NonExiste e1) { 
					System.out.println("--- [ERROR] Error saving passage : " + e1.raison);

				}
			}
		});
	}
}
