package accessControl.ihm;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;

import accessControl.Collab;
import accessControl.Existe;
import accessControl.Habilitation;
import accessControl.NonExiste;
import accessControl.ServiceHabilitation;
import accessControl.ServiceRH;
import accessControl.ihm.Ihm.WListener;
import accessControl.model.ModeleDynamiqueCollaborateurPerm;
import accessControl.model.ModeleDynamiqueCollaborateurTemp;

import javax.swing.JButton;
import javax.swing.JComboBox;

public class IHMHabilitation extends Ihm {

	private ServiceHabilitation SH;
	
	private JTable lesCollaborateursPerm;
	private JTable lesCollaborateursTemp;

	private ModeleDynamiqueCollaborateurPerm modelePerm = new ModeleDynamiqueCollaborateurPerm();
	private ModeleDynamiqueCollaborateurTemp modeleTemp = new ModeleDynamiqueCollaborateurTemp();
	
	private Collab collabSelectionne = new Collab();
	
	private JComboBox<Habilitation> JCBListeHabilitationPerm;
	private JComboBox<Habilitation> JCBListeHabAjouterPerm;
	private JComboBox<Habilitation> JCBListeHabilitationTemp;
	private JComboBox<Habilitation> JCBListeHabAjouterTemp;
	
	private JButton btnSupprimerHabilitationPerm;
	private JButton btnSupprimerHabilitationTemp;
	private JButton btnAjouterHabTemp;
	private JButton btnAjouterHabPerm;
	
	private JScrollPane tableScP;
	private JScrollPane tableScT;
	
	private JButton btnSelectionnerPerm = new JButton("Selectionner");
	private JButton btnSelectionnerTemp = new JButton("Selectionner");
	
	private JLabel lblListeHabPerm = new JLabel("Liste des habilitations :");
	private JLabel lblCollaborateursPermanents = new JLabel("Collaborateurs permanents");
	private JLabel lblCollaborateursTemporaires = new JLabel("Collaborateurs temporaires");
	private JLabel lblListeHabTemp = new JLabel("Liste des habilitations :");
	private JLabel lblListeHabAjouterTemp = new JLabel("Ajouter une habilitation :");
	private JLabel lblListeHabAjouterPerm = new JLabel("Ajouter une habilitation :");

	public static void main(String args[]) {
		IHMHabilitation frame = new IHMHabilitation();
	}

	public IHMHabilitation() {

		init();
		initComponents();
		addComponentsHandlers();
		fillTables();

		this.setVisible(true);
	}

	private int from_string(String h) {
		int hab = 0;
		switch (h) {
		case "RH":
			hab = 0;
			break;
		case "RESP":
			hab = 1;
			break;
		case "ACCU":
			hab = 2;
			break;
		case "ANNU":
			hab = 3;
			break;
		case "HISTO":
			hab = 4;
			break;
		}
		return hab;
	}

	private void majListeHabPerm() {
		try {
			short habCollab[] = SH.getHabilitations(collabSelectionne);
			JCBListeHabilitationPerm.removeAllItems();
			JCBListeHabAjouterPerm.removeAllItems();
			if (habCollab[0] != 99) {
				System.out.println("--- Habilitations trouv�es : " + habCollab.toString());
				for (int i = 0; i < habCollab.length; i++) {
					JCBListeHabilitationPerm.addItem(Habilitation.from_int(habCollab[i]));
				}
				btnSupprimerHabilitationPerm.setEnabled(true);
			} else {
				System.out.println("--- Aucune habilitation pour : " + collabSelectionne.toString());
				btnSupprimerHabilitationPerm.setEnabled(false);
			}
			for (int i = 0; i < 5; i++) {
				int k = 0;
				boolean trouve = false;
				while (k < habCollab.length && !trouve) {
					if (habCollab[k] == i) {
						trouve = true;
					}
					k++;
				}
				if (k == habCollab.length && !trouve) {
					JCBListeHabAjouterPerm.addItem(Habilitation.from_int(i));
				}
			}
			if (habCollab.length == 5) {
				btnAjouterHabPerm.setEnabled(false);
			} else {
				btnAjouterHabPerm.setEnabled(true);
			}
		} catch (NonExiste e1) {
			System.out.println("--- [ERROR] Error updating hab list : " + e1.raison);

		}
	}

	private void majListeHabTemp() {
		try {
			short habCollab[] = SH.getHabilitations(collabSelectionne);
			JCBListeHabilitationTemp.removeAllItems();
			JCBListeHabAjouterTemp.removeAllItems();
			if (habCollab[0] != 99) {
				for (int i = 0; i < habCollab.length; i++) {
					JCBListeHabilitationTemp.addItem(Habilitation.from_int(habCollab[i]));
				}
				btnSupprimerHabilitationTemp.setEnabled(true);
			} else {
				System.out.println("--- Aucune habilitation pour : " + collabSelectionne.toString());
				btnSupprimerHabilitationTemp.setEnabled(false);
			}
			for (int i = 0; i < 5; i++) {
				int k = 0;
				boolean trouve = false;
				while (k < habCollab.length && !trouve) {
					if (habCollab[k] == i) {
						trouve = true;
					}
					k++;
				}
				if (k == habCollab.length && !trouve) {
					JCBListeHabAjouterTemp.addItem(Habilitation.from_int(i));
				}
			}
			if (habCollab.length == 5) {
				btnAjouterHabTemp.setEnabled(false);
			} else {
				btnAjouterHabTemp.setEnabled(true);
			}
		} catch (NonExiste e1) {
			System.out.println("--- [ERROR] Error updating hab list : " + e1.raison);

		}
	}

	private void init() {
		this.setTitle("Service Habilitation");
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setPreferredSize(new Dimension(1200, 600));
		this.pack();
		this.getContentPane().setLayout(null);
		this.setLocationRelativeTo(null);

		SH = getClient().getServiceHabilitation();

		WListener wlist = new WListener(this);
		this.addWindowListener(wlist);
		getContentPane().setLayout(null);
	}

	private void initComponents() {
		JCBListeHabilitationPerm = new JComboBox<Habilitation>();
		JCBListeHabilitationPerm.setBounds(225, 411, 202, 23);
		getContentPane().add(JCBListeHabilitationPerm);
		JCBListeHabilitationPerm.setEnabled(false);

		btnSupprimerHabilitationPerm = new JButton("Supprimer");
		btnSupprimerHabilitationPerm.setBounds(437, 411, 133, 23);
		getContentPane().add(btnSupprimerHabilitationPerm);
		btnSupprimerHabilitationPerm.setEnabled(false);

		btnSelectionnerPerm.setBounds(437, 340, 133, 23);
		getContentPane().add(btnSelectionnerPerm);

		btnSelectionnerTemp.setBounds(1027, 340, 133, 23);
		getContentPane().add(btnSelectionnerTemp);

		btnSupprimerHabilitationTemp = new JButton("Supprimer");
		btnSupprimerHabilitationTemp.setBounds(1027, 420, 133, 23);
		getContentPane().add(btnSupprimerHabilitationTemp);
		btnSupprimerHabilitationTemp.setEnabled(false);

		JCBListeHabilitationTemp = new JComboBox<Habilitation>();
		JCBListeHabilitationTemp.setBounds(815, 420, 202, 23);
		getContentPane().add(JCBListeHabilitationTemp);
		JCBListeHabilitationTemp.setEnabled(false);

		JCBListeHabAjouterPerm = new JComboBox<Habilitation>();
		JCBListeHabAjouterPerm.setBounds(225, 460, 202, 23);
		getContentPane().add(JCBListeHabAjouterPerm);
		JCBListeHabAjouterPerm.setEnabled(false);

		btnAjouterHabPerm = new JButton("Ajouter");
		btnAjouterHabPerm.setBounds(437, 460, 133, 23);
		getContentPane().add(btnAjouterHabPerm);
		btnAjouterHabPerm.setEnabled(false);

		JCBListeHabAjouterTemp = new JComboBox<Habilitation>();
		JCBListeHabAjouterTemp.setBounds(815, 460, 202, 23);
		getContentPane().add(JCBListeHabAjouterTemp);
		JCBListeHabAjouterTemp.setEnabled(false);

		btnAjouterHabTemp = new JButton("Ajouter");
		btnAjouterHabTemp.setBounds(1027, 460, 133, 23);
		getContentPane().add(btnAjouterHabTemp);
		btnAjouterHabTemp.setEnabled(false);

		lblListeHabPerm.setBounds(59, 415, 156, 14);
		getContentPane().add(lblListeHabPerm);

		lblListeHabAjouterPerm.setBounds(59, 464, 156, 14);
		getContentPane().add(lblListeHabAjouterPerm);

		lblCollaborateursPermanents.setBounds(10, 11, 348, 14);
		getContentPane().add(lblCollaborateursPermanents);

		lblCollaborateursTemporaires.setBounds(600, 11, 330, 14);
		getContentPane().add(lblCollaborateursTemporaires);

		lblListeHabTemp.setBounds(649, 424, 156, 14);
		getContentPane().add(lblListeHabTemp);

		lblListeHabAjouterTemp.setBounds(649, 464, 156, 14);
		getContentPane().add(lblListeHabAjouterTemp);
	}

	private void addComponentsHandlers() {
		btnSelectionnerPerm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JCBListeHabilitationPerm.removeAllItems();
				JCBListeHabAjouterPerm.removeAllItems();
				JCBListeHabAjouterPerm.setEnabled(true);
				JCBListeHabilitationPerm.setEnabled(true);
				btnAjouterHabPerm.setEnabled(true);
				btnSupprimerHabilitationPerm.setEnabled(true);
				JCBListeHabAjouterTemp.setEnabled(false);
				JCBListeHabilitationTemp.setEnabled(false);
				btnAjouterHabTemp.setEnabled(false);
				btnSupprimerHabilitationTemp.setEnabled(false);

				int selection = lesCollaborateursPerm.getSelectedRow();
				collabSelectionne = modelePerm.getCollabPerm(selection);
				majListeHabPerm();
			}
		});

		btnSelectionnerTemp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JCBListeHabilitationTemp.removeAllItems();
				JCBListeHabAjouterTemp.removeAllItems();
				JCBListeHabAjouterTemp.setEnabled(true);
				JCBListeHabilitationTemp.setEnabled(true);
				btnAjouterHabTemp.setEnabled(true);
				btnSupprimerHabilitationTemp.setEnabled(true);
				JCBListeHabAjouterPerm.setEnabled(false);
				JCBListeHabilitationPerm.setEnabled(false);
				btnAjouterHabPerm.setEnabled(false);
				btnSupprimerHabilitationPerm.setEnabled(false);

				int selection = lesCollaborateursTemp.getSelectedRow();
				collabSelectionne = modeleTemp.getCollabTemp(selection);
				majListeHabTemp();
			}
		});

		btnAjouterHabTemp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String habSelectionnee = JCBListeHabAjouterTemp.getSelectedItem().toString();
				int habAAdd = from_string(habSelectionnee);
				System.out.println(
						"--- Habilitation a ajouter : " + habSelectionnee.toString() + " (id ENUM : " + habAAdd + ")");
				try {
					System.out.println(
							"--- Collab. selectionne : " + collabSelectionne.prenom + " " + collabSelectionne.nom);
					SH.ajouterHabilitation(collabSelectionne, Habilitation.from_int(habAAdd));
				} catch (Existe e1) {
					System.out.println("--- [ERROR] Error adding hab : " + e1.raison);

				}
				majListeHabTemp();
			}
		});

		btnAjouterHabPerm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String habSelectionnee = JCBListeHabAjouterPerm.getSelectedItem().toString();
				int habAAdd = from_string(habSelectionnee);
				System.out.println(
						"--- Habilitation a ajouter : " + habSelectionnee.toString() + " (id ENUM : " + habAAdd + ")");
				try {
					System.out.println(
							"--- Collab. selectionne : " + collabSelectionne.prenom + " " + collabSelectionne.nom);
					SH.ajouterHabilitation(collabSelectionne, Habilitation.from_int(habAAdd));
				} catch (Existe e1) {
					System.out.println("--- [ERROR] Error adding hab : " + e1.raison);

				}
				majListeHabPerm();
			}
		});

		btnSupprimerHabilitationTemp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String habSelectionnee = JCBListeHabilitationTemp.getSelectedItem().toString();
				int habASuppr = from_string(habSelectionnee);
				System.out.println("--- Habilitation a supprimer : " + habSelectionnee.toString() + " (id ENUM : "
						+ habASuppr + ")");
				try {
					System.out.println(
							"--- Collab. selectionne : " + collabSelectionne.prenom + " " + collabSelectionne.nom);
					SH.supprimerHabilitation(collabSelectionne, Habilitation.from_int(habASuppr));
				} catch (NonExiste e1) {
					System.out.println("--- [ERROR] Error deleting hab : " + e1.raison);

				}
				majListeHabTemp();
			}
		});

		btnSupprimerHabilitationPerm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String habSelectionnee = JCBListeHabilitationPerm.getSelectedItem().toString();
				int habASuppr = from_string(habSelectionnee);
				System.out.println("--- Habilitation a supprimer : " + habSelectionnee.toString() + " (id ENUM : "
						+ habASuppr + ")");
				try {
					System.out.println(
							"--- Collab. selectionne : " + collabSelectionne.prenom + " " + collabSelectionne.nom);
					SH.supprimerHabilitation(collabSelectionne, Habilitation.from_int(habASuppr));
				} catch (NonExiste e1) {
					System.out.println("--- [ERROR] Error deleting hab : " + e1.raison);

				}
				majListeHabPerm();
			}
		});
	}

	private void fillTables() {
		// Remplissage de la table Perm
		lesCollaborateursPerm = new JTable(modelePerm);
		tableScP = new JScrollPane(lesCollaborateursPerm);
		tableScP.setColumnHeaderView(lesCollaborateursPerm.getTableHeader());
		lesCollaborateursPerm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lesCollaborateursPerm.setShowVerticalLines(false);
		tableScP.setBounds(10, 30, 560, 300);
		getContentPane().add(tableScP);

		// Remplissage de la table Temp
		lesCollaborateursTemp = new JTable(modeleTemp);
		tableScT = new JScrollPane(lesCollaborateursTemp);
		tableScT.setColumnHeaderView(lesCollaborateursTemp.getTableHeader());
		lesCollaborateursTemp.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lesCollaborateursTemp.setShowVerticalLines(false);
		tableScT.setBounds(600, 30, 560, 300);
		getContentPane().add(tableScT);
	}
}
