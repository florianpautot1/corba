package accessControl;

/**
 * Struct definition : Zone
 * 
 * @author OpenORB Compiler
*/
public final class Zone implements org.omg.CORBA.portable.IDLEntity
{
    @Override
	public String toString() {
		return "Zone [id=" + id + ", name=" + name + "]";
	}

	/**
     * Struct member id
     */
    public short id;

    /**
     * Struct member name
     */
    public String name;

    /**
     * Default constructor
     */
    public Zone()
    { }

    /**
     * Constructor with fields initialization
     * @param id id struct member
     * @param name name struct member
     */
    public Zone(short id, String name)
    {
        this.id = id;
        this.name = name;
    }

}
