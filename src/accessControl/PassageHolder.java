package accessControl;

/**
 * Holder class for : Passage
 * 
 * @author OpenORB Compiler
 */
final public class PassageHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal Passage value
     */
    public accessControl.Passage value;

    /**
     * Default constructor
     */
    public PassageHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public PassageHolder(accessControl.Passage initial)
    {
        value = initial;
    }

    /**
     * Read Passage from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = PassageHelper.read(istream);
    }

    /**
     * Write Passage into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        PassageHelper.write(ostream,value);
    }

    /**
     * Return the Passage TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return PassageHelper.type();
    }

}
