package accessControl;

/**
 * Interface definition : ServiceHistorisation
 * 
 * @author OpenORB Compiler
 */
public class _ServiceHistorisationStub extends org.omg.CORBA.portable.ObjectImpl
        implements ServiceHistorisation
{
    static final String[] _ids_list =
    {
        "IDL:accessControl/ServiceHistorisation:1.0"
    };

    public String[] _ids()
    {
     return _ids_list;
    }

    private final static Class _opsClass = accessControl.ServiceHistorisationOperations.class;

    /**
     * Operation sauvegarderPassage
     */
    public void sauvegarderPassage(accessControl.Passage p)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("sauvegarderPassage",true);
                    accessControl.PassageHelper.write(_output,p);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("sauvegarderPassage",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceHistorisationOperations _self = (accessControl.ServiceHistorisationOperations) _so.servant;
                try
                {
                    _self.sauvegarderPassage( p);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getPassages
     */
    public accessControl.Passage[] getPassages()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getPassages",true);
                    _input = this._invoke(_output);
                    accessControl.Passage[] _arg_ret = accessControl.ServiceHistorisationPackage.PassagesHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getPassages",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceHistorisationOperations _self = (accessControl.ServiceHistorisationOperations) _so.servant;
                try
                {
                    return _self.getPassages();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getPassagesParZones
     */
    public accessControl.Passage[] getPassagesParZones(short idZone)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getPassagesParZones",true);
                    _output.write_short(idZone);
                    _input = this._invoke(_output);
                    accessControl.Passage[] _arg_ret = accessControl.ServiceHistorisationPackage.PassagesHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getPassagesParZones",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceHistorisationOperations _self = (accessControl.ServiceHistorisationOperations) _so.servant;
                try
                {
                    return _self.getPassagesParZones( idZone);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

}
