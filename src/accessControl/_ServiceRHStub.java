package accessControl;

/**
 * Interface definition : ServiceRH
 * 
 * @author OpenORB Compiler
 */
public class _ServiceRHStub extends org.omg.CORBA.portable.ObjectImpl
        implements ServiceRH
{
    static final String[] _ids_list =
    {
        "IDL:accessControl/ServiceRH:1.0"
    };

    public String[] _ids()
    {
     return _ids_list;
    }

    private final static Class _opsClass = accessControl.ServiceRHOperations.class;

    /**
     * Operation ajouterCollabPerm
     */
    public void ajouterCollabPerm(accessControl.Collab col)
        throws accessControl.Existe
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("ajouterCollabPerm",true);
                    accessControl.CollabHelper.write(_output,col);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.ExisteHelper.id()))
                    {
                        throw accessControl.ExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("ajouterCollabPerm",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceRHOperations _self = (accessControl.ServiceRHOperations) _so.servant;
                try
                {
                    _self.ajouterCollabPerm( col);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation supprimerCollabPerm
     */
    public void supprimerCollabPerm(short idCol)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("supprimerCollabPerm",true);
                    _output.write_short(idCol);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("supprimerCollabPerm",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceRHOperations _self = (accessControl.ServiceRHOperations) _so.servant;
                try
                {
                    _self.supprimerCollabPerm( idCol);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation modifierCollabPerm
     */
    public void modifierCollabPerm(accessControl.Collab col)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("modifierCollabPerm",true);
                    accessControl.CollabHelper.write(_output,col);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("modifierCollabPerm",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceRHOperations _self = (accessControl.ServiceRHOperations) _so.servant;
                try
                {
                    _self.modifierCollabPerm( col);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getCollabPerm
     */
    public accessControl.Collab getCollabPerm(short idCol)
        throws accessControl.NonExiste
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getCollabPerm",true);
                    _output.write_short(idCol);
                    _input = this._invoke(_output);
                    accessControl.Collab _arg_ret = accessControl.CollabHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    if (_exception_id.equals(accessControl.NonExisteHelper.id()))
                    {
                        throw accessControl.NonExisteHelper.read(_exception.getInputStream());
                    }

                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getCollabPerm",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceRHOperations _self = (accessControl.ServiceRHOperations) _so.servant;
                try
                {
                    return _self.getCollabPerm( idCol);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getCollabPerms
     */
    public accessControl.Collab[] getCollabPerms()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getCollabPerms",true);
                    _input = this._invoke(_output);
                    accessControl.Collab[] _arg_ret = accessControl.CollabsHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getCollabPerms",_opsClass);
                if (_so == null)
                   continue;
                accessControl.ServiceRHOperations _self = (accessControl.ServiceRHOperations) _so.servant;
                try
                {
                    return _self.getCollabPerms();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

}
