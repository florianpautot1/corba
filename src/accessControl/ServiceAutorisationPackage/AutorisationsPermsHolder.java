package accessControl.ServiceAutorisationPackage;

/**
 * Holder class for : AutorisationsPerms
 * 
 * @author OpenORB Compiler
 */
final public class AutorisationsPermsHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal AutorisationsPerms value
     */
    public accessControl.AutorisationPerm[] value;

    /**
     * Default constructor
     */
    public AutorisationsPermsHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public AutorisationsPermsHolder(accessControl.AutorisationPerm[] initial)
    {
        value = initial;
    }

    /**
     * Read AutorisationsPerms from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = AutorisationsPermsHelper.read(istream);
    }

    /**
     * Write AutorisationsPerms into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        AutorisationsPermsHelper.write(ostream,value);
    }

    /**
     * Return the AutorisationsPerms TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return AutorisationsPermsHelper.type();
    }

}
