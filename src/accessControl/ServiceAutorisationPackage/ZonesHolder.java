package accessControl.ServiceAutorisationPackage;

/**
 * Holder class for : Zones
 * 
 * @author OpenORB Compiler
 */
final public class ZonesHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal Zones value
     */
    public accessControl.Zone[] value;

    /**
     * Default constructor
     */
    public ZonesHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public ZonesHolder(accessControl.Zone[] initial)
    {
        value = initial;
    }

    /**
     * Read Zones from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = ZonesHelper.read(istream);
    }

    /**
     * Write Zones into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        ZonesHelper.write(ostream,value);
    }

    /**
     * Return the Zones TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return ZonesHelper.type();
    }

}
