package accessControl.ServiceAutorisationPackage;

/**
 * Holder class for : AutorisationsTemps
 * 
 * @author OpenORB Compiler
 */
final public class AutorisationsTempsHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal AutorisationsTemps value
     */
    public accessControl.AutorisationTemp[] value;

    /**
     * Default constructor
     */
    public AutorisationsTempsHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public AutorisationsTempsHolder(accessControl.AutorisationTemp[] initial)
    {
        value = initial;
    }

    /**
     * Read AutorisationsTemps from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = AutorisationsTempsHelper.read(istream);
    }

    /**
     * Write AutorisationsTemps into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        AutorisationsTempsHelper.write(ostream,value);
    }

    /**
     * Return the AutorisationsTemps TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return AutorisationsTempsHelper.type();
    }

}
