package accessControl.ServiceAutorisationPackage;

/** 
 * Helper class for : AutorisationsPerms
 *  
 * @author OpenORB Compiler
 */ 
public class AutorisationsPermsHelper
{
    private static final boolean HAS_OPENORB;
    static {
        boolean hasOpenORB = false;
        try {
            Thread.currentThread().getContextClassLoader().loadClass("org.openorb.CORBA.Any");
            hasOpenORB = true;
        }
        catch(ClassNotFoundException ex) {
        }
        HAS_OPENORB = hasOpenORB;
    }
    /**
     * Insert AutorisationsPerms into an any
     * @param a an any
     * @param t AutorisationsPerms value
     */
    public static void insert(org.omg.CORBA.Any a, accessControl.AutorisationPerm[] t)
    {
        a.insert_Streamable(new accessControl.ServiceAutorisationPackage.AutorisationsPermsHolder(t));
    }

    /**
     * Extract AutorisationsPerms from an any
     * @param a an any
     * @return the extracted AutorisationsPerms value
     */
    public static accessControl.AutorisationPerm[] extract(org.omg.CORBA.Any a)
    {
        if (!a.type().equal(type()))
            throw new org.omg.CORBA.MARSHAL();
        if(HAS_OPENORB && a instanceof org.openorb.CORBA.Any) {
            // streamable extraction. The jdk stubs incorrectly define the Any stub
            org.openorb.CORBA.Any any = (org.openorb.CORBA.Any)a;
            try {
                org.omg.CORBA.portable.Streamable s = any.extract_Streamable();
                if(s instanceof accessControl.ServiceAutorisationPackage.AutorisationsPermsHolder)
                    return ((accessControl.ServiceAutorisationPackage.AutorisationsPermsHolder)s).value;
            } catch (org.omg.CORBA.BAD_INV_ORDER ex) {
            }
            accessControl.ServiceAutorisationPackage.AutorisationsPermsHolder h = new accessControl.ServiceAutorisationPackage.AutorisationsPermsHolder(read(a.create_input_stream()));
            a.insert_Streamable(h);
            return h.value;
        }
        return read(a.create_input_stream());
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the AutorisationsPerms TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            _tc = orb.create_alias_tc(id(),"AutorisationsPerms",orb.create_sequence_tc(0,accessControl.AutorisationPermHelper.type()));
        }
        return _tc;
    }

    /**
     * Return the AutorisationsPerms IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:accessControl/ServiceAutorisation/AutorisationsPerms:1.0";

    /**
     * Read AutorisationsPerms from a marshalled stream
     * @param istream the input stream
     * @return the readed AutorisationsPerms value
     */
    public static accessControl.AutorisationPerm[] read(org.omg.CORBA.portable.InputStream istream)
    {
        accessControl.AutorisationPerm[] new_one;
        {
        int size7 = istream.read_ulong();
        new_one = new accessControl.AutorisationPerm[size7];
        for (int i7=0; i7<new_one.length; i7++)
         {
            new_one[i7] = accessControl.AutorisationPermHelper.read(istream);

         }
        }

        return new_one;
    }

    /**
     * Write AutorisationsPerms into a marshalled stream
     * @param ostream the output stream
     * @param value AutorisationsPerms value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, accessControl.AutorisationPerm[] value)
    {
        ostream.write_ulong(value.length);
        for (int i7=0; i7<value.length; i7++)
        {
            accessControl.AutorisationPermHelper.write(ostream,value[i7]);

        }
    }

}
