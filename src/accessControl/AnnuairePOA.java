package accessControl;

/**
 * Interface definition : Annuaire
 * 
 * @author OpenORB Compiler
 */
public abstract class AnnuairePOA extends org.omg.PortableServer.Servant
        implements AnnuaireOperations, org.omg.CORBA.portable.InvokeHandler
{
    public Annuaire _this()
    {
        return AnnuaireHelper.narrow(_this_object());
    }

    public Annuaire _this(org.omg.CORBA.ORB orb)
    {
        return AnnuaireHelper.narrow(_this_object(orb));
    }

    private static String [] _ids_list =
    {
        "IDL:accessControl/Annuaire:1.0"
    };

    public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte [] objectId)
    {
        return _ids_list;
    }

    public final org.omg.CORBA.portable.OutputStream _invoke(final String opName,
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler)
    {

        if (opName.equals("ajouterCollab")) {
                return _invoke_ajouterCollab(_is, handler);
        } else if (opName.equals("getCollab")) {
                return _invoke_getCollab(_is, handler);
        } else if (opName.equals("getCollabs")) {
                return _invoke_getCollabs(_is, handler);
        } else if (opName.equals("modifierCollab")) {
                return _invoke_modifierCollab(_is, handler);
        } else if (opName.equals("supprimerCollab")) {
                return _invoke_supprimerCollab(_is, handler);
        } else if (opName.equals("verifierPhoto")) {
                return _invoke_verifierPhoto(_is, handler);
        } else {
            throw new org.omg.CORBA.BAD_OPERATION(opName);
        }
    }

    // helper methods
    private org.omg.CORBA.portable.OutputStream _invoke_ajouterCollab(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);

        try
        {
            ajouterCollab(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.Existe _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.ExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_supprimerCollab(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();

        try
        {
            supprimerCollab(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_modifierCollab(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        accessControl.Collab arg0_in = accessControl.CollabHelper.read(_is);

        try
        {
            modifierCollab(arg0_in);

            _output = handler.createReply();

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_verifierPhoto(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();
        String arg1_in = _is.read_string();

        boolean _arg_result = verifierPhoto(arg0_in, arg1_in);

        _output = handler.createReply();
        _output.write_boolean(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getCollab(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();

        try
        {
            accessControl.Collab _arg_result = getCollab(arg0_in);

            _output = handler.createReply();
            accessControl.CollabHelper.write(_output,_arg_result);

        }
        catch (accessControl.NonExiste _exception)
        {
            _output = handler.createExceptionReply();
            accessControl.NonExisteHelper.write(_output,_exception);
        }
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getCollabs(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        accessControl.Collab[] _arg_result = getCollabs();

        _output = handler.createReply();
        accessControl.CollabsHelper.write(_output,_arg_result);

        return _output;
    }

}
