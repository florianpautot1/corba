package accessControl;

/** 
 * Helper class for : ServiceHistorisation
 *  
 * @author OpenORB Compiler
 */ 
public class ServiceHistorisationHelper
{
    /**
     * Insert ServiceHistorisation into an any
     * @param a an any
     * @param t ServiceHistorisation value
     */
    public static void insert(org.omg.CORBA.Any a, accessControl.ServiceHistorisation t)
    {
        a.insert_Object(t , type());
    }

    /**
     * Extract ServiceHistorisation from an any
     * @param a an any
     * @return the extracted ServiceHistorisation value
     */
    public static accessControl.ServiceHistorisation extract(org.omg.CORBA.Any a)
    {
        if (!a.type().equal(type()))
            throw new org.omg.CORBA.MARSHAL();
        try {
            return accessControl.ServiceHistorisationHelper.narrow(a.extract_Object());
        } catch (final org.omg.CORBA.BAD_PARAM e) {
            throw new org.omg.CORBA.MARSHAL(e.getMessage());
        }
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the ServiceHistorisation TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            _tc = orb.create_interface_tc(id(),"ServiceHistorisation");
        }
        return _tc;
    }

    /**
     * Return the ServiceHistorisation IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:accessControl/ServiceHistorisation:1.0";

    /**
     * Read ServiceHistorisation from a marshalled stream
     * @param istream the input stream
     * @return the readed ServiceHistorisation value
     */
    public static accessControl.ServiceHistorisation read(org.omg.CORBA.portable.InputStream istream)
    {
        return(accessControl.ServiceHistorisation)istream.read_Object(accessControl._ServiceHistorisationStub.class);
    }

    /**
     * Write ServiceHistorisation into a marshalled stream
     * @param ostream the output stream
     * @param value ServiceHistorisation value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, accessControl.ServiceHistorisation value)
    {
        ostream.write_Object((org.omg.CORBA.portable.ObjectImpl)value);
    }

    /**
     * Narrow CORBA::Object to ServiceHistorisation
     * @param obj the CORBA Object
     * @return ServiceHistorisation Object
     */
    public static ServiceHistorisation narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof ServiceHistorisation)
            return (ServiceHistorisation)obj;

        if (obj._is_a(id()))
        {
            _ServiceHistorisationStub stub = new _ServiceHistorisationStub();
            stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
            return stub;
        }

        throw new org.omg.CORBA.BAD_PARAM();
    }

    /**
     * Unchecked Narrow CORBA::Object to ServiceHistorisation
     * @param obj the CORBA Object
     * @return ServiceHistorisation Object
     */
    public static ServiceHistorisation unchecked_narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof ServiceHistorisation)
            return (ServiceHistorisation)obj;

        _ServiceHistorisationStub stub = new _ServiceHistorisationStub();
        stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
        return stub;

    }

}
