package accessControl.ServiceHistorisationPackage;

/** 
 * Helper class for : Passages
 *  
 * @author OpenORB Compiler
 */ 
public class PassagesHelper
{
    private static final boolean HAS_OPENORB;
    static {
        boolean hasOpenORB = false;
        try {
            Thread.currentThread().getContextClassLoader().loadClass("org.openorb.CORBA.Any");
            hasOpenORB = true;
        }
        catch(ClassNotFoundException ex) {
        }
        HAS_OPENORB = hasOpenORB;
    }
    /**
     * Insert Passages into an any
     * @param a an any
     * @param t Passages value
     */
    public static void insert(org.omg.CORBA.Any a, accessControl.Passage[] t)
    {
        a.insert_Streamable(new accessControl.ServiceHistorisationPackage.PassagesHolder(t));
    }

    /**
     * Extract Passages from an any
     * @param a an any
     * @return the extracted Passages value
     */
    public static accessControl.Passage[] extract(org.omg.CORBA.Any a)
    {
        if (!a.type().equal(type()))
            throw new org.omg.CORBA.MARSHAL();
        if(HAS_OPENORB && a instanceof org.openorb.CORBA.Any) {
            // streamable extraction. The jdk stubs incorrectly define the Any stub
            org.openorb.CORBA.Any any = (org.openorb.CORBA.Any)a;
            try {
                org.omg.CORBA.portable.Streamable s = any.extract_Streamable();
                if(s instanceof accessControl.ServiceHistorisationPackage.PassagesHolder)
                    return ((accessControl.ServiceHistorisationPackage.PassagesHolder)s).value;
            } catch (org.omg.CORBA.BAD_INV_ORDER ex) {
            }
            accessControl.ServiceHistorisationPackage.PassagesHolder h = new accessControl.ServiceHistorisationPackage.PassagesHolder(read(a.create_input_stream()));
            a.insert_Streamable(h);
            return h.value;
        }
        return read(a.create_input_stream());
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the Passages TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            _tc = orb.create_alias_tc(id(),"Passages",orb.create_sequence_tc(0,accessControl.PassageHelper.type()));
        }
        return _tc;
    }

    /**
     * Return the Passages IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:accessControl/ServiceHistorisation/Passages:1.0";

    /**
     * Read Passages from a marshalled stream
     * @param istream the input stream
     * @return the readed Passages value
     */
    public static accessControl.Passage[] read(org.omg.CORBA.portable.InputStream istream)
    {
        accessControl.Passage[] new_one;
        {
        int size7 = istream.read_ulong();
        new_one = new accessControl.Passage[size7];
        for (int i7=0; i7<new_one.length; i7++)
         {
            new_one[i7] = accessControl.PassageHelper.read(istream);

         }
        }

        return new_one;
    }

    /**
     * Write Passages into a marshalled stream
     * @param ostream the output stream
     * @param value Passages value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, accessControl.Passage[] value)
    {
        ostream.write_ulong(value.length);
        for (int i7=0; i7<value.length; i7++)
        {
            accessControl.PassageHelper.write(ostream,value[i7]);

        }
    }

}
