package accessControl.ServiceHistorisationPackage;

/**
 * Holder class for : Passages
 * 
 * @author OpenORB Compiler
 */
final public class PassagesHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal Passages value
     */
    public accessControl.Passage[] value;

    /**
     * Default constructor
     */
    public PassagesHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public PassagesHolder(accessControl.Passage[] initial)
    {
        value = initial;
    }

    /**
     * Read Passages from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = PassagesHelper.read(istream);
    }

    /**
     * Write Passages into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        PassagesHelper.write(ostream,value);
    }

    /**
     * Return the Passages TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return PassagesHelper.type();
    }

}
